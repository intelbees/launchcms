<?php

namespace App\MenuRenderer;
use LaunchCMS\Services\Facades\StructureService;
use View;
class ContentMenuRenderer implements MenuRendererInterface
{
    public function render()
    {
        $allContentTypes = StructureService::getAllContentTypes();
        $view = View::make('menu.content-menu', ['contentTypes'=> $allContentTypes]);
        return $view->render();
    }

}