<?php

namespace App\MenuRenderer;


interface MenuRendererInterface
{
    function render();
}