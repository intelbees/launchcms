<?php

namespace App\Providers;

use App\Http\Helpers\ClientLocalization;

use App\Http\Helpers\MenuBuilder;
use Sentinel;
use LaunchCMS\Plugin\PluginServiceProvider;

class AppServiceProvider extends PluginServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        // Using Closure based composers...
        view()->composer('layout.main_menu', function ($view) {

            $view->with('adminMenu', MenuBuilder::buildMenuViewObjectFromConfig('launchcms.admin_menu'));

        });
        view()->composer('*', function ($view) {
            $user = Sentinel::getUser();
            $view->with('user', $user);
        });
        view()->composer('support.localization', function ($view) {

            $view->with('messages', ClientLocalization::getMessages());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
