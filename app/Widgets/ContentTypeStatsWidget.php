<?php

namespace App\Widgets;
use LaunchCMS\Models\Content\ContentType;
use View;
use LaunchCMS\Widget\AbstractWidget;

class ContentTypeStatsWidget extends AbstractWidget
{
    public function render($params = null)
    {
        $countContentType = ContentType::count();
        
        $view = View::make('widgets.content_type_stats', ['countContentType'=> $countContentType]);
        return $view->render();
    }

}