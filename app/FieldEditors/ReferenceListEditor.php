<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use View;

class ReferenceListEditor extends AbstractEditor
{


    public function render(Content $content = null)
    {
        $selectedContentIDs = $this->getRawFieldValue($content);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $contentTypeID = $this->field->getExtraDataTypeInfo()[Field::CONTENT_TYPE_ID];
        $contentType = StructureService::getContentTypeFromCacheByID($contentTypeID);
        $selectedDataRows = [];
        if(!empty($selectedContentIDs)) {
            $queryBuilder = ContentService::getQueryBuilder($contentType->getAlias());
            $selectedDataRows = $queryBuilder->whereIn('_id', $selectedContentIDs)->get();
        }
        $searchURL = route('cms_search_content_by_name', ['content_type_id' => $contentTypeID]);
        $view = View::make('field-editors.reference-list-editor', [
            'field' => $this->field,
            'contentTypeID' => $contentTypeID,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'searchURL' => $searchURL,
            'selectedContents' => $selectedDataRows,
            'dialogMode' => $this->dialogMode
        ]);

        return $view->render();
    }


}