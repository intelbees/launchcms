<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use View;

class BooleanEditor extends AbstractEditor
{

    public function render(Content $content = null)
    {
        $date = $this->getRawFieldValue($content);
        $displayValue = $this->createDisplayValue($date);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $view = View::make('field-editors.boolean-editor', [
            'field'              => $this->field,
            'value'              => $displayValue,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'dialogMode'         => $this->dialogMode,
        ]);

        return $view->render();
    }

    public function createPersistValue($value)
    {
        if(empty($value)) {
            return null;
        }
        return $value === 'yes';
    }

    public function createDisplayValue($value)
    {
        if(empty($value)) {
            return null;
        }
        if($value === true) {
            return 'yes';
        }
        return 'no';
    }

}