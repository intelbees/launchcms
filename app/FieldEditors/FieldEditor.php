<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Field;

interface FieldEditor
{
    function __construct(Field $field, Field $embeddedField);
    function render(Content $content);
    function setDialogMode($dialogMode);
}