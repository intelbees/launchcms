<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use View;

class IntegerEditor extends AbstractEditor
{

    public function render(Content $content = null)
    {
        $date = $this->getRawFieldValue($content);
        $displayValue = $this->createDisplayValue($date);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $view = View::make('field-editors.integer-editor', [
            'field'              => $this->field,
            'value'              => $displayValue,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'dialogMode'         => $this->dialogMode,
        ]);

        return $view->render();
    }

    public function createPersistValue($value)
    {
        if(empty($value)) {
            return 0;
        }
        return intval($value);
    }

    public function createDisplayValue($value)
    {
        return $value;
    }
}