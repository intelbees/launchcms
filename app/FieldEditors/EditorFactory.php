<?php

namespace App\FieldEditors;

use LaunchCMS\Models\Content\Field;

class EditorFactory
{
    /**
     * This method is used to get the associated editor for specific field.
     * $embeddedField is used for the case we want to render the field in a parent embedded field
     * @param Field $field
     * @param Field|null $embeddedField
     * @return DateEditor|DateTimeEditor|EmbeddedEditor|ReferenceEditor|ReferenceListEditor|StringEditor|null
     */
    public static function getEditor(Field $field, Field $embeddedField = null) {
        $fieldType = $field->getDataType();
        $editor = null;
        if($fieldType === Field::STRING) {
            return new StringEditor($field, $embeddedField);
        }
        else if($fieldType === Field::DATE) {
            return new DateEditor($field, $embeddedField);
        }
        else if($fieldType === Field::DATETIME) {
            return new DateTimeEditor($field, $embeddedField);
        }
        else if($fieldType === Field::REFERENCE) {
            return new ReferenceEditor($field, $embeddedField);
        }
        else if($fieldType === Field::REFERENCE_LIST) {
            return new ReferenceListEditor($field, $embeddedField);
        }
        else if($fieldType === Field::EMBEDDED) {
            return new EmbeddedEditor($field, $embeddedField);
        }
        else if($fieldType === Field::EMBEDDED_LIST) {
            return new EmbeddedListEditor($field, $embeddedField);
        }
        else if($fieldType === Field::BOOLEAN) {
            return new BooleanEditor($field, $embeddedField);
        }
        else if($fieldType === Field::INTEGER) {
            return new IntegerEditor($field, $embeddedField);
        }
        else if($fieldType === Field::DOUBLE) {
            return new DoubleEditor($field, $embeddedField);
        }
        else if($fieldType === Field::GEO) {
            return new GeoEditor($field, $embeddedField);
        }
        return null;
    }

    /**
     * This method is used for the case we want to get the editor to render
     * on dialog mode - which means in the case we want to add Embedded record in an Embedded list field.
     * The embedded record is input via a Bootstrap dialog. To render the field in the bootstrap dialog, we do not need
     * to pass the data to the render method because data of the embedded record will be filled into each field on the
     * dialog via Javascript (not by server side method as what we do in the rendering fields on the main content editor screen).
     * @param Field $field
     * @return null
     */
    public function getEditorInDialogMode(Field $field) {
        $editor =$this->getEditor($field, null);
        $editor->setDialogMode(true);
        return $editor;
    }

    
}