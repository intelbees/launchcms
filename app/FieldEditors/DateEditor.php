<?php

namespace App\FieldEditors;


use Carbon\Carbon;
use LaunchCMS\Models\Content\DataObject\Content;
use View;

class DateEditor extends AbstractEditor
{

    public function render(Content $content = null)
    {
        $date = $this->getRawFieldValue($content);
        $displayValue =$this->createDisplayValue($date);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $editor = 'date_picker';
        if ($this->field->getExtraDataTypeInfo() != null && isset( $this->field->getExtraDataTypeInfo()[ 'editor' ] )) {
            $editor = $this->field->getExtraDataTypeInfo()[ 'editor' ];
        }
        $view = View::make('field-editors.date-editor', [
            'field'              => $this->field,
            'value'              => $displayValue,
            'editor'             => $editor,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'dialogMode'         => $this->dialogMode,
        ]);

        return $view->render();
    }

    protected function getDateFormat() {
        return 'm/d/Y';
    }

    public function createPersistValue($value)
    {
        if(empty($value)) {
            return null;
        }
        return Carbon::createFromFormat('m/d/Y', $value);
    }

    public function createDisplayValue($date)
    {
        if(empty($date)) {
            return null;
        }
        $date->setToStringFormat($this->getDateFormat());
        return (string) $date;
    }

}