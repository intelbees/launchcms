<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use View;

class StringEditor extends AbstractEditor
{

    protected function getEditorType()
    {
        $editorType = 'text_field';
        if (isset( $this->field->getExtraDataTypeInfo()[ 'editor' ] )) {
            $editorType = $this->field->getExtraDataTypeInfo()[ 'editor' ];
        }

        return $editorType;
    }

    public function render(Content $content = null)
    {
        $value = $this->getRawFieldValue($content);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $editorType = $this->getEditorType();
        $view = View::make('field-editors.string-editor', [
            'field'              => $this->field,
            'value'              => $value,
            'editorType'         => $editorType,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'dialogMode' => $this->dialogMode
        ]);

        return $view->render();
    }

    

}