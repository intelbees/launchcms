<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\GeoField;
use View;

class GeoEditor extends AbstractEditor
{

    public function render(Content $content = null)
    {
        $date = $this->getRawFieldValue($content);
        $displayValue = $this->createDisplayValue($date);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $view = View::make('field-editors.geo-editor', [
            'field'              => $this->field,
            'value'              => $displayValue,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'dialogMode'         => $this->dialogMode,
        ]);

        return $view->render();
    }

    public function createPersistValue($value)
    {
        if(empty($value)) {
            return null;
        }
        $lngLat = explode(',', $value);
        if(count($lngLat) !== 2) {
            return null;
        }
        $lat = floatval($lngLat[0]);
        $lng = floatval($lngLat[1]);

        return GeoField::createLocationData($lat, $lng);
    }

    public function createDisplayValue($value)
    {
        if(empty($value)) {
            return null;
        }
        return [
            GeoField::LAT => $value['coordinates'][GeoField::LAT],
            GeoField::LNG => $value['coordinates'][GeoField::LNG]
        ];

    }

}