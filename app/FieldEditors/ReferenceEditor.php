<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\Helper\FieldFactory;
use LaunchCMS\Models\Content\ReferenceField;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use View;

class ReferenceEditor extends AbstractEditor
{


    public function render(Content $content = null)
    {
        $value = $this->getRawFieldValue($content);

        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $contentTypeID = $this->field->getExtraDataTypeInfo()[Field::CONTENT_TYPE_ID];
        $contentType = StructureService::getContentTypeFromCacheByID($contentTypeID);


        $selectedContent = null;
        if(!empty($value)) {
            $selectedContent = ContentService::getContentByID($contentType->getAlias(), $value);
        }

        $searchURL = route('cms_search_content_by_name', ['content_type_id' => $contentTypeID]);
        $view = View::make('field-editors.reference-editor', [
            'field' => $this->field,
            'value'=> $value,
            'contentTypeID' => $contentTypeID,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'searchURL' => $searchURL,
            'selectedContent' => $selectedContent,
            'dialogMode' => $this->dialogMode
        ]);

        return $view->render();
    }



}