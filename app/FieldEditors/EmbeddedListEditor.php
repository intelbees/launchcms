<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Facades\StructureService;
use View;

class EmbeddedListEditor extends AbstractEditor
{

    protected function buildDefaultFieldDisplayInTable($allFields)
    {
        $result = [ ];
        $fieldTypeNotDisplayInTable = [ Field::REFERENCE_LIST ];
        /** @var Field $field */
        foreach ($allFields as $field) {
            if (in_array($field->getDataType(), $fieldTypeNotDisplayInTable)) {
                continue;
            }
            //we don't want to add too long text to the table as there is not enough place
            if ($field->getDataType() === Field::STRING) {
                if ($field->getExtraDataTypeInfo()[ 'editor' ] !== 'text_field') {
                    continue;
                }
            }
            $result[] = $field;
        }

        return $result;
    }

    protected function getDataTypeDisplayFields(DataType $dataType)
    {
        $settings = $dataType->getSettings();
        $displayedFields = isset( $settings[ 'table_display_fields' ] ) ? $settings[ 'table_display_fields' ] : [ ];
        $result = [ ];
        $allFields = $dataType->getAllFields();
        if (empty( $displayedFields )) {
            return $this->buildDefaultFieldDisplayInTable($allFields);
        }
        foreach ($displayedFields as $displayedFieldAlias) {
            $field = $dataType->getField($displayedFieldAlias);
            if ( !empty( $field )) {
                $result [] = $field;
            }
        }

        return $result;
    }

    public function render(Content $content)
    {
        $dataTypeID = $this->field->getExtraDataTypeInfo()[ Field::DATA_TYPE_ID ];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);
        $displayFields = $this->getDataTypeDisplayFields($dataType);
        $displayData = [ ];
        $rawData = [ ];
        $fieldAlias = $this->field->getAlias();
        if (array_key_exists($fieldAlias, $content->attributes)) {
            $rawData = $content->attributes[ $fieldAlias ];
        }

        if(!empty($rawData)) {
            foreach ($rawData as $row) {
                $displayRow = [];
                foreach ($displayFields as $displayField) {
                    $fieldFormatter = EditorFactory::getEditor($displayField);
                    $valueInDB = $row[ $displayField->getAlias() ];
                    $displayValue = $fieldFormatter->createDisplayValue($valueInDB);
                    $displayRow[ $displayField->getAlias() ] = $displayValue;
                }
                $displayRow['_id'] = $row['_id'];
                $displayData[] = $displayRow;
            }
        }

        $view = View::make('field-editors.embedded-list-editor', [ 'field'         => $this->field,
                                                                   'displayFields' => $displayFields,
                                                                   'dataType'      => $dataType,
                                                                   'dataRows'      => $displayData,
                                                                   'content'       => $content ]);

        return $view->render();
    }


}