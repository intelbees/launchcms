<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\Helper\FieldFactory;
use LaunchCMS\Services\Facades\StructureService;
use View;

class EmbeddedEditor extends AbstractEditor
{


    public function render(Content $content = null)
    {
        $dataTypeID = $this->field->getExtraDataTypeInfo()[ Field::DATA_TYPE_ID ];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);
        $dataTypeFields = $dataType->getAllFields();
        $view = View::make('field-editors.embedded-editor', [ 'field'          => $this->field,
                                                              'dataTypeFields' => $dataTypeFields,
                                                              'embeddedField'  => $this->field,
                                                              'content'        => $content ]);

        return $view->render();
    }

    public function createPersistValue($value) {
        if($value === null) {
            return null;
        }
        $output = [];
        /** @var EmbeddedField $embeddedField */
        $embeddedField = FieldFactory::getConcreteField($this->field);
        $dataType = $embeddedField->getDataTypeStructureObject();
        foreach ($value as $key => $valueData) {
            $fieldInDataType = $dataType->getField($key);
            $formatter = EditorFactory::getEditor($fieldInDataType);
            if(!empty($formatter)) {
                $output[$key] = $formatter->createPersistValue($valueData);
            }
        }
        return $output;
    }
}