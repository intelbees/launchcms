<?php

namespace App\FieldEditors;


use Carbon\Carbon;
use LaunchCMS\Models\Content\DataObject\Content;
use View;

class DateTimeEditor extends AbstractEditor
{

    public function render(Content $content = null)
    {
        /** @var Carbon $dateTime */
        $dateTime = $this->getRawFieldValue($content);
        $displayValue = $this->createDisplayValue($dateTime);
        $embeddedFieldAlias = $this->getEmbeddedFieldAlias();
        $view = View::make('field-editors.datetime-editor', [
            'field' => $this->field,
            'value'=> $displayValue,
            'embeddedFieldAlias' => $embeddedFieldAlias,
            'dialogMode' => $this->dialogMode
        ]);
        return $view->render();
    }

    protected function getDateTimeFormat() {
        return 'd/m/Y g:i A';
    }

    public function createPersistValue($value)
    {
        if(empty($value)) {
            return null;
        }
        return Carbon::createFromFormat($this->getDateTimeFormat(), $value);
    }

    public function createDisplayValue($value)
    {
        if(empty($value)) {
            return null;
        }
        $value->setToStringFormat($this->getDateTimeFormat());
        return (string) $value;
    }

}