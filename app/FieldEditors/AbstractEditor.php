<?php

namespace App\FieldEditors;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Field;

abstract class AbstractEditor implements FieldEditor
{
    protected $embeddedField;
    protected $field;
    protected $dialogMode = false;
    public function __construct(Field $field = null, Field $embeddedField = null)
    {
        $this->field = $field;
        $this->embeddedField = $embeddedField;
    }
    public function setDialogMode($dialogMode) {
        $this->dialogMode = $dialogMode;
    }

    /**
     * This method is used for getting raw value (from DB) of the field.
     * This method also handles the case where current field is inside the embedded field
     * (which is passed in the constructor)
     * @param Content|null $content
     * @return null
     */
    public function getRawFieldValue(Content $content = null) {
        if($content == null) {
            return null;
        }
        $fieldAlias = $this->field->getAlias();
        if(empty($this->embeddedField)) {
            if(!array_key_exists($fieldAlias, $content->attributes)) {
                return null;
            }
            return $content->attributes[$fieldAlias];
        }
        $embeddedFieldAlias = $this->embeddedField->getAlias();
        if(!array_key_exists($embeddedFieldAlias, $content->attributes)) {
            return null;
        }
        $dataOfEmbeddedField = $content->attributes[$embeddedFieldAlias];
        if(!array_key_exists($fieldAlias, $dataOfEmbeddedField)) {
            return null;
        }
        return $dataOfEmbeddedField[$fieldAlias];
    }

    public function getEmbeddedFieldAlias() {
        return $embeddedFieldAlias = !empty($this->embeddedField) ? $this->embeddedField->getAlias() : '';
    }

    /**
     * This method gets the raw $value from HTTP request and build it to the correct data format
     * before storing to the field in the MongoDB database
     * @param $value
     * @return mixed
     */
    public function createPersistValue($value) {
        return $value;
    }

    /**
     * This method receives the raw value of the field from database and build it to the value for rendering data in the editor
     * @param $value
     * @return mixed
     */
    public function createDisplayValue($value) {
        return $value;
    }
}