<?php

namespace App\Bootstrap;


use Illuminate\Contracts\Foundation\Application;
use LaunchCMS\Plugin\Support\ClassLoader;

class RegisterClassLoader
{

    /**
     * Register The October Auto Loader
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function bootstrap(Application $app)
    {
        ClassLoader::register();
        ClassLoader::addDirectories([
            $app->basePath().'/plugins'
        ]);
    }

}