<?php

namespace App\Http\Controllers\Admin;
use LaunchCMS\Utils\StringUtil;
use Sentinel;

class ElFinderAccessManager
{
    public static function checkAccess($attr, $path, $data, $volume) {
        
        $user = Sentinel::getUser();
        if(empty($user)) {
            return -1;
        }
        $homePath = $user->home;


        if(!empty($homePath)  && !StringUtil::startsWith($path, $homePath)) {
            return -1;
        }
        return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
            ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
            :  null;                                    // else elFinder decide it itself
    }
}