<?php
namespace App\Http\Controllers\Admin;
use LaunchCMS\Http\Controllers\AdminController;
use Theme;
use View;
use Session;
use Widget;
class DashboardController extends AdminController
{
    public function show() {


        $widgetsInStatBlock = Widget::widgetsInBlock('dashboard', 'stats');
        $widgetsInMainBlock = Widget::widgetsInBlock('dashboard', 'main');
        $widgetsInSideBarBlock = Widget::widgetsInBlock('dashboard', 'sidebar');
        return View::make('pages.dashboard-page', [
                        'statWidgets' => $widgetsInStatBlock,
                        'mainWidgets' => $widgetsInMainBlock,
                        'sidebarWidgets' => $widgetsInSideBarBlock,
        ] );
    }


}