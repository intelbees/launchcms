<?php

namespace App\Http\Controllers\Admin;


use App\Http\Helpers\ClientLocalization;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Services\Facades\StructureService;
use Sentinel;
use Validator;
use View;

class DataTypeController extends AdminController
{
    private function initializeMessageForClient() {
        //add some common messages to use in client side
        ClientLocalization::putKeys([
            'launchcms.messages.cannot_use_local_storage',
            'launchcms.messages.processing',
        ]);
    }

    public function showDataTypeListingPage() {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type')) {
            abort(403, 'Unauthorized action.');
        }
        $this->initializeMessageForClient();
        $dataTypes = StructureService::getAllDataTypes();
        return View::make('pages.data-type-page', ['dataTypes' => $dataTypes
        ]);
    }



    public function showDataTypeEditPage($dataTypeAlias) {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type') || !$user->hasAccess('cms_access_data_type')) {
            abort(403, 'Unauthorized action.');
        }
        $this->initializeMessageForClient();
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeByAlias($dataTypeAlias);
        $fields = $dataType->getAllFields();
        $fieldTypesToShow = [];
        $fieldsInConfig =  config('launchcms.ui_fields');
        $dataTypesCannotBeShown = ['embedded_list', 'embedded'];
        foreach ($fieldsInConfig as $uiField) {
            if(!in_array($uiField['data_type'], $dataTypesCannotBeShown)) {
                $fieldTypesToShow[] = $uiField;
            }
        }
        $rowToShowFields = round(count($fieldTypesToShow)/ 3);

        if(count($fieldTypesToShow) % 3 !== 0) {
            $rowToShowFields += 1;
        }
        $allContentTypes = StructureService::getAllContentTypes();
        return View::make('pages.data-type-edit.index', [
                    'dataType' => $dataType,
                    'structure' => $dataType,
                    'attributeToHideInFieldMetaData' => ['field_group', 'is_unique'],
                    'allContentTypes' => $allContentTypes,
                    'locales' => ClientLocalization::$LOCALES,
                    'fields' => $fields,
                    'uiFields' => $fieldTypesToShow,
                    'rowToShowFields' => $rowToShowFields
        ]);
    }
}