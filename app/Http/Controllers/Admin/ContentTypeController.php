<?php

namespace App\Http\Controllers\Admin;


use App\Http\Helpers\ClientLocalization;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use Sentinel;
use View;

class ContentTypeController extends AdminController
{   protected static $ALL_CONTENT_TYPES = null;
    /** @var  StructureServiceInterface */
    protected $structureService;

    /** @var  ContentServiceInterface */
    protected $contentService;
    protected function initializeMessageForClient() {
        //add some common messages to use in client side
        ClientLocalization::putKeys(['launchcms.messages.cannot_use_local_storage',
            'launchcms.messages.processing',
        ]);
    }

    public function showContentTypeListingPage() {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content_type')) {
            abort(403, 'Unauthorized action.');
        }
        $this->initializeMessageForClient();
        return View::make('pages.content-type-page', []);
    }



    public function showContentTypeEditPage($contentTypeAlias) {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content_type') || !$user->hasAccess('cms_create_content_type')) {
            abort(403, 'Unauthorized action.');
        }
        $this->initializeMessageForClient();
        /** @var ContentType $contentType */
        $contentType = $this->structureService->getContentTypeByAlias($contentTypeAlias);
        
        $inheritContentType = $contentType->parent;
        $fields = $contentType->getAllFields();
        $fieldGroups = $contentType->getFieldGroups();

        $uiFields = config('launchcms.ui_fields');
        $rowToShowFields = floor(count($uiFields)/ 3);
        if(count($uiFields) % 3 !== 0) {
            $rowToShowFields += 1;
        }
        $allDataTypes = $this->structureService->getAllDataTypes();
        $allContentTypes = $this->structureService->getAllContentTypes();
        return View::make('pages.content-type-edit.index', [
                    'contentType' => $contentType,
                    'structure' => $contentType,
                    'attributeToHideInFieldMetaData' => [],
                    'allDataTypes' => $allDataTypes,
                    'allContentTypes' => $allContentTypes,
                    'locales' => ClientLocalization::$LOCALES,
                    'inheritContentType' => $inheritContentType,
                    'fields' => $fields,
                    'fieldGroups' => $fieldGroups,
                    'uiFields' => $uiFields,
                    'rowToShowFields' => $rowToShowFields
        ]);
    }
    protected  function getAllContentTypesInPageCache() {
        if(self::$ALL_CONTENT_TYPES == null) {
            self::$ALL_CONTENT_TYPES  =  $this->structureService->getAllContentTypes();
        }
        return self::$ALL_CONTENT_TYPES;
    }

    public function __construct(StructureServiceInterface $structureService, ContentServiceInterface $contentService)
    {
        $this->structureService = $structureService;
        $this->contentService = $contentService;
        parent::__construct();
        view()->composer(['pages.commons.structure-sidebar', 'pages.commons.content-type-selector'], function ($view) {
            $allContentTypes = $this->getAllContentTypesInPageCache();
            $view->with('contentTypes', $allContentTypes);
        });

        view()->composer('pages.commons.workflow-selector', function ($view) {
            $workflows = $this->structureService->getAllWorkflows();
            $view->with('workflows', $workflows);
        });
    }

}