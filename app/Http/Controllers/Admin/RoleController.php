<?php

namespace App\Http\Controllers\Admin;


use Activation;
use App\Http\Helpers\ContentTableJSONBuilder;
use App\Http\Helpers\FieldGroupViewObjectBuilder;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\User\Role;
use LaunchCMS\Services\Facades\PermissionService;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use Sentinel;
use View;

class RoleController extends ContentTypeBasedStructureController
{
    /** @var  StructureServiceInterface */
    protected $structureService;

    /** @var  ContentServiceInterface */
    protected $contentService;

    protected function buildUserDefaultFields()
    {
        $result = [ ];
        $result [] = [ 'alias' => 'name',
                       'name'  => trans('launchcms.content_builtin_field.name'),
                       'type'  => 'builtin' ];
        $result [] = [ 'alias' => 'slug',
                       'name'  => 'Slug',
                       'type'  => 'builtin' ];
        return $result;
    }

    public function roleListing()
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_role')) {
            abort(403, 'Unauthorized action.');
        }
        $contentType = $this->structureService->getContentTypeByAlias('cms_roles');
        $displayFields = self::getDisplayFieldsOnTableView($contentType, $this->buildUserDefaultFields());

        return View::make('pages.role-listing-page', [
            'contentType'   => $contentType,
            'displayFields' => $displayFields,
        ]);
    }

    public function showRoleEditPage($id)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_role') || !$user->hasAccess('cms_create_role')) {
            abort(403, 'Unauthorized action.');
        }
        $contentTypeAlias = 'cms_roles';
        /** @var ContentType $contentType */
        $contentType = $this->structureService->getContentTypeByAlias($contentTypeAlias);
        if(empty($contentType)) {
            abort(404, 'Content type '.$contentTypeAlias.' does not exist');
        }
        /** @var Content $content */
        $content = $this->contentService->getContentByID($contentTypeAlias, $id);
        if(empty($content)) {
            abort(404, 'This entity does not exist');
        }
        $fieldGroupViewObjectBuilder = new FieldGroupViewObjectBuilder();
        $fieldGroupViewObjects = $fieldGroupViewObjectBuilder->buildFieldGroupViewObjects($contentType);
        $dataTypesToPrepareForDialog = $contentType->getDataTypesRelatedToEditableFields();

        /** @var Role $role */
        $role = Role::where('_id', $id)->first();
        $permissions = PermissionService::getAllPermissions();
        $selectedPermissions = isset($role->permissions) ? $role->permissions : [] ;

        $allContentTypes = $this->structureService->getAllContentTypes();
        $contentTypes = [];
        /** @var ContentType $contentType */
        foreach ($allContentTypes as $contentTypeToAdd) {
            if(!$contentTypeToAdd->isBuiltInType()) {
                $contentTypes[] = $contentTypeToAdd;
            }
        }

        $allContentPermissions = PermissionService::getAllContentPermissions();
        return View::make('pages.role-edit.index', [
            'contentType'                  => $contentType,
            'content'                      => $content,
            'fieldGroupTabs'               => $fieldGroupViewObjects,
            'permissions'                  => $permissions,
            'selectedPermissions'          => $selectedPermissions,
            'dataTypesToPrepareForDialogs' => $dataTypesToPrepareForDialog,
            'contentTypes'            => $contentTypes,
            'allContentPermissions' => $allContentPermissions,
            'role' => $role

        ]);
    }

    public function __construct(StructureServiceInterface $structureService, ContentServiceInterface $contentService)
    {
        $this->structureService = $structureService;
        $this->contentService = $contentService;
        parent::__construct();
    }

}