<?php

namespace App\Http\Controllers\Admin;


use App\FieldEditors\DateTimeEditor;
use App\Http\Helpers\ContentTableJSONBuilder;
use App\Http\Helpers\FieldGroupViewObjectBuilder;
use App\Http\Helpers\HtmlWidgetHelper;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\User\User;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use LaunchCMS\Services\WorkflowEngine;
use Sentinel;
use View;

class ContentController extends ContentTypeBasedStructureController
{

    /** @var  StructureServiceInterface */
    protected $structureService;

    /** @var  ContentServiceInterface */
    protected $contentService;

    public function landing()
    {
        /** @var User $user */
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            abort(403, 'Unauthorized action');
        }
        $allContentTypes = $this->structureService->getAllContentTypes();

        return View::make('pages.content-landing-page', [
            'contentTypes' => $allContentTypes,
        ]);

    }

    public function contentListing($contentTypeAlias)
    {
        /** @var User $user */
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            abort(403, 'Unauthorized action');
        }
        $contentType = $this->structureService->getContentTypeByAlias($contentTypeAlias);
        $displayFields = self::getDisplayFieldsOnTableView($contentType);

        return View::make('pages.content-listing-page', [
            'contentType'   => $contentType,
            'displayFields' => $displayFields,
        ]);
    }


    public function showContentEditPage($contentTypeAlias, $id) {
        /** @var ContentType $contentType */
        $contentType = $this->structureService->getContentTypeFromCacheByAlias($contentTypeAlias);
        /** @var Content $content */
        $content = $this->contentService->getContentByID($contentTypeAlias, $id);
        $fieldGroupViewObjectBuilder = new FieldGroupViewObjectBuilder();
        $fieldGroupViewObjects = $fieldGroupViewObjectBuilder->buildFieldGroupViewObjects($contentType);
        $dataTypesToPrepareForDialog = $contentType->getDataTypesRelatedToEditableFields();
        /** @var WorkflowEngine $workflowEngine */
        $workflowEngine = $this->structureService->getWorkflowEngineFromContentType($contentType);
        $nextTransitions = $workflowEngine->getNextTransitions($content);
        foreach ($nextTransitions as &$transition) {
            if(!isset($transition['name']) && isset($transition['trans_key'])) {
                $transition['name'] = trans($transition['trans_key']);
            }
            if($transition[WorkflowEngine::TRANSITION_TO] !== WorkflowEngine::NIL_STATE) {
                $destinationStateAlias = $transition[WorkflowEngine::TRANSITION_TO];
                $destinationState = $workflowEngine->getState($destinationStateAlias);
                $transition['destination_system_state'] = $destinationState[WorkflowEngine::MAPPED_SYSTEM_STATE];
            }
        }

        $scheduledTime = $content->getWorkflowDataAttribute(WorkflowEngine::SCHEDULED_TIME);
        $currentState = $workflowEngine->getState($content->getWorkflowStatus());
        //TODO in case the current state is null because it's an old status and were removed in the workflow, what to do here?
        if(!empty($currentState)) {
            if(isset($currentState['trans_key'])) {
                $currentState['name'] = trans($currentState['trans_key']);
            }
        }
        $workflowNote = $content->getWorkflowDataAttribute('note');
        if(!empty($scheduledTime)) {
            $dateTimeEditor = new DateTimeEditor(null, null);
            $scheduledTime = $dateTimeEditor->createDisplayValue($scheduledTime);
        }
        $stateButtonWidgetHtml = HtmlWidgetHelper::buildWorkflowStatusButtonHtml($content->getWorkflowStatus(), $workflowEngine);
        return View::make('pages.content-edit.index', [
            'contentType'  => $contentType,
            'content'   => $content,
            'fieldGroupTabs' => $fieldGroupViewObjects,
            'dataTypesToPrepareForDialogs' => $dataTypesToPrepareForDialog,
            'nextTransitions' => $nextTransitions,
            'workflowNote' => $workflowNote,
            'scheduledTime' => $scheduledTime,
            'currentState' => $currentState,
            'stateButtonWidgetHtml' => $stateButtonWidgetHtml,
        ]);
    }

    public function __construct(StructureServiceInterface $structureService, ContentServiceInterface $contentService)
    {
        $this->structureService = $structureService;
        $this->contentService = $contentService;
        parent::__construct();
        view()->composer('pages.commons.content-sidebar', function ($view) {
            $allContentTypes = $this->structureService->getAllContentTypes();
            $filteredContentTypes = [];
            /** @var User $user */
            $user = Sentinel::getUser();
            /** @var ContentType $contentType */
            foreach ($allContentTypes as $contentType) {
                if($contentType->isBuiltInType()) {
                    continue;
                }
                if($user->canDoActions(['content.modify', 'content.delete', 'content.change_status'], (string) $contentType->_id)) {
                    $filteredContentTypes []= $contentType;
                }
            }
            $view->with('contentTypes', $filteredContentTypes);
        });
    }

}