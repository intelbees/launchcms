<?php

namespace App\Http\Controllers\Admin\Templates;
use Illuminate\Http\Request;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\User\User;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Exceptions\UserException;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Facades\UserService;
use Validator;

class UserContentAjaxProcessor extends DefaultContentAjaxProcessor
{
    protected function buildUserDefaultFields() {
        $result = [ ];
        $result [] = [ 'alias' => 'name',
                       'name'  => trans('launchcms.content_builtin_field.name'),
                       'type'  => 'builtin' ];
        $result [] = [ 'alias' => 'email',
                       'name'  => 'Email',
                       'type'  => 'builtin' ];
        $result [] = [ 'alias' => 'last_login.date',
                       'name'  => 'Last login',
                       'type'  => 'builtin' ];
        return $result;
    }

    protected function canDoContentOperation(User $user, ContentType $contentType) {
        if(!$user->hasAccess('cms_access_user') &&
            !$user->hasAccess('cms_create_user') &&
            !$user->hasAccess('cms_delete_user')) {
            return false;
        }
        return true;
    }

    protected function canCreateOrModifyContent(User $user, ContentType $contentType) {
        if(!$user->hasAccess('cms_create_user')) {
            return false;
        }
        return true;
    }

    protected function canDeleteContent(User $user, ContentType $contentType) {
        if(!$user->hasAccess('cms_delete_user')) {
            return false;
        }
        return true;
    }


    protected function buildEditURL($contentTypeAlias, $contentID)
    {
        return route('cms_user_edit', [ 'user_id' => $contentID]);

    }


    public function getTableDisplayFields(ContentType $contentType)
    {
        return self::getDisplayFieldsOnTableView($contentType, $this->buildUserDefaultFields());
    }

    public function createDraftContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $email = $request->input('email');
        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->home = $request->input('home');
        try {
            $user = UserService::createUser($user);
        } catch (UserException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success'     => true,
                                  'message'     => $this->getSuccessSavingMessage(),
                                  'content_url' => route('cms_user_edit', [ 'user_id' => (string) $user->_id ]),
        ]);
    }

    public function saveContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'                 => 'required',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $id = $request->input('id');
        $contentTypeAlias = 'cms_users';
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $id);
        $content->name = $request->input('name');
        $password = $request->input('password');
        $adminAccess = $request->input('admin_access');
        $adminAccess = $adminAccess === 'yes'? true: false;
        if($request->has('workflow_status')) {
            $content->setWorkflowStatus($request->input('workflow_status'));
        }
        $content->admin_user = $adminAccess;


        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        $this->putValuesFromRequestToContent($request, $contentType, $content);

        try {
            ContentService::saveContent($contentTypeAlias, $content);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }
        if(!empty($password)) {
            UserService::setUserPassword($id, $password);
        }

        $activated = $request->input('activated');
        if($activated==='activated') {
            $activated = true;
        } else {
            $activated = false;
        }

        $user = User::where('_id', $id)->first();
        if($request->has('permissions')) {
            $user->permissions = $request->input('permissions');
        } else {
            $user->permissions = [];
        }
        if($request->has('roles')) {
            $user->roles = $request->input('roles');
        } else {
            $user->roles = [];
        }
        $user->save();
        $oldActivatedFlag = UserService::isUserActivated($user);
        if($activated !== $oldActivatedFlag) {
            if($activated) {
                UserService::activateUser($user);
            } else {
                UserService::deactivateUser($user);
            }
        }


        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessSavingMessage(),

        ]);
    }


}