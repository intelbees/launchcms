<?php

namespace App\Http\Controllers\Admin\Templates;
use Illuminate\Http\Request;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\User\Role;
use LaunchCMS\Models\User\User;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Exceptions\UserException;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Facades\UserService;
use Validator;

class RoleContentAjaxProcessor extends DefaultContentAjaxProcessor
{
    protected function buildUserDefaultFields() {
        $result = [ ];
        $result [] = [ 'alias' => 'name',
                       'name'  => trans('launchcms.content_builtin_field.name'),
                       'type'  => 'builtin' ];
        $result [] = [ 'alias' => 'slug',
                       'name'  => 'Slug',
                       'type'  => 'builtin' ];
        return $result;
    }

    public function getTableDisplayFields(ContentType $contentType)
    {
        return self::getDisplayFieldsOnTableView($contentType, $this->buildUserDefaultFields());
    }

    protected function canDoContentOperation(User $user, ContentType $contentType) {
        if(!$user->hasAccess('cms_access_role') &&
            !$user->hasAccess('cms_create_role') &&
            !$user->hasAccess('cms_delete_role')) {
            return false;
        }
        return true;
    }

    protected function canCreateOrModifyContent(User $user, ContentType $contentType) {
        if(!$user->hasAccess('cms_create_role')) {
            return false;
        }
        return true;
    }

    protected function canDeleteContent(User $user, ContentType $contentType) {
        if(!$user->hasAccess('cms_delete_role')) {
            return false;
        }
        return true;
    }

    protected function buildEditURL($contentTypeAlias, $contentID)
    {
        return route('cms_role_edit', [ 'role_id' => $contentID]);

    }

    public function createDraftContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'slug' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $slug = $request->input('slug');
        try {
            $role = UserService::createRole($name, $slug);
        } catch (UserException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success'     => true,
                                  'message'     => $this->getSuccessSavingMessage(),
                                  'content_url' => route('cms_role_edit', [ 'role_id' => (string) $role->_id ]),
        ]);
    }

    public function saveContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'                 => 'required',
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $id = $request->input('id');
        $contentTypeAlias = 'cms_roles';
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $id);
        $content->name = $request->input('name');

        if($request->has('workflow_status')) {
            $content->setWorkflowStatus($request->input('workflow_status'));
        }
        $content->slug = $request->input('slug');


        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        $this->putValuesFromRequestToContent($request, $contentType, $content);

        try {
            ContentService::saveContent($contentTypeAlias, $content);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }
        /** @var Role $role */
        $role = Role::where('_id', $id)->first();
        if($request->has('permissions')) {
            $permissions = $request->input('permissions');
            $role->permissions = [];
            foreach ($permissions as $permission) {
                $role->addPermission($permission, true);
            }
        } else {
            $role->permissions = [];
        }
        if($request->has('content_permission_matrix')) {
            $role->setContentPermissionMatrix($request->input('content_permission_matrix'));
        } else {
            $role->setContentPermissionMatrix([]);
        }
        $role->save();
        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessSavingMessage(),

        ]);
    }


}