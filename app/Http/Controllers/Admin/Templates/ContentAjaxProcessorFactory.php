<?php

namespace App\Http\Controllers\Admin\Templates;


class ContentAjaxProcessorFactory
{
    public static function getProcessorTemplate($contentTypeAlias) {
        
        if($contentTypeAlias === 'cms_users') {
            return new UserContentAjaxProcessor();
        }
        else if($contentTypeAlias === 'cms_roles') {
            return new RoleContentAjaxProcessor();
        }
        return new DefaultContentAjaxProcessor();
    }
}