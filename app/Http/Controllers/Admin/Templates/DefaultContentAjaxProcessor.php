<?php

namespace App\Http\Controllers\Admin\Templates;

use App\FieldEditors\DateTimeEditor;
use App\FieldEditors\EditorFactory;
use Illuminate\Http\Request;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\User\User;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Utils\ArrayUtil;
use Sentinel;
use Validator;
use LaunchCMS\Services\WorkflowEngine;
class DefaultContentAjaxProcessor extends ContentAjaxProcessorTemplate
{

    protected function getSuccessSavingMessage()
    {
        return trans('launchcms.content_edit_screen.messages.content_save_successfully');
    }

    protected function getSuccessDeletionMessage()
    {
        return trans('launchcms.content_edit_screen.messages.content_delete_successfully');
    }

    protected function canDoContentOperation(User $user, ContentType $contentType) {
        if(!$contentType->isBuiltInType() && !$user->canDoActions(['content.delete', 'content.modify',
                'content.change_status'], (string) $contentType->_id)) {
            return false;
        }
        return true;
    }

    protected function canCreateOrModifyContent(User $user, ContentType $contentType) {
        if(!$contentType->isBuiltInType() && !$user->canDoActions(['content.modify'], (string) $contentType->_id)) {
            return false;
        }
        return true;
    }

    protected function canDeleteContent(User $user, ContentType $contentType) {
        if(!$contentType->isBuiltInType() && !$user->canDoActions(['content.delete'], (string) $contentType->_id)) {
            return false;
        }
        return true;
    }

    protected function canChangeContentStatus(User $user, ContentType $contentType) {
        if(!$contentType->isBuiltInType() && !$user->canDoActions(['content.change_status'], (string) $contentType->_id)) {
            return false;
        }
        return true;
    }

    public function listContent($contentTypeAlias, Request $request)
    {
        /** @var User $user */
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if(empty($contentType)) {
            return response('Invalid content type.', 404);
        }
        if(!$this->canDoContentOperation($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        $queryBuilder = ContentService::getQueryBuilder($contentTypeAlias);
        $returnedArray = [ ];
        $pageSize = 5;
        $currentPage = 1;
        if ($request->has('current')) {
            $currentPage = intval($request->input('current'));
        }

        $numberOfItemToSkip = $this->buildPaginationJSONResult($pageSize, $currentPage, $queryBuilder, $returnedArray);
        $data = $queryBuilder->skip($numberOfItemToSkip)->take($pageSize)->orderBy('created_time', 'desc')->get();


        $resultData = $this->buildJSONTableData($contentType, $data);


        $returnedArray[ 'success' ] = true;
        $returnedArray[ 'result_count' ] = count($data);
        $returnedArray[ 'result' ] = $resultData[ 'result' ];
        $returnedArray[ 'display_fields' ] = $resultData[ 'display_fields' ];

        return response()->json($returnedArray);

    }

    public function createDraftContent(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'content_type_alias' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if(!$this->canCreateOrModifyContent($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        $name = $request->input('name');
        $id = null;
        try {
            $id = ContentService::createDraftContent($contentTypeAlias, $name);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }

        return response()->json([ 'success'     => true,
                                  'message'     => $this->getSuccessSavingMessage(),
                                  'content_url' => route('cms_content_edit', [ 'content_type_alias' => $contentTypeAlias, 'content_id' => $id ]),
        ]);
    }

    public function deleteContent(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'id'                 => 'required',
            'content_type_alias' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if(!$this->canDeleteContent($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        $id = $request->input('id');
        try {
            ContentService::deleteContentByID($contentTypeAlias, $id);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessDeletionMessage(),

        ]);
    }

    protected function putValuesFromRequestToContent(Request $request, ContentType $contentType, Content &$content) {

        $allFields = $contentType->getAllFields();

        /** @var Field $field */
        foreach ($allFields as $field) {
            if ($field->getDataType() === Field::EMBEDDED_LIST) {
                continue;
            }
            $fieldAlias = $field->getAlias();

            $dataOfFieldOnRequest = $request->input($fieldAlias);
            $fieldEditor = EditorFactory::getEditor($field);
            if ( !empty( $fieldEditor )) {
                $valueToSave = $fieldEditor->createPersistValue($dataOfFieldOnRequest);
                $content->attributes[ $fieldAlias ] = $valueToSave;
            }
        }
        
    }
    public function saveContent(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'id'                 => 'required',
            'content_type_alias' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $id = $request->input('id');
        $contentTypeAlias = $request->input('content_type_alias');
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if(!$this->canCreateOrModifyContent($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $id);
        $content->name = $request->input('name');
        if($request->has('workflow_status')) {
            $content->setWorkflowStatus($request->input('workflow_status'));
        }

        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        $this->putValuesFromRequestToContent($request, $contentType, $content);

        try {
            ContentService::saveContent($contentTypeAlias, $content);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessSavingMessage(),

        ]);
    }

    public function searchContentByName(ContentType $contentType, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'q' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $q = $request->input('q');
        $queryBuilder = ContentService::getQueryBuilder($contentType->getAlias());
        $data = $queryBuilder->where('name', 'like', $q . '%')->get();
        $returnedData = [ ];
        foreach ($data as $dataItem) {
            $returnedData[] = [ 'id' => (string) $dataItem[ '_id' ], 'name' => $dataItem[ 'name' ] ];
        }

        return response()->json($returnedData);
    }

    public function deleteEmbeddedRecord(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'content_id'         => 'required',
            'content_type_alias' => 'required',
            'field_alias'        => 'required',
            'record_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }

        $contentTypeAlias = $request->input('content_type_alias');
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if(!$this->canCreateOrModifyContent($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        $fieldAlias = $request->input('field_alias');;
        $contentID = $request->input('content_id');;
        $recordID = $request->input('record_id');;

        $rowIndexToDelete = -1;
        $index = 0;
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $contentID);
        $persistRows = null;
        if (isset( $content->attributes[ $fieldAlias ] )) {
            $persistRows = $content->attributes[ $fieldAlias ];
        }
        if (empty( $persistRows )) {
            return response()->json([ 'success' => true,
                                      'message' => $this->getSuccessSavingMessage(),

            ]);
        }
        if ( !empty( $persistRows )) {
            foreach ($persistRows as $persistRow) {
                if ($persistRow[ '_id' ] === $recordID) {
                    $rowIndexToDelete = $index;
                    break;
                }
                $index++;
            }
        }


        if ($rowIndexToDelete >= 0) {
            ArrayUtil::removeElementAtIndex($rowIndexToDelete, $persistRows);
            $content->attributes[ $fieldAlias ] = $persistRows;
        }
        try {
            ContentService::saveContent($contentTypeAlias, $content);
        } catch (CMSServiceException $ex) {
            if ($ex->getCode() === ContentException::INVALID_CONTENT) {

            }

            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessSavingMessage(),

        ]);
    }

    public function getEmbeddedRecords($contentTypeAlias, $fieldAlias, $contentID)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $contentID);
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);

        if($this->canDoContentOperation($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        /** @var Field $field */
        $field = $contentType->getField($fieldAlias);

        $dataTypeID = $field->getExtraDataTypeInfo()[ Field::DATA_TYPE_ID ];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);

        /** @var Field $editableFields */
        $editableFields = $dataType->getEditableFields();
        $persistRows = [ ];
        if (isset( $content->attributes[ $fieldAlias ] )) {
            $persistRows = $content->attributes[ $fieldAlias ];
        }
        $displayRows = [ ];
        if ( !empty( $persistRows )) {
            foreach ($persistRows as $persistRow) {
                $displayRow = [ ];
                $displayRow[ 'id' ] = $persistRow[ '_id' ];
                foreach ($editableFields as $editableField) {
                    $alias = $editableField->getAlias();
                    $fieldEditor = EditorFactory::getEditor($editableField);
                    $dataColumnInDB = $persistRow[ $alias ];
                    $displayValue = $fieldEditor->createDisplayValue($dataColumnInDB);
                    $displayRow[ $alias ] = $displayValue;
                }
                $displayRows [] = $displayRow;
            }
        }
        $editableFieldMetaData = [ ];
        /** @var Field $editableField */
        foreach ($editableFields as $editableField) {
            $fieldMetaData = [ ];
            $fieldMetaData[ 'alias' ] = $editableField->getAlias();
            $fieldMetaData[ 'type' ] = $editableField->getDataType();
            $fieldMetaData[ 'settings' ] = $editableField->getExtraDataTypeInfo();
            $editableFieldMetaData[] = $fieldMetaData;
        }

        return response()->json([ 'success' => true,
                                  'data'    => $displayRows,
                                  'fields'  => $editableFieldMetaData,

        ]);

    }

    public function saveEmbeddedRecord(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'content_id'         => 'required',
            'content_type_alias' => 'required',
            'field_alias'        => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $contentID = $request->input('content_id');
        $contentTypeAlias = $request->input('content_type_alias');
        $fieldAlias = $request->input('field_alias');
        $recordID = null;
        if ($request->has('record_id')) {
            $recordID = $request->input('record_id');
        }

        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $contentID);
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        if(empty($contentType)) {
            return response('Invalid content type.', 404);
        }
        if(!$this->canCreateOrModifyContent($user, $contentType)) {
            return response('Unauthorized.', 403);
        }

        /** @var Field $field */
        $field = $contentType->getField($fieldAlias);
        $dataTypeID = $field->getExtraDataTypeInfo()[ Field::DATA_TYPE_ID ];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);

        /** @var Field $editableFields */
        $editableFields = $dataType->getEditableFields();
        if ( !isset( $content->attributes[ $fieldAlias ] )) {
            $content->attributes[ $fieldAlias ] = [ ];
        }

        if ( !empty( $recordID )) {
            foreach ($content->attributes[ $fieldAlias ] as &$row) {
                if ($row[ '_id' ] === $recordID) {
                    $this->buildEmbeddedRecordRowFromRequest($request, $editableFields, $row);
                    break;
                }
            }
        } else {
            $rowToInsert = [ ];
            $this->buildEmbeddedRecordRowFromRequest($request, $editableFields, $rowToInsert);
            $content->attributes[ $fieldAlias ] [] = $rowToInsert;
        }
        try {
            ContentService::saveContent($contentTypeAlias, $content);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessSavingMessage(),

        ]);

    }

    public function changeContentWorkflowStatus(Request $request) {
        /** @var User $user */
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_content')) {
            return response('Unauthorized.', 403);
        }
        $contentID = $request->input('content_id');
        $contentTypeAlias = $request->input('content_type_alias');
        $nextState = $request->input('next_state');
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        if(empty($contentType)) {
            return response('Invalid content type.', 404);
        }

        if(!$this->canCreateOrModifyContent($user, $contentType)) {
            return response('Unauthorized.', 403);
        }
        if(!$this->canChangeContentStatus($user, $contentType)) {
            return response('Unauthorized.', 403);
        }

        $workflowData = $request->input('workflow_data');

        if(!empty($workflowData)) {

            //TODO in the future, we should allow user to define field in workflow data and remove this kind of hard coding logic
            if(isset($workflowData[WorkflowEngine::SCHEDULED_TIME])) {
                $requestValue = $workflowData[WorkflowEngine::SCHEDULED_TIME];
                $dateTimeEditor = new DateTimeEditor(null, null);
                $workflowData[WorkflowEngine::SCHEDULED_TIME] = $dateTimeEditor->createPersistValue($requestValue);
            }
        }

        try {
            ContentService::changeContentWorkflowStatus($contentTypeAlias, $contentID, $nextState, $workflowData);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success'       => false,
                                      'error'         => $ex->getTranslatedMessage(),
                                      'error_code'    => $ex->getCode(),
                                      'detail_errors' => $ex->getErrors(),
            ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => $this->getSuccessSavingMessage(),

        ]);
    }
}