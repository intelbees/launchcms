<?php

namespace App\Http\Controllers\Admin\Templates;
use App\FieldEditors\EditorFactory;
use App\Http\Helpers\HtmlWidgetHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\WorkflowSystemStatus;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\WorkflowEngine;
use Validator;


abstract class ContentAjaxProcessorTemplate
{
    protected function buildPaginationJSONResult($pageSize, $currentPage, $queryBuilder, array &$result)
    {
        $totalItem = $queryBuilder->count();
        $numberOfPage = floor($totalItem / $pageSize);
        if ($totalItem % $pageSize !== 0) {
            $numberOfPage += 1;
        }

        $result[ 'total_page' ] = $numberOfPage;
        $result[ 'total_record' ] = $totalItem;
        $result[ 'current_page' ] = $currentPage;
        $result[ 'page_size' ] = $pageSize;
        $previousPage = ( $currentPage - 1 ) >= 0 ? ( $currentPage - 1 ) : 0;
        $result[ 'prev_page' ] = $previousPage > 0 ? $previousPage : null;
        $result[ 'next_page' ] = $currentPage + 1 <= $numberOfPage ? $currentPage + 1 : null;
        $numberOfItemToSkip = $previousPage * $pageSize;

        return $numberOfItemToSkip;
    }
    public function getEmbeddedRecords($contentTypeAlias, $fieldAlias, $contentID)
    {
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $contentID);
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        /** @var Field $field */
        $field = $contentType->getField($fieldAlias);

        $dataTypeID = $field->getExtraDataTypeInfo()[ Field::DATA_TYPE_ID ];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);

        /** @var Field $editableFields */
        $editableFields = $dataType->getEditableFields();
        $persistRows = [ ];
        if (isset( $content->attributes[ $fieldAlias ] )) {
            $persistRows = $content->attributes[ $fieldAlias ];
        }
        $displayRows = [ ];
        if ( !empty( $persistRows )) {
            foreach ($persistRows as $persistRow) {
                $displayRow = [ ];
                $displayRow[ 'id' ] = $persistRow[ '_id' ];
                foreach ($editableFields as $editableField) {
                    $alias = $editableField->getAlias();
                    $fieldEditor = EditorFactory::getEditor($editableField);
                    $dataColumnInDB = $persistRow[ $alias ];
                    $displayValue = $fieldEditor->createDisplayValue($dataColumnInDB);
                    $displayRow[ $alias ] = $displayValue;
                }
                $displayRows [] = $displayRow;
            }
        }
        $editableFieldMetaData = [ ];
        /** @var Field $editableField */
        foreach ($editableFields as $editableField) {
            $fieldMetaData = [ ];
            $fieldMetaData[ 'alias' ] = $editableField->getAlias();
            $fieldMetaData[ 'type' ] = $editableField->getDataType();
            $fieldMetaData[ 'settings' ] = $editableField->getExtraDataTypeInfo();
            $editableFieldMetaData[] = $fieldMetaData;
        }

        return response()->json([ 'success' => true,
                                  'data'    => $displayRows,
                                  'fields'  => $editableFieldMetaData,

        ]);

    }

    protected function buildEmbeddedRecordRowFromRequest(Request $request, $editableFields, array &$row)
    {
        foreach ($editableFields as $editableField) {
            $alias = $editableField->getAlias();
            if (!$request->has($alias)) {
                continue;
            }
            $fieldValueFromRequest = $request->input($alias);
            $editor = EditorFactory::getEditor($editableField);
            if(!empty($editor)) {
                $row[ $alias ] = $editor->createPersistValue($fieldValueFromRequest);
            }
        }
    }


    public function buildJSONTableData(ContentType $contentType, array $returnRows)
    {
        $result = [ ];
        $displayFields = $this->getTableDisplayFields($contentType);
        $referenceData = [ ];
        foreach ($returnRows as $dataItem) {
            $result [] = $this->buildRawReturnedContentDataRow($contentType, $dataItem, $displayFields, $referenceData);
        }
        $this->addMoreDisplayFieldToResult($result, $referenceData, $displayFields, $contentType);
        $output = [ ];
        $output[ 'result' ] = $result;
        $output[ 'display_fields' ] = $displayFields;

        return $output;
    }
    protected function buildEditURL($contentTypeAlias, $contentID) {
        return route('cms_content_edit', [ 'content_type_alias' => $contentTypeAlias, 'content_id' => $contentID ]);
    }

    protected function buildRawReturnedContentDataRow(ContentType $contentType, array $row, array $displayFields, array &$referenceData)
    {
        $returnData = [ ];
        $content = Content::fromArray($row);
        /** @var WorkflowEngine $workflowEngine */
        $workflowEngine = StructureService::getWorkflowEngineFromContentType($contentType);
        foreach ($displayFields as $displayField) {
            $alias = $displayField[ 'alias' ];
            $valueFromDB = $content->attributeValue($alias);
            $field = $contentType->getField($alias);
            $fieldEditor = null;
            if(!empty($field)) {
                $fieldEditor = EditorFactory::getEditor($field);
            }

            $displayValue = null;
            if ( !empty( $fieldEditor )) {
                $displayValue = $fieldEditor->createDisplayValue($valueFromDB);
            } else {
                $displayValue = $valueFromDB;
            }

            $returnData[ $alias ] = $displayValue;
            if ($displayField[ 'type' ] !== Field::REFERENCE) {
                continue;
            }
            if ( !isset( $reference_data[ $alias ] )) {
                $referenceData[ $alias ] = [ ];
            }
            if (isset( $row[ $alias ] ) && !empty( $row[ $alias ] )) {
                $referenceContentID = $row[ $alias ][ 'value' ];
                if ( !in_array($referenceContentID, $referenceData[ $alias ])) {
                    $referenceData[ $alias ] [] = $referenceContentID;
                }
            }
        }
        $id = (string) $content->id;
        $returnData[ '_id' ] = $id;
        $returnData[ 'name' ] = $content->name;
        $returnData[ 'created_time' ] = $content->createdTime->diffForHumans();
        $returnData[ 'last_updated_time' ] = $content->lastUpdatedTime->diffForHumans();
        $workflowStatus = $content->getWorkflowStatus();
        $returnData[ 'workflow_status' ] = HtmlWidgetHelper::buildWorkflowStatusButtonHtml($workflowStatus, $workflowEngine);
        $returnData[ 'mapped_system_status' ] = $content->getMappedSystemStatus();
        $returnData[ 'edit_url' ] = $this->buildEditURL($contentType->getAlias(), $id);

        return $returnData;
    }
    
    

    public function getTableDisplayFields(ContentType $contentType) {
        return self::getDisplayFieldsOnTableView($contentType, []);
    }

    public static function getDisplayFieldsOnTableView(ContentType $contentType, $defaultFields = [])
    {
        $settings = $contentType->getSettings();
        $displayedFields = isset( $settings[ 'table_display_fields' ] ) ? $settings[ 'table_display_fields' ] : [ ];
        $result = [ ];
        if (empty( $displayedFields )) {

            if(empty($defaultFields)) {
                $result [] = [ 'alias' => 'name',
                               'name'  => trans('launchcms.content_builtin_field.name'),
                               'type'  => 'builtin' ];
                $result [] = [ 'alias' => 'workflow_status',
                               'name'  => trans('launchcms.content_builtin_field.workflow_status'),
                               'type'  => 'builtin' ];
            } else {
                $result = $defaultFields;
            }
            return $result;
        }
        foreach ($displayedFields as $fieldAlias) {
            if (Content::isBuiltInField($fieldAlias)) {
                $result[] = [ 'alias' => $fieldAlias,
                              'name'  => trans('launchcms.content_builtin_field.' . $fieldAlias),
                              'type'  => 'builtin' ];

                continue;
            }
            /** @var Field $field */
            $field = $contentType->getField($fieldAlias);
            $fieldInfo = [ 'alias' => $fieldAlias,
                           'name'  => $field->getName(),
                           'type'  => $field->getDataType() ];
            if ($field->getDataType() === Field::REFERENCE) {
                $contentTypeID = $field->getExtraDataTypeInfo()[ Field::CONTENT_TYPE_ID ];
                $fieldInfo[ 'ref_content_type_id' ] = $contentTypeID;
            }
            $result[] = $fieldInfo;
        }

        return $result;
    }

    protected function buildContentDictionary(array $referenceData, ContentType $contentType)
    {
        if (empty( $referenceData )) {
            return [ ];
        }
        $contentTypeData = [ ];
        //we need to merge content ID array from different fields to remove duplicated querying
        foreach ($referenceData as $fieldAlias => $data) {
            /** @var Field $field */
            $field = $contentType->getField($fieldAlias);
            $extraInfo = $field->getExtraDataTypeInfo();
            $contentTypeID = $extraInfo[ Field::CONTENT_TYPE_ID ];
            if ( !isset( $contentTypeData[ $contentTypeID ] )) {
                $contentTypeData[ $contentTypeID ] = [ ];
            }
            $contentTypeData[ $contentTypeID ] = array_merge($contentTypeData[ $contentTypeID ], $data);
            $contentTypeData[ $contentTypeID ] = array_unique($contentTypeData[ $contentTypeID ], SORT_REGULAR);
        }
        $finalContentDictionary = [ ];
        //now, we do query on each ContentType and merge final result to a 'content dictionary' with key is '_id'
        foreach ($contentTypeData as $contentTypeID => $idContentData) {
            /** @var ContentType $refContentType */
            $refContentType = StructureService::getContentTypeFromCacheByID($contentTypeID);
            $collectionName = $refContentType->rootContentType()->getAlias();
            if (empty( $idContentData )) {
                continue;
            }
            $records = DB::collection($collectionName)->whereIn('_id', $idContentData);
            foreach ($records as $record) {
                $id = (string) $record[ '_id' ];
                $key = $contentTypeID . '_' . $id;

                if ( !isset( $finalContentDictionary[ $key ] )) {
                    $finalContentDictionary[ $key ] = $record;
                }
            }
        }

        return $finalContentDictionary;
    }

    protected function addMoreDisplayFieldToResult(array &$result, array $referenceData, $displayFields, ContentType $contentType)
    {
        $finalContentDictionary = $this->buildContentDictionary($referenceData, $contentType);
        $referenceFields = [ ];
        foreach ($displayFields as $field) {
            if ($field[ 'type' ] === Field::REFERENCE) {
                $referenceFields[] = $field;
            }
        }
        if (empty( $referenceFields )) {
            return;
        }
        foreach ($result as &$row) {
            foreach ($referenceFields as $field) {
                $alias = $field[ 'alias' ];
                $contentID = !empty( $row[ $alias ] ) ? $row[ $alias ] : '';
                $refContentTypeID = $field[ 'ref_content_type_id' ];
                $keyToGetContentObject = $refContentTypeID . '_' . $contentID;
                $content = isset( $finalContentDictionary[ $keyToGetContentObject ] ) ? $finalContentDictionary[ $keyToGetContentObject ] : null;
                if (empty( $content )) {
                    $row[ $alias . '_text' ] = '';
                } else {
                    $row[ $alias . '_text' ] = $content[ 'name' ];
                }
            }
        }

    }

    public abstract function listContent($contentTypeAlias, Request $request);
    public abstract function createDraftContent(Request $request);
    public abstract function deleteContent(Request $request);
    public abstract function saveContent(Request $request);
    public abstract function searchContentByName(ContentType $contentType, Request $request);
    public abstract function deleteEmbeddedRecord(Request $request);
    public abstract function saveEmbeddedRecord(Request $request);
    public abstract function changeContentWorkflowStatus(Request $request);
}