<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Facades\StructureService;
use Validator;
use Sentinel;


class DataTypeAjaxController extends StructureAjaxController
{
    protected function getDataTypeDataByAlias($dataTypeAlias)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type')) {
            return response('Unauthorized.', 403);
        }
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeByAlias($dataTypeAlias);
        if (empty( $dataType )) {
            return null;
        }
        $result = $this->buildReturnedDataFromStructureObject($dataType);
        return $result;
    }

    public function listDataTypes()
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type')) {
            return response('Unauthorized.', 403);
        }
        $dataTypes = StructureService::getAllDataTypes();
        $result = [ ];
        foreach ($dataTypes as $dataType) {
            $result [] = [
                'name'        => $dataType->getName(),
                'alias'       => $dataType->getAlias(),
                'description' => $dataType->getDescription(),
                'builtin'     => $dataType->isBuiltInType(),
                'edit_url'    => route('cms_edit_data_type_page', [ 'alias' => $dataType->alias ]),
            ];
        }

        return response()->json([ 'success' => true, 'data' => $result ]);
    }

    public function saveDataTypeSettings(Request $request) {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type') || !$user->hasAccess('cms_create_data_type')) {
            return response('Unauthorized.', 403);
        }
        $dataTypeAlias = $request->input('data_type_alias');
        $contentInfoToUpdate = [ 'settings' => []];
        if ($request->has('table_display_fields')) {
            $contentInfoToUpdate[ 'settings' ] ['table_display_fields'] = $request->input('table_display_fields');
        }
        
        if ($request->has('editable_fields')) {
            $contentInfoToUpdate[ 'settings' ] ['editable_fields'] = $request->input('editable_fields');
        }

        if(!empty($contentInfoToUpdate['settings'])) {
            StructureService::updateDataTypeInfo($dataTypeAlias, $contentInfoToUpdate);
        }
        return response()->json([ 'success'          => true,
                                  'message'          => trans('launchcms.data_types_screen.messages.data_type_save_successfully'),
                                  'alias_changed'    => false,
        ]);

    }
    public function saveDataType(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type') || !$user->hasAccess('cms_create_data_type')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $alias = $request->input('alias');
        $dataTypeAlias = $request->input('data_type_alias');
        $description = $request->input('description');

        $dataTypeInfo = [ 'name'        => $name,
                             'alias'       => $alias,
                             'description' => $description,
        ];
        $aliasChanged = false;
        $dataTypeObj = null;
        try {
            if (empty( $dataTypeAlias )) {
                $dataTypeObj = StructureService::createDataType($dataTypeInfo);
            } else {
                $dataTypeObj = StructureService::updateDataTypeInfo($dataTypeAlias, $dataTypeInfo);
                $aliasChanged = ( $dataTypeAlias !== $dataTypeObj->getAlias() );
            }

        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success'          => true,
                                  'message'          => trans('launchcms.data_types_screen.messages.data_type_save_successfully'),
                                  'alias_changed'    => $aliasChanged,
                                  'data_type_url' => route('cms_edit_data_type_page', $dataTypeObj->getAlias()) ]);
    }



    public function getDataTypeData($dataTypeAlias)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type')) {
            return response('Unauthorized.', 403);
        }
        $dataTypeData = $this->getDataTypeDataByAlias($dataTypeAlias);
        if (empty( $dataTypeData )) {
            return response()->json([ 'success' => false, 'error' => trans('launchcms.cms_errors.data_type_not_found') ]);
        }

        return response()->json([ 'success' => true, 'data' => $dataTypeData ]);
    }


    public function deleteDataType(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type') || !$user->hasAccess('cms_delete_data_type')) {
            return response('Unauthorized.', 403);
        }
        $dataTypeAlias = $request->input('alias');
        try {
            StructureService::deleteDataTypeByAlias($dataTypeAlias);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.data_types_screen.messages.data_type_deleted_successfully') ]);

    }


    public function saveField(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type') || !$user->hasAccess('cms_create_data_type')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }

        $fieldAlias = $request->input('field_alias');
        $structureAlias = $request->input('structure_type_alias');
        $dataType = $request->input('data_type');
        /** @var DataType $dataType */
        $structureObject = StructureService::getDataTypeByAlias($structureAlias);
        $field = null;
        if (empty( $fieldAlias )) {
            $field = new Field();
            $field->setDataType($dataType);
        } else {
            $field = $structureObject->getOwnField($fieldAlias);
        }

        $name = $request->input('name');
        $field->setName($name);
        $alias = $request->input('alias');
        $field->setAlias($alias);
        $description = $request->input('description');
        $field->setDescription($description);
        $position = $request->input('position');
        if(empty($position)) {
            $position = 0;
        }
        $field->setPosition($position);

        $field->setIndex($request->has('is_indexed'));
        $field->setAllowFullTextSearch($request->has('allow_full_text_search'));
        $field->setUnique($request->has('is_unique'));
        $field->setRequired($request->has('is_required'));
        if ($request->has('validation_rules')) {
            try {
                $field->setValidationRules($request->input('validation_rules'));
            } catch (CMSServiceException $ex) {
                return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
            }
        }
        if ($request->has('settings')) {
            $settings = $request->input('settings');
            $field->setExtraDataTypeInfo($settings);
        }

        try {
            if (empty( $fieldAlias )) {
                StructureService::addFieldToDataType($structureAlias, $field);
            } else {
                StructureService::updateFieldOfDataType($structureAlias, $fieldAlias, $field);
            }
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.data_type_edit_screen.messages.field_save_successfully') ]);

    }

    public function deleteField(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_data_type') || !$user->hasAccess('cms_create_data_type')) {
            return response('Unauthorized.', 403);
        }
        $dataTypeAlias = $request->input('data_type_alias');
        $fieldAlias = $request->input('field_alias');
        try {
            StructureService::deleteFieldOfDataType($dataTypeAlias, $fieldAlias, true);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.data_type_edit_screen.messages.field_deleted_successfully') ]);

    }
}