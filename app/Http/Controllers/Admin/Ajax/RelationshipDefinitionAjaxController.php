<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\User\RelationshipDefinition;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Facades\UserService;
use Sentinel;
use Validator;


class RelationshipDefinitionAjaxController extends AdminController
{

    protected function buildReturnRelationshipData(RelationshipDefinition $dbRelationshipObj) {
        return [
            'name' => $dbRelationshipObj->getName(),
            'alias' => $dbRelationshipObj->getAlias(),
            'inverse' => $dbRelationshipObj->inverse()? 'yes': 'no',
            'need_approval' => $dbRelationshipObj->needApproval() ? 'yes':'no',

        ];
    }
    protected function getRelationshipDefinitionByAlias($alias)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_relationship_definition')) {
            return response('Unauthorized.', 403);
        }

        $relationshipDefinition = UserService::getRelationshipDefinitionByAlias($alias);
        if (empty( $relationshipDefinition )) {
            return null;
        }
        return $this->buildReturnRelationshipData($relationshipDefinition);
    }

    public function listRelationshipDefinition()
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_relationship_definition')) {
            return response('Unauthorized.', 403);
        }
        $relationshipDefinitions = UserService::getAllRelationshipDefinitions();
        $result = [ ];
        /** @var RelationshipDefinition $relationshipDefinition */
        foreach ($relationshipDefinitions as $relationshipDefinition) {
            $result [] = $this->buildReturnRelationshipData($relationshipDefinition);
        }

        return response()->json([ 'success' => true, 'data' => $result ]);
    }



    public function saveRelationshipDefinition(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_create_relationship_definition')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $alias = $request->input('alias');
        $relationshipDefinitionAlias = $request->input('relation_definition_alias');

        $inverse = $request->has('inverse');
        $needApproval = $request->has('need_approval');
        $relationshipInfo = [ 'name'                => $name,
                             'alias'               => $alias,
                             'inverse'   => $inverse,
                             'need_approval' => $needApproval,
                             ];
        $relationshipDefinitionObj = null;
        try {
            if (empty( $relationshipDefinitionAlias )) {
                UserService::createRelationshipDef($relationshipInfo);
            } else {
                UserService::updateRelationshipDef($relationshipDefinitionAlias, $relationshipInfo);

            }

        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success'          => true,
                                  'message'          => trans('launchcms.relationship_definition_screen.messages.relationship_save_successfully'),
                                  ]);
    }



    public function getRelationshipDefinitionData($alias)
    {
        $returnData = $this->getRelationshipDefinitionByAlias($alias);
        if (empty( $returnData )) {
            return response()->json([ 'success' => false, 'error' => trans('launchcms.cms_errors.relationship_not_found') ]);
        }

        return response()->json([ 'success' => true, 'data' => $returnData ]);
    }


    public function deleteRelationshipDefinition(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_delete_relationship_definition')) {
            return response('Unauthorized.', 403);
        }
        $alias = $request->input('alias');
        try {
            UserService::deleteRelationshipDefByAlias($alias);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.relationship_definition_screen.messages.relationship_deleted_successfully') ]);

    }


}