<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\FieldGroup;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Facades\StructureService;
use Validator;
use Sentinel;


class ContentTypeAjaxController extends StructureAjaxController
{

    protected function getContentTypeDataByAlias($contentTypeAlias)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_access_content_type')) {
            return response('Unauthorized.', 403);
        }
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            return null;
        }
        $result = $this->buildReturnedDataFromStructureObject($contentType);
        $result[ 'field_groups' ] = [ ];
        $fieldGroups = $contentType->getFieldGroups();

        if ( !empty( $fieldGroups )) {
            foreach ($fieldGroups as $fieldGroup) {
                $result[ 'field_groups' ][] = [ 'name'        => $fieldGroup->getName(),
                                                'alias'       => $fieldGroup->getAlias(),
                                                'description' => $fieldGroup->getDescription(),
                                                'position'    => $fieldGroup->getPosition() ];

            }
        }

        $ownFieldGroups = $contentType->fieldGroups;
        $result[ 'own_field_groups' ] = [];
        if ( !empty( $ownFieldGroups )) {
            foreach ($ownFieldGroups as $fieldGroup) {
                $result[ 'own_field_groups' ][] = [ 'name'        => $fieldGroup->getName(),
                                                'alias'       => $fieldGroup->getAlias(),
                                                'description' => $fieldGroup->getDescription(),
                                                'position'    => $fieldGroup->getPosition() ];

            }
        }

        return $result;
    }

    public function listContentTypes()
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_access_content_type')) {
            return response('Unauthorized.', 403);
        }
        $contentTypes = StructureService::getAllContentTypes();
        $result = [ ];
        /** @var ContentType $contentType */
        foreach ($contentTypes as $contentType) {
            $result [] = [
                'name'        => $contentType->getName(),
                'alias'       => $contentType->getAlias(),
                'description' => $contentType->getDescription(),
                'builtin'     => $contentType->isBuiltInType(),
                'edit_url'    => route('cms_edit_content_type_page', [ 'alias' => $contentType->alias ]),
            ];
        }

        return response()->json([ 'success' => true, 'data' => $result ]);
    }

    public function saveContentTypeSettings(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_create_content_type')) {
            return response('Unauthorized.', 403);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $contentInfoToUpdate = [ 'settings' => [ ] ];
        if ($request->has('table_display_fields')) {
            $contentInfoToUpdate[ 'settings' ] [ 'table_display_fields' ] = $request->input('table_display_fields');
        } else {
            $contentInfoToUpdate[ 'settings' ] [ 'table_display_fields' ] = [];
        }

        if ($request->has('filter_fields')) {
            $contentInfoToUpdate[ 'settings' ] [ 'filter_fields' ] = $request->input('filter_fields');
        } else {
            $contentInfoToUpdate[ 'settings' ] [ 'filter_fields' ] = [];
        }

        if ($request->has('editable_fields')) {
            $contentInfoToUpdate[ 'settings' ] [ 'editable_fields' ] = $request->input('editable_fields');
        }
        else {
            $contentInfoToUpdate[ 'settings' ] [ 'editable_fields' ] = [];
        }

        if ( !empty( $contentInfoToUpdate[ 'settings' ] )) {
            StructureService::updateContentTypeInfo($contentTypeAlias, $contentInfoToUpdate);
        }

        return response()->json([ 'success'       => true,
                                  'message'       => trans('launchcms.content_types_screen.messages.content_type_save_successfully'),
                                  'alias_changed' => false,
        ]);

    }

    public function saveContentType(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_create_content_type')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $alias = $request->input('alias');
        $contentTypeAlias = $request->input('content_type_alias');
        $description = $request->input('description');
        $enableVersioning = $request->input('enable_versioning');
        $enableVersioning = !empty( $enableVersioning );

        $enableLocalization = $request->input('enable_localization');
        $enableLocalization = !empty( $enableLocalization );
        $primaryLocale = $request->input('primary_locale');
        $workflowID = null;
        if ($request->has('workflow_id')) {
            $workflowID = $request->input('workflow_id');
            if (empty( $workflowID )) {
                $workflowID = null;
            }
        }

        $contentTypeInfo = [ 'name'                => $name,
                             'alias'               => $alias,
                             'description'         => $description,
                             'enable_versioning'   => $enableVersioning,
                             'enable_localization' => $enableLocalization,
                             'workflow_id'         => $workflowID,
                             'primary_locale'      => $primaryLocale ];


        $contentType = null;
        $aliasChanged = false;
        try {
            if (empty( $contentTypeAlias )) {
                //we only allow user to set inherit when they create the content type
                $inherit = $request->input('inherit');
                if ($inherit !== '') {
                    $contentTypeInfo[ 'inherit' ] = $inherit;
                }
                $contentType = StructureService::createContentType($contentTypeInfo);
            } else {
                $contentType = StructureService::updateContentTypeInfo($contentTypeAlias, $contentTypeInfo);
                $aliasChanged = ( $contentTypeAlias !== $contentType->getAlias() );
            }

        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success'          => true,
                                  'message'          => trans('launchcms.content_types_screen.messages.content_type_save_successfully'),
                                  'alias_changed'    => $aliasChanged,
                                  'content_type_url' => route('cms_edit_content_type_page', $contentType->getAlias()) ]);
    }

    public function saveFieldGroup(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_create_content_type')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $alias = $request->input('alias');
        $description = $request->input('description');
        $position = $request->input('position');
        $fieldGroupAlias = $request->input('field_group_alias');
        $contentTypeAlias = $request->input('content_type_alias');

        $fieldGroupInfo = FieldGroup::fromArray([ 'name'        => $name, 'alias' => $alias,
                                                  'position'    => $position,
                                                  'description' => $description ]);

        try {
            if (empty( $fieldGroupAlias )) {
                StructureService::addFieldGroup($contentTypeAlias, $fieldGroupInfo);
            } else {
                StructureService::updateFieldGroup($contentTypeAlias, $fieldGroupAlias, $fieldGroupInfo);
            }
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.content_type_edit_screen.messages.field_group_save_successfully') ]);

    }

    public function getContentTypeData($contentTypeAlias)
    {
        $contentTypeData = $this->getContentTypeDataByAlias($contentTypeAlias);
        if (empty( $contentTypeData )) {
            return response()->json([ 'success' => false, 'error' => trans('launchcms.cms_errors.content_type_not_found') ]);
        }

        return response()->json([ 'success' => true, 'data' => $contentTypeData ]);
    }


    public function deleteContentType(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_delete_content_type')) {
            return response('Unauthorized.', 403);
        }
        $contentTypeAlias = $request->input('alias');
        try {
            StructureService::deleteContentTypeByAlias($contentTypeAlias);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.content_types_screen.messages.content_type_deleted_successfully') ]);

    }

    public function deleteFieldGroup(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_create_content_type')) {
            return response('Unauthorized.', 403);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $fieldGroupAlias = $request->input('field_group_alias');
        try {
            StructureService::deleteFieldGroup($contentTypeAlias, $fieldGroupAlias);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.content_type_edit_screen.messages.field_group_deleted_successfully') ]);

    }

    public function saveField(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_create_content_type')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }

        $fieldAlias = $request->input('field_alias');
        $contentTypeAlias = $request->input('structure_type_alias');
        $dataType = $request->input('data_type');
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        $field = null;
        if (empty( $fieldAlias )) {
            $field = new Field();
            $field->setDataType($dataType);
        } else {
            $field = $contentType->getOwnField($fieldAlias);
        }

        $name = $request->input('name');
        $field->setName($name);
        $alias = $request->input('alias');
        $field->setAlias($alias);
        $description = $request->input('description');
        $field->setDescription($description);
        $position = $request->input('position');
        if (empty( $position )) {
            $position = 0;
        }
        $field->setPosition($position);
        $fieldGroupAlias = $request->input('field_group');
        if ($fieldGroupAlias === '') {
            $fieldGroupAlias = null;
        }
        $field->setFieldGroup($fieldGroupAlias);
        $field->setIndex($request->has('is_indexed'));
        $field->setAllowFullTextSearch($request->has('allow_full_text_search'));
        $field->setUnique($request->has('is_unique'));

        $field->setRequired($request->has('is_required'));
        if ($request->has('validation_rules')) {
            try {
                $field->setValidationRules($request->input('validation_rules'));
            } catch (CMSServiceException $ex) {
                return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
            }
        }
        if ($request->has('settings')) {
            $settings = $request->input('settings');
            $field->setExtraDataTypeInfo($settings);
        }

        try {
            if (empty( $fieldAlias )) {
                StructureService::addField($contentTypeAlias, $field);
            } else {
                StructureService::updateField($contentTypeAlias, $fieldAlias, $field);
            }
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.content_type_edit_screen.messages.field_save_successfully') ]);

    }

    public function deleteField(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_create_content_type')) {
            return response('Unauthorized.', 403);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $fieldAlias = $request->input('field_alias');
        try {
            StructureService::deleteField($contentTypeAlias, $fieldAlias, true);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.content_type_edit_screen.messages.field_deleted_successfully') ]);

    }
}