<?php

namespace App\Http\Controllers\Admin\Ajax;


use App\FieldEditors\EditorFactory;
use App\Http\Controllers\Admin\Templates\ContentAjaxProcessorFactory;
use Illuminate\Http\Request;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use Validator;

class ContentAjaxController extends AdminController
{

    public function listContent($contentTypeAlias, Request $request)
    {
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->listContent($contentTypeAlias, $request);
    }

    public function createDraftContent(Request $request)
    {
        if(!$request->has('content_type_alias')) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->createDraftContent($request);
    }

    public function deleteContent(Request $request)
    {
        if(!$request->has('content_type_alias')) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->deleteContent($request);
    }

    public function saveContent(Request $request)
    {
        if(!$request->has('content_type_alias')) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->saveContent($request);
    }

    public function searchContentByName($contentTypeID, Request $request)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByID($contentTypeID);
        $contentTypeAlias = $contentType->getAlias();
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->searchContentByName($contentType, $request);
    }



    public function deleteEmbeddedRecord(Request $request) {
        if(!$request->has('content_type_alias')) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->deleteEmbeddedRecord($request);
    }



    public function saveEmbeddedRecord(Request $request)
    {
        if(!$request->has('content_type_alias')) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->saveEmbeddedRecord($request);
    }

    public function changeContentWorkflowStatus(Request $request) {
        if(!$request->has('content_type_alias')) {
            return response()->json([ 'success' => false ]);
        }
        $contentTypeAlias = $request->input('content_type_alias');
        $processorTemplate = ContentAjaxProcessorFactory::getProcessorTemplate($contentTypeAlias);
        return $processorTemplate->changeContentWorkflowStatus($request);
    }

    public function getEmbeddedRecords($contentTypeAlias, $fieldAlias, $contentID)
    {
        /** @var Content $content */
        $content = ContentService::getContentByID($contentTypeAlias, $contentID);
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        /** @var Field $field */
        $field = $contentType->getField($fieldAlias);

        $dataTypeID = $field->getExtraDataTypeInfo()[ Field::DATA_TYPE_ID ];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);

        /** @var Field $editableFields */
        $editableFields = $dataType->getEditableFields();
        $persistRows = [ ];
        if (isset( $content->attributes[ $fieldAlias ] )) {
            $persistRows = $content->attributes[ $fieldAlias ];
        }
        $displayRows = [ ];
        if ( !empty( $persistRows )) {
            foreach ($persistRows as $persistRow) {
                $displayRow = [ ];
                $displayRow[ 'id' ] = $persistRow[ '_id' ];
                foreach ($editableFields as $editableField) {
                    $alias = $editableField->getAlias();
                    $fieldEditor = EditorFactory::getEditor($editableField);
                    $dataColumnInDB = $persistRow[ $alias ];

                    $displayValue = $fieldEditor->createDisplayValue($dataColumnInDB);
                    $displayRow[ $alias ] = $displayValue;
                }
                $displayRows [] = $displayRow;
            }
        }
        $editableFieldMetaData = [ ];
        /** @var Field $editableField */
        foreach ($editableFields as $editableField) {
            $fieldMetaData = [ ];
            $fieldMetaData[ 'alias' ] = $editableField->getAlias();
            $fieldMetaData[ 'type' ] = $editableField->getDataType();
            $fieldMetaData[ 'settings' ] = $editableField->getExtraDataTypeInfo();
            $editableFieldMetaData[] = $fieldMetaData;
        }

        return response()->json([ 'success' => true,
                                  'data'    => $displayRows,
                                  'fields'  => $editableFieldMetaData,

        ]);

    }
}