<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\Content\Workflow;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Facades\StructureService;
use Sentinel;
use Validator;


class WorkflowDefinitionAjaxController extends AdminController
{

    protected function buildReturnWorkflowData(Workflow $workflow) {
        return [
            'id' => (string) $workflow->_id,
            'name' => $workflow->getName(),
            'alias' => $workflow->getAlias(),
            'description' => $workflow->getDescription(),
            'json' => $workflow->getJSON()
        ];
    }
    protected function getWorkflowByID($id)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_workflow_definition')) {
            return response('Unauthorized.', 403);
        }

        $relationshipDefinition = StructureService::getWorkflowByID($id);
        if (empty( $relationshipDefinition )) {
            return null;
        }
        return $this->buildReturnWorkflowData($relationshipDefinition);
    }

    public function listWorkflowDefinition()
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_workflow_definition')) {
            return response('Unauthorized.', 403);
        }
        $allWorkflows = StructureService::getAllWorkflows();
        $result = [ ];

        foreach ($allWorkflows as $workflow) {
            $result [] = $this->buildReturnWorkflowData($workflow);
        }

        return response()->json([ 'success' => true, 'data' => $result ]);
    }



    public function saveWorkflowDefinition(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_create_workflow_definition')) {
            return response('Unauthorized.', 403);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'alias' => 'required',
            'json'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $name = $request->input('name');
        $alias = $request->input('alias');
        $workflowAlias = $request->input('workflow_alias');
        $json = $request->input('json');
        $description =  $request->input('description');
        $workflowInfo = [ 'name'                => $name,
                             'alias'               => $alias,
                             'json'   => $json,
                             'description' => $description,
                             ];
        $relationshipDefinitionObj = null;
        try {
            if (empty( $workflowAlias )) {
                StructureService::addWorkflow($workflowInfo);
            } else {
                StructureService::updateWorkflow($workflowAlias, $workflowInfo);
            }

        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false,
                                      'error' => $ex->getTranslatedMessage(),
                                      'detail_errors' => $ex->getErrors()
            ]);
        }

        return response()->json([ 'success'          => true,
                                  'message'          => trans('launchcms.workflow_definition_screen.messages.workflow_save_successfully'),
                                  ]);
    }



    public function getWorkflowDefinitionData($id)
    {
        $returnData = $this->getWorkflowByID($id);
        if (empty( $returnData )) {
            return response()->json([ 'success' => false, 'error' => trans('launchcms.cms_errors.workflow_not_found') ]);
        }

        return response()->json([ 'success' => true, 'data' => $returnData ]);
    }


    public function deleteWorkflowDefinition(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_delete_workflow_definition')) {
            return response('Unauthorized.', 403);
        }
        $id = $request->input('id');
        try {
            StructureService::deleteWorkflowByID($id);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.workflow_definition_screen.messages.workflow_deleted_successfully') ]);

    }


}