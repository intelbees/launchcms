<?php

namespace App\Http\Controllers\Admin\Ajax;

use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\FieldGroup;
use LaunchCMS\Models\Content\Structure;


class StructureAjaxController extends AdminController
{

    protected function buildFieldObjectArray($fields, Structure $structure)
    {
        $result = [ ];
        /** @var Field $field */
        foreach ($fields as $field) {
            $fieldInfoArray = [ 'name' => $field->getName(),

                                'alias'                  => $field->getAlias(),
                                'description'            => $field->getDescription(),
                                'data_type'              => $field->getDataType(),
                                'data_type_text'         => trans('launchcms.fields.' . $field->getDataType() . '.name'),
                                'position'               => $field->getPosition(),
                                'is_indexed'             => $field->index(),
                                'is_unique'              => $field->isUnique(),
                                'is_required'            => $field->isRequired(),
                                'allow_full_text_search' => $field->allowFullTextSearch(),
                                'settings'               => $field->getExtraDataTypeInfo(),
                                'validation_rules'       => $field->getValidationRules(),
                                'field_group'            => $field->getFieldGroup(),
                                'builtin'       => $field->isBuiltInType(),
                                

            ];
            if ($structure instanceof ContentType) {
                $fieldGroupName = $field->getFieldGroup();
                if(!empty($fieldGroupName)) {
                    /** @var FieldGroup $fieldGroupObj */
                    $fieldGroupObj = $structure->getFieldGroupByAlias($fieldGroupName);
                    if(!empty($fieldGroupObj)) {
                        $fieldInfoArray[ 'field_group_name' ] = $fieldGroupObj->getName();
                    } else {
                        $fieldInfoArray[ 'field_group_name' ]  = null;
                    }

                }

            }
            $result[] = $fieldInfoArray;
        }

        return $result;
    }
    protected static function fieldSortAndGroupByComparison(array $a, array $b) {
        if($a['field_group'] === $b['field_group']) {
            if($a['position'] === $b['position']) {
                return 0;
            }
            return $a['position'] < $b['position'] ? -1 : 1;
        }
        return $a['field_group'] < $b['field_group'] ? -1 : 1;

    }
    protected function buildReturnedDataFromStructureObject(Structure $structure)
    {
        if (empty( $structure )) {
            return null;
        }
        $settings = $structure->getSettings();
        $returnedSettings = [ ];
        $returnedSettings[ 'table_display_fields' ] = isset( $settings[ 'table_display_fields' ] ) ? $settings[ 'table_display_fields' ] : [ ];
        if ($structure instanceof ContentType) {
            $returnedSettings[ 'filter_fields' ] = isset( $settings[ 'filter_fields' ] ) ? $settings[ 'filter_fields' ] : [ ];
        }
        $returnedSettings[ 'editable_fields' ] = isset( $settings[ 'editable_fields' ] ) ? $settings[ 'editable_fields' ] : [ ];
        $result = [ 'name'        => $structure->getName(),
                    'alias'       => $structure->getAlias(),
                    'description' => $structure->getDescription(),
                    'builtin'     => $structure->isBuiltInType(),
                    'settings'    => $returnedSettings,
        ];
        if($structure instanceof ContentType) {
            $result['workflow_id'] = $structure->getWorkflowID();
        }
        $result[ 'fields' ] = [ ];
        $fields = $structure->fields;

        if ( !empty( $fields )) {
            $result[ 'fields' ] = $this->buildFieldObjectArray($fields, $structure);
            usort($result[ 'fields' ], array('App\Http\Controllers\Admin\Ajax\StructureAjaxController','fieldSortAndGroupByComparison'));
        }

        if ( !empty( $fields )) {
            $result[ 'configurable_editable_fields' ] = $this->buildConfigurableEditableFields($fields);
            $result[ 'configurable_table_display_fields' ] = $this->buildConfigurableTableDisplayFields($structure, $fields);
        } else {
            $result[ 'configurable_editable_fields' ] = [];
            $result[ 'configurable_table_display_fields'] = [];
        }

        return $result;
    }

    protected function buildConfigurableEditableFields($fields) {
        $configurableInfoFields = [ ];
        /** @var Field $field */
        foreach ($fields as $field) {
            if(!$field->isBuiltInType()) {
                $configurableInfoFields[] = [ 'alias' => $field->getAlias(), 'name' => $field->name ];
            }
        }
        return $configurableInfoFields;
    }

    protected function buildConfigurableFilterableFields($structure, $fields) {
        $configurableInfoFields = [ ];
        if ($structure instanceof ContentType) {
            // We don't add name, created_time, last_updated time field for data type because real instance of data type is embedded object.
            // It doesn't have name
            $configurableInfoFields [] = [ 'alias' => 'name', 'name' => trans('launchcms.content_builtin_field.name') ];
            $configurableInfoFields [] = [ 'alias' => 'created_at', 'name' => trans('launchcms.content_builtin_field.created_time') ];
            $configurableInfoFields [] = [ 'alias' => 'updated_at', 'name' => trans('launchcms.content_builtin_field.last_updated_time') ];
        }
        $fieldTypeShouldNotReturn = [ 'embedded', 'embedded_list', 'reference_list', 'geo' ];
        /** @var Field $field */
        foreach ($fields as $field) {
            if ( !in_array($field->getDataType(), $fieldTypeShouldNotReturn)) {
                $configurableInfoFields[] = [ 'alias' => $field->getAlias(), 'name' => $field->name ];
            }
        }
        return $configurableInfoFields;
    }

    protected function buildConfigurableTableDisplayFields($structure, $fields) {
        $configurableInfoFields = [ ];
        if ($structure instanceof ContentType) {
            // We don't add name, created_time, last_updated time field for data type because real instance of data type is embedded object.
            // It doesn't have name
            $configurableInfoFields [] = [ 'alias' => 'name', 'name' => trans('launchcms.content_builtin_field.name') ];
            $configurableInfoFields [] = [ 'alias' => 'created_at', 'name' => trans('launchcms.content_builtin_field.created_time') ];
            $configurableInfoFields [] = [ 'alias' => 'updated_at', 'name' => trans('launchcms.content_builtin_field.last_updated_time') ];
        }
        $fieldTypeShouldNotReturn = [ 'embedded', 'embedded_list', 'reference_list', 'geo' ];
        /** @var Field $field */
        foreach ($fields as $field) {
            if ( !in_array($field->getDataType(), $fieldTypeShouldNotReturn)) {
                $configurableInfoFields[] = [ 'alias' => $field->getAlias(), 'name' => $field->name ];
            }
        }
        return $configurableInfoFields;
    }
}