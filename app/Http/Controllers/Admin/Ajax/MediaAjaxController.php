<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Interfaces\MediaServiceInterface;
use Illuminate\Support\Facades\File;
use LaunchCMS\Utils\StringUtil;
use Sentinel;
use Validator;


class MediaAjaxController extends AdminController
{
    /** @var MediaServiceInterface */
    protected $mediaService;

    public function __construct(MediaServiceInterface $mediaService)
    {
        $this->mediaService = $mediaService;
        parent::__construct();
    }

    /**
     * @param MediaServiceInterface $mediaService
     */
    public function setMediaService(MediaServiceInterface $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    protected function buildFolderInfoData(array $folderInfo)
    {
        $output = $folderInfo;
        if (isset( $output[ 'files' ] ) &&
            !empty( $output[ 'files' ] )
        ) {
            foreach ($output[ 'files' ] as &$file) {
                if ( !isset( $file[ 'mimeType' ] )) {
                    $file[ 'mimeType' ] = 'Unknown';
                }
                $file[ 'modified' ] = $file[ 'modified' ]->format('j-M-y g:ia');
                $file[ 'isImage' ] = self::isImage($file[ 'mimeType' ]);
                $file[ 'size' ] = self::humanFilesize($file[ 'size' ]);
            }
        }

        return $output;
    }

    public function getFolderInfo(Request $request)
    {
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_access_media')) {
            return response('Unauthorized.', 403);
        }
        $folder = $request->input('folder');
        $homePath = $user->home;
        if(empty($homePath)) {
            $homePath = config('launchcms.media.default_path');
        }

        if(!StringUtil::startsWith($folder, $homePath)) {
            return response()->json([ 'success' => false, 'error' => trans('launchcms.cms_errors.no_permission_to_access_path') ]);
        }
        //TODO check user permission to allow them to access the folder
        $folderInfo = null;
        try {
            $folderInfo = $this->mediaService->folderInfo($folder);
            $folderInfo = $this->buildFolderInfoData($folderInfo);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'data'    => $folderInfo ]);

    }

    public function uploadFile(Request $request)
    {
        $file = $_FILES[ 'media_file' ];
        $fileName = $file[ 'name' ];
        $path = str_finish($request->get('path'), '/') . $fileName;
        $content = File::get($file[ 'tmp_name' ]);

        try {
            $this->mediaService->saveFile($path, $content);
        } catch (CMSServiceException $ex) {
            $error = $ex->getTranslatedMessage();
            return response()->json([ 'success' => false, 'error' => $error ]);
        }

        return response()->json([ 'error'                   => '',
                                  'message' => trans('launchcms.media_screen.messages.file_has_been_upload_successfully', [ 'file' => $path ]),
                                  'initialPreview'          => [ ],
                                  'initialPreviewConfig'    => [ ],
                                  'initialPreviewThumbTags' => [ ] ]);
    }

    public function deleteMediaItems(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'paths' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_delete_media')) {
            return response('Unauthorized.', 403);
        }
        $paths = $request->input('paths');
        foreach ($paths as $path) {

            try {
                if(StringUtil::startsWith($path, '[f]')) {
                    $realFilePath = substr($path, 3);
                    $this->mediaService->deleteFile($realFilePath);
                }
                else if (StringUtil::startsWith($path, '[d]')) {
                    $realFilePath = substr($path, 3);
                    $this->mediaService->deleteDirectory($realFilePath);
                }
            } catch (CMSServiceException $ex) {
                return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
            }
        }


        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.media_screen.messages.media_items_has_been_deleted_successfully') ]);

    }

    public function deleteFile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'path' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_delete_media')) {
            return response('Unauthorized.', 403);
        }
        $path = $request->input('path');
        try {
            $this->mediaService->deleteFile($path);
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.media_screen.messages.file_has_been_deleted_successfully', [ 'file' => $path ]) ]);
    }

    public function renameFile(Request $request)
    {

    }

    public function renameFolder(Request $request)
    {

    }

    public function createFolder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'path' => 'required',
            'new_folder'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ]);
        }
        $user = Sentinel::getUser();
        if ( !$user->hasAccess('cms_delete_media')) {
            return response('Unauthorized.', 403);
        }
        $path = $request->input('path');
        $newFolder = $request->input('new_folder');
        try {
            $this->mediaService->createDirectory($path.'/'.ltrim($newFolder, '/'));
        } catch (CMSServiceException $ex) {
            return response()->json([ 'success' => false, 'error' => $ex->getTranslatedMessage() ]);
        }

        return response()->json([ 'success' => true,
                                  'message' => trans('launchcms.media_screen.messages.folder_has_been_created', [ 'file' => $path ]) ]);
    }

    /**
     * Return sizes readable by humans
     */
    public static function humanFilesize($bytes, $decimals = 2)
    {
        $size = [ 'B', 'kB', 'MB', 'GB', 'TB', 'PB' ];
        $factor = floor(( strlen($bytes) - 1 ) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .
        @$size[ $factor ];
    }

    /**
     * Is the mime type an image
     */
    public static function isImage($mimeType)
    {
        return starts_with($mimeType, 'image/');
    }

}