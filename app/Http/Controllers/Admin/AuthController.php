<?php
namespace App\Http\Controllers\Admin;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\User\User;
use Theme;
use View;
use Sentinel;
use Session;
use Illuminate\Http\Request;

class AuthController extends AdminController
{
    /**
     * Logout the admin user
     */
    public function logout() {
        Sentinel::logout();
        return redirect()->route('cms_login');
    }

    /**
     * Show admin login page
     * @return mixed
     */
    public function showLogin() {
        return View::make('pages.login', array());
    }

    /**
     * Process admin login submit
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request) {
        $email = $request->input('email');
        $password = $request->input('password');

        $credentials = [
            'email'    => $email,
            'password' => $password,
        ];

        $result = Sentinel::authenticate($credentials);
        if($result === false) {
            $request->session()->flash('message', trans('launchcms.messages.invalid_credential'));
            return redirect()->route('cms_login');
        }
        if($result instanceof User) {
            if(!$result->admin_user) {
                Sentinel::logout();
                $request->session()->flash('message', trans('launchcms.messages.not_admin_user'));
                return redirect()->route('cms_login');
            }
        }

        return redirect()->route('cms_dashboard');
    }

    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('guest');
    }
}