<?php

namespace App\Http\Controllers\Admin;


use Activation;
use App\Http\Helpers\ClientLocalization;
use App\Http\Helpers\ContentTableJSONBuilder;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Services\Interfaces\MediaServiceInterface;
use Sentinel;
use View;
use Illuminate\Http\Request;

class MediaController extends AdminController
{
    /** @var MediaServiceInterface  */
    protected $mediaService;

    public function __construct(MediaServiceInterface $mediaService)
    {
        $this->mediaService = $mediaService;
        parent::__construct();
    }
    protected function initializeMessageForClient() {
        //add some common messages to use in client side
        ClientLocalization::putKeys(['launchcms.messages.cannot_use_local_storage',
            'launchcms.messages.processing',
            'launchcms.messages.confirm_delete_media_paths'
        ]);
    }
    public function index() {
        $this->initializeMessageForClient();
        $user = Sentinel::getUser();
        $homePath = $user->home;
        if(empty($homePath)) {
            $homePath = config('launchcms.media.default_path');
        }
        return View::make('pages.media.listing', ['homePath' => $homePath]);
    }

    public function fileBrowser(Request $request) {
        $this->initializeMessageForClient();
        $user = Sentinel::getUser();
        $homePath = $user->home;
        if(empty($homePath)) {
            $homePath = config('launchcms.media.default_path');
        }
        $ckeditorCallback = $request->input('CKEditorFuncNum');
        return View::make('pages.media.file-browser', ['homePath' => $homePath, 'ckeditorCallBack' => $ckeditorCallback]);
    }
}