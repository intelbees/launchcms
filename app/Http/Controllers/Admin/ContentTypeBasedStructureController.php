<?php

namespace App\Http\Controllers\Admin;


use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\Workflow;
use LaunchCMS\Services\WorkflowEngine;
use View;

class ContentTypeBasedStructureController extends AdminController
{

    public static function getDisplayFieldsOnTableView(ContentType $contentType, $defaultFields = [])
    {
        $settings = $contentType->getSettings();
        $displayedFields = isset( $settings[ 'table_display_fields' ] ) ? $settings[ 'table_display_fields' ] : [ ];

        $result = [ ];
        if (empty( $displayedFields )) {
            if(empty($defaultFields)) {
                $result [] = [ 'alias' => 'name',
                               'name'  => trans('launchcms.content_builtin_field.name'),
                               'type'  => 'builtin' ];
                $result [] = [ 'alias' => 'workflow_status',
                               'name'  => trans('launchcms.content_builtin_field.workflow_status'),
                               'type'  => 'builtin' ];
            } else {
                $result = $defaultFields;
            }
            return $result;
        }
        foreach ($displayedFields as $fieldAlias) {
            if (Content::isBuiltInField($fieldAlias)) {
                $result[] = [ 'alias' => $fieldAlias,
                              'name'  => trans('launchcms.content_builtin_field.' . $fieldAlias),
                              'type'  => 'builtin' ];

                continue;
            }

            /** @var Field $field */
            $field = $contentType->getField($fieldAlias);
            if(empty($field)) {
                continue;
            }

            $fieldInfo = [ 'alias' => $fieldAlias,
                           'name'  => $field->getName(),
                           'type'  => $field->getDataType() ];
            if ($field->getDataType() === Field::REFERENCE) {
                $contentTypeID = $field->getExtraDataTypeInfo()[ Field::CONTENT_TYPE_ID ];
                $fieldInfo[ 'ref_content_type_id' ] = $contentTypeID;
            }
            $result[] = $fieldInfo;
        }

        return $result;
    }

    
}