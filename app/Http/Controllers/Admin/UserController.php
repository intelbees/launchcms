<?php

namespace App\Http\Controllers\Admin;


use App\Http\Helpers\ContentTableJSONBuilder;
use App\Http\Helpers\FieldGroupViewObjectBuilder;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\User\User;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\PermissionService;
use LaunchCMS\Services\Facades\StructureService;
use Activation;
use LaunchCMS\Services\Facades\UserService;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use View;

class UserController extends ContentTypeBasedStructureController
{
    /** @var  StructureServiceInterface */
    protected $structureService;

    /** @var  ContentServiceInterface */
    protected $contentService;
    
    protected function buildUserDefaultFields()
    {
        $result = [ ];
        $result [] = [ 'alias' => 'name',
                       'name'  => trans('launchcms.content_builtin_field.name'),
                       'type'  => 'builtin' ];
        $result [] = [ 'alias' => 'email',
                       'name'  => 'Email',
                       'type'  => 'builtin' ];
        $result [] = [ 'alias' => 'last_login.date',
                       'name'  => 'Last login',
                       'type'  => 'builtin' ];

        return $result;
    }

    public function userListing()
    {
        $contentType = $this->structureService->getContentTypeByAlias('cms_users');
        $displayFields = self::getDisplayFieldsOnTableView($contentType, $this->buildUserDefaultFields());

        return View::make('pages.user-listing-page', [
            'contentType'   => $contentType,
            'displayFields' => $displayFields,
        ]);
    }


    public function showUserEditPage($id)
    {
        /** @var ContentType $contentType */
        $contentType = $this->structureService->getContentTypeByAlias('cms_users');
        /** @var Content $content */
        $content = $this->contentService->getContentByID('cms_users', $id);
        $fieldGroupViewObjectBuilder = new FieldGroupViewObjectBuilder();
        $fieldGroupViewObjects = $fieldGroupViewObjectBuilder->buildFieldGroupViewObjects($contentType);
        $dataTypesToPrepareForDialog = $contentType->getDataTypesRelatedToEditableFields();

        /** @var User $user */
        $user = User::where('_id', $id)->first();
        $activated = UserService::isUserActivated($user);
        $adminAccess = $user->getAttribute('admin_user');
        if ($adminAccess == null) {
            $adminAccess = false;
        }
        $roles = UserService::getAllRoles();
        $permissions = PermissionService::getAllPermissions();
        $selectedRoles = isset($user->roles) ? $user->roles : [];
        $selectedPermissions = isset($user->permissions) ? $user->permissions : [] ;

        return View::make('pages.user-edit.index', [
            'contentType'                  => $contentType,
            'content'                      => $content,
            'fieldGroupTabs'               => $fieldGroupViewObjects,
            'activated'                    => $activated,
            'adminAccess'                  => $adminAccess,
            'roles'                        => $roles,
            'permissions'                  => $permissions,
            'selectedRoles'                => $selectedRoles,
            'selectedPermissions'          => $selectedPermissions,
            'user'                         => $user,
            'dataTypesToPrepareForDialogs' => $dataTypesToPrepareForDialog,
        ]);
    }

    public function __construct(StructureServiceInterface $structureService, ContentServiceInterface $contentService)
    {
        $this->structureService = $structureService;
        $this->contentService = $contentService;
        parent::__construct();
    }

}