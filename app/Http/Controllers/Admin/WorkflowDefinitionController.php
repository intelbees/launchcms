<?php

namespace App\Http\Controllers\Admin;


use App\Http\Helpers\ClientLocalization;
use LaunchCMS\Http\Controllers\AdminController;
use LaunchCMS\Services\Facades\UserService;
use Sentinel;
use View;

class WorkflowDefinitionController extends AdminController
{
    protected function initializeMessageForClient() {
        //add some common messages to use in client side
        ClientLocalization::putKeys(['launchcms.messages.cannot_use_local_storage',
            'launchcms.messages.processing',
        ]);
    }
    public function showListingPage() {
        $user = Sentinel::getUser();
        if(!$user->hasAccess('cms_access_workflow_definition')) {
            abort(403, 'Unauthorized action.');
        }
        $this->initializeMessageForClient();

        return View::make('pages.workflow-definition-page');
    }


}