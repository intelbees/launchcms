<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([ 'middleware' => [ 'web' ], 'prefix' => config('launchcms.admin_slug') ], function() {
    Route::get('dashboard', [ 'as' => 'cms_dashboard', 'uses' => 'Admin\DashboardController@show' ]);
    Route::get('content-type', [ 'as' => 'cms_content_type_listing_page', 'uses' => 'Admin\ContentTypeController@showContentTypeListingPage' ]);
    Route::get('content-type/edit/{alias}', [ 'as' => 'cms_edit_content_type_page', 'uses' => 'Admin\ContentTypeController@showContentTypeEditPage' ]);

    Route::get('data-type', [ 'as' => 'cms_data_type_listing_page', 'uses' => 'Admin\DataTypeController@showDataTypeListingPage' ]);
    Route::get('data-type/edit/{alias}', [ 'as' => 'cms_edit_data_type_page', 'uses' => 'Admin\DataTypeController@showDataTypeEditPage' ]);

    Route::get('ajax/content-type/list', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@listContentTypes' ]);
    Route::post('ajax/content-type/save', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@saveContentType' ]);
    Route::post('ajax/content-type/save-settings', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@saveContentTypeSettings' ]);
    Route::post('ajax/content-type/delete', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@deleteContentType' ]);
    Route::get('ajax/content-type/get/{alias}', [ 'as' => 'cms_get_content_type_data', 'uses' => 'Admin\Ajax\ContentTypeAjaxController@getContentTypeData' ]);
    Route::post('ajax/content-type/field-group/save', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@saveFieldGroup' ]);
    Route::post('ajax/content-type/field-group/delete', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@deleteFieldGroup' ]);
    Route::post('ajax/content-type/field/save', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@saveField' ]);
    Route::post('ajax/content-type/field/delete', [ 'uses' => 'Admin\Ajax\ContentTypeAjaxController@deleteField' ]);


    Route::get('ajax/data-type/list', [ 'uses' => 'Admin\Ajax\DataTypeAjaxController@listDataTypes' ]);
    Route::post('ajax/data-type/save', [ 'uses' => 'Admin\Ajax\DataTypeAjaxController@saveDataType' ]);
    Route::post('ajax/data-type/save-settings', [ 'uses' => 'Admin\Ajax\DataTypeAjaxController@saveDataTypeSettings' ]);
    Route::post('ajax/data-type/delete', [ 'uses' => 'Admin\Ajax\DataTypeAjaxController@deleteDataType' ]);
    Route::get('ajax/data-type/get/{alias}', [ 'as' => 'cms_get_data_type_data', 'uses' => 'Admin\Ajax\DataTypeAjaxController@getDataTypeData' ]);
    Route::post('ajax/data-type/field/save', [ 'uses' => 'Admin\Ajax\DataTypeAjaxController@saveField' ]);
    Route::post('ajax/data-type/field/delete', [ 'uses' => 'Admin\Ajax\DataTypeAjaxController@deleteField' ]);


    Route::get('ajax/content/{content_type_id}/search-content-by-name', [ 'as' => 'cms_search_content_by_name', 'uses' => 'Admin\Ajax\ContentAjaxController@searchContentByName' ]);
    Route::post('ajax/content/save-draft', [ 'uses' => 'Admin\Ajax\ContentAjaxController@createDraftContent' ]);
    Route::post('ajax/content/save-content', [ 'uses' => 'Admin\Ajax\ContentAjaxController@saveContent' ]);
    Route::post('ajax/content/change-status', [ 'uses' => 'Admin\Ajax\ContentAjaxController@changeContentWorkflowStatus' ]);
    Route::post('ajax/content/delete-content', [ 'uses' => 'Admin\Ajax\ContentAjaxController@deleteContent' ]);
    Route::post('ajax/content/save-embedded-record', [ 'uses' => 'Admin\Ajax\ContentAjaxController@saveEmbeddedRecord' ]);
    Route::get('ajax/content/get-embedded-records/{content_type_alias}/{field_alias}/{content_id}', ['as' => 'cms_get_embedded_records', 'uses' => 'Admin\Ajax\ContentAjaxController@getEmbeddedRecords' ]);
    Route::post('ajax/content/delete-embedded-record', ['as' => 'cms_delete_embedded_record', 'uses' => 'Admin\Ajax\ContentAjaxController@deleteEmbeddedRecord' ]);

    Route::get('ajax/content/{content_type_alias}/list', [ 'uses' => 'Admin\Ajax\ContentAjaxController@listContent' ]);

    Route::get('ajax/relationship/list', [ 'uses' => 'Admin\Ajax\RelationshipDefinitionAjaxController@listRelationshipDefinition' ]);
    Route::get('ajax/relationship/detail/{alias}', [ 'uses' => 'Admin\Ajax\RelationshipDefinitionAjaxController@getRelationshipDefinitionData' ]);
    Route::post('ajax/relationship/save', [ 'uses' => 'Admin\Ajax\RelationshipDefinitionAjaxController@saveRelationshipDefinition' ]);
    Route::post('ajax/relationship/delete', [ 'uses' => 'Admin\Ajax\RelationshipDefinitionAjaxController@deleteRelationshipDefinition' ]);
    Route::get('relationship/list', ['as' => 'cms_relationship_definition_listing', 'uses' => 'Admin\RelationshipDefinitionController@showListingPage' ]);


    Route::get('ajax/workflow/list', [ 'uses' => 'Admin\Ajax\WorkflowDefinitionAjaxController@listWorkflowDefinition' ]);
    Route::get('ajax/workflow/detail/{id}', [ 'uses' => 'Admin\Ajax\WorkflowDefinitionAjaxController@getWorkflowDefinitionData' ]);
    Route::post('ajax/workflow/save', [ 'uses' => 'Admin\Ajax\WorkflowDefinitionAjaxController@saveWorkflowDefinition' ]);
    Route::post('ajax/workflow/delete', [ 'uses' => 'Admin\Ajax\WorkflowDefinitionAjaxController@deleteWorkflowDefinition' ]);
    Route::get('workflow/list', ['as' => 'cms_workflow_definition_listing', 'uses' => 'Admin\WorkflowDefinitionController@showListingPage' ]);



    Route::get('content/{content_type_alias}/list/', [ 'as' => 'cms_content_listing', 'uses' => 'Admin\ContentController@contentListing' ]);
    Route::get('content/{content_type_alias}/edit/{content_id}', [ 'as' => 'cms_content_edit', 'uses' => 'Admin\ContentController@showContentEditPage' ]);
    Route::get('content', [ 'as' => 'cms_content_landing', 'uses' => 'Admin\ContentController@landing' ]);

    Route::get('user', [ 'as' => 'cms_user_listing', 'uses' => 'Admin\UserController@userListing' ]);
    Route::get('user/edit/{user_id}', [ 'as' => 'cms_user_edit', 'uses' => 'Admin\UserController@showUserEditPage' ]);

    Route::get('role', [ 'as' => 'cms_role_listing', 'uses' => 'Admin\RoleController@roleListing' ]);
    Route::get('role/edit/{role_id}', [ 'as' => 'cms_role_edit', 'uses' => 'Admin\RoleController@showRoleEditPage' ]);



    Route::get('login', [ 'as' => 'cms_login', 'uses' => 'Admin\AuthController@showLogin' ]);
    Route::post('login', [ 'as' => 'cms_post_login', 'uses' => 'Admin\AuthController@postLogin' ]);
    Route::get('logout', [ 'as' => 'cms_logout', 'uses' => 'Admin\AuthController@logout' ]);

    Route::post('ajax/media/upload', [ 'uses' => 'Admin\Ajax\MediaAjaxController@uploadFile' ]);
    Route::post('ajax/media/create-folder', [ 'uses' => 'Admin\Ajax\MediaAjaxController@createFolder' ]);
    Route::post('ajax/media/delete-media-items', [ 'uses' => 'Admin\Ajax\MediaAjaxController@deleteMediaItems' ]);
    Route::post('ajax/media/rename', [ 'uses' => 'Admin\Ajax\MediaAjaxController@rename' ]);

    Route::get('ajax/media', [ 'uses' => 'Admin\Ajax\MediaAjaxController@getFolderInfo' ]);
    Route::get('media-manager', [ 'as' => 'media_management', 'uses' => 'Admin\MediaController@index' ]);
    Route::get('file-browser', [ 'as' => 'file_browser', 'uses' => 'Admin\MediaController@fileBrowser' ]);


    /** Elfinder - File manager */
    /*
    Route::get('media-manager', ['as' => 'media_management', 'uses' => 'Admin\ElfinderController@showIndex']);
    Route::any('media-manager/connector', ['as' => 'elfinder.connector', 'uses' => 'Admin\ElfinderController@showConnector']);
    Route::get('media-manager/popup/{input_id}', ['as' => 'elfinder.popup', 'uses' => 'Admin\ElfinderController@showPopup']);
    Route::get('media-manager/filepicker/{input_id}', ['as' => 'elfinder.filepicker', 'uses' => 'Admin\ElfinderController@showFilePicker']);
    Route::get('media-manager/tinymce', ['as' => 'elfinder.tinymce', 'uses' => 'Admin\ElfinderController@showTinyMCE']);
    Route::get('media-manager/tinymce4', ['as' => 'elfinder.tinymce4', 'uses' => 'Admin\ElfinderController@showTinyMCE4']);
    Route::get('media-manager/ckeditor', ['as' => 'elfinder.ckeditor', 'uses' => 'Admin\ElfinderController@showCKeditor4']);
    */


});

Route::get('glide/{path}', function($path){
    $server = \League\Glide\ServerFactory::create([
        'source' => app('filesystem')->disk('media')->getDriver(),
        'cache' => storage_path('glide'),
    ]);
    return $server->getImageResponse($path, Input::query());
})->where('path', '.+');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group([ 'middleware' => [ 'web' ] ], function() {
    //
});
