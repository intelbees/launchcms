<?php

namespace App\Http\Helpers;


use App\MenuRenderer\MenuRendererInterface;

class MenuBuilder
{
    public static function buildMenuViewObjectFromConfig($configKey) {
        $result = [];
        $menu = config($configKey);
        foreach ($menu as $mainMenuItem) {
            $menuViewObject = self::buildMenuViewObject($mainMenuItem);
            if(!empty($menuViewObject)) {
                $result[] = $menuViewObject;
            }
        }
        return $result;
    }

    protected static function buildMenuViewObject($menuItemConfig) {
        $result = [];
        foreach ($menuItemConfig as $key => $value) {
            if($key === 'label_key') {
                $result['label'] = trans($value);
            }
            else if($key ==='children') {
                $childrenData = $value;
                $childrenDataViewOutput = [];
                foreach ($childrenData as $child) {
                    $childrenDataViewOutput[] = self::buildMenuViewObject($child);
                }
                $result['children'] = $childrenDataViewOutput;
            }
            else if($key ==='item_renderer') {
                $rendererClassName = $value;
                $rendererObj = self::getMenuItemRendererFromClassName($rendererClassName);
                $result['item_renderer'] = $rendererObj;
            }
            else if($key === 'url') {
                $result[$key] =  self::buildUrl($value);
            }
            else {
                $result[$key] = $value;
            }
        }
        if(!isset($result['url'])) {
            $result['url'] = '#';
        }

        return $result;
    }

    public static function buildUrl($urlData) {
        if(is_string($urlData)) {
            return $urlData;
        }
        if(is_array($urlData)) {
            $params = [];
            if(isset($urlData['params'])) {
                $params = $urlData['params'];
            }

            if(isset($urlData['route_name'])) {
                return route($urlData['route_name'], $params);
            }
            else if(isset($urlData['action'])) {
                return action($urlData['action'], $params);
            }
        }
        return '#';
    }

    public static function getMenuItemRendererFromClassName($className) {
        $class = '\\' . ltrim($className, '\\');
        $renderer = new $class();
        if(!($renderer instanceof  MenuRendererInterface)) {
            return null;
        }
        return $renderer;
    }
}