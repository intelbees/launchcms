<?php

namespace App\Http\Helpers;


use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Field;

class FieldGroupViewObjectBuilder
{

    protected function buildUserFieldGroupViewObjects(ContentType $contentType, $editableFields)
    {
        $fieldGroupArray = [];
        $userFieldGroups =$contentType->getFieldGroups();

        /** @var FieldGroup $fieldGroupObject */
        foreach ($userFieldGroups as $fieldGroupObject) {
            $fieldGroupViewObject = [];
            $fieldGroupViewObject['name'] = $fieldGroupObject->getName();
            $fieldGroupViewObject['alias'] = $fieldGroupObject->getAlias();
            $fieldGroupViewObject['description'] = $fieldGroupObject->getDescription();
            $fieldGroupViewObject['fields'] = [];
            $fieldsInGroup = $contentType->getFieldsOfGroup($fieldGroupObject->getAlias());
            
            /** @var Field $field */
            foreach ($fieldsInGroup as $field) {
                $fieldCanBeEdit = empty($editableFields) || in_array($field->getAlias(), $editableFields);
                if($fieldCanBeEdit){
                    $fieldGroupViewObject['fields'] []= $field;
                }
            }
            if(!empty($fieldGroupViewObject['fields'])) {
                $fieldGroupArray[] = $fieldGroupViewObject;
            }
        }

        return $fieldGroupArray;
    }
    protected function buildBuiltInFieldGroupViewObject(ContentType $contentType, $editableFields) {
        $defaultFieldGroup = [];
        $defaultFieldGroup['name'] = trans('launchcms.content_edit_screen.default_field_group_name');
        $defaultFieldGroup['alias'] = 'default-group';
        $defaultFieldGroup['description'] = '';
        $defaultFieldGroup['fields'] = [];
        $allFields = $contentType->getAllFields();
        /** @var Field $field */
        foreach ($allFields as $field) {
            $fieldGroupOfField = $field->getFieldGroup();

            $fieldCanBeEdit = empty( $editableFields ) || in_array($field->getAlias(), $editableFields);
            if (empty( $fieldGroupOfField ) && $fieldCanBeEdit && !$field->isBuiltInType()) {
                $defaultFieldGroup[ 'fields' ] [] = $field;
            }
        }
        usort($defaultFieldGroup['fields'], [ 'LaunchCMS\Models\Content\ContentType', 'positionSort' ]);
        return $defaultFieldGroup;

    }

    public function buildFieldGroupViewObjects(ContentType $contentType) {
        $fieldGroupArray = [];
        $settings = $contentType->getSettings();
        $editableFields = isset( $settings[ ContentType::EDITABLE_FIELDS ] ) ? $settings[ ContentType::EDITABLE_FIELDS ] : [ ];

        $displayUserFieldGroups = $this->buildUserFieldGroupViewObjects($contentType, $editableFields);
        $fieldGroupArray = array_merge($fieldGroupArray, $displayUserFieldGroups);

        $defaultFieldGroup = $this->buildBuiltInFieldGroupViewObject($contentType, $editableFields);

        if(!empty($defaultFieldGroup['fields'])) {
            $fieldGroupArray [] = $defaultFieldGroup;
        }
        return $fieldGroupArray;
    }




}