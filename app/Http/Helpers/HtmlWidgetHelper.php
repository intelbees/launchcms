<?php

namespace App\Http\Helpers;
use LaunchCMS\Services\WorkflowEngine;
use LaunchCMS\Models\Content\WorkflowSystemStatus;

class HtmlWidgetHelper
{
    public static function buildWorkflowStatusButtonHtml($workflowStatus, WorkflowEngine $workflowEngine) {
        if(empty($workflowStatus)) {
            return null;
        }
        $status = $workflowEngine->getState($workflowStatus);
        if(empty($status)) {
            return null;
        }
        $statusName = null;
        if(isset($status['name'])) {
            $statusName = $status['name'];
        } else {
            $statusName = trans($status['trans_key']);
        }
        $systemState = $status[WorkflowEngine::MAPPED_SYSTEM_STATE];
        $cssClass = '';
        if(isset($status['css_class'])) {
            $cssClass = $status['css_class'];
        } else {
            if($systemState === WorkflowSystemStatus::DRAFT) {
                $cssClass = 'label-default';
            }
            else if($systemState === WorkflowSystemStatus::UNPUBLISHED) {
                $cssClass = 'label-primary';
            }
            else if($systemState === WorkflowSystemStatus::PUBLISHED || $systemState === WorkflowSystemStatus::SCHEDULED_PUBLISH) {
                $cssClass = 'label-success';
            }
            else if($systemState === WorkflowSystemStatus::REJECTED) {
                $cssClass = 'label-warning';
            }
        }

        $template = '<span class="label %s">%s</span>';
        return sprintf($template, $cssClass, $statusName);
    }

}