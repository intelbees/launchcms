<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use LaunchCMS\Services\Interfaces\InstallationServiceInterface;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'launchcms:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install LaunchCMS';

    /** @var  InstallationServiceInterface */
    protected $installationService;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Starting the installation".PHP_EOL);
        Cache::flush();
        $this->installationService->install();

        $this->comment("Complete the installation".PHP_EOL);
    }

    public function __construct(InstallationServiceInterface $installationService)
    {
        $this->installationService = $installationService;
        parent::__construct();
    }


}
