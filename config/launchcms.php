<?php
return [
    'admin_slug' =>  'admin',
    'admin_color_scheme' => 'skin-blue',
    'default_elastic_index' => 'launchcms',
    'widgets' => [
        'content_type_stats' => [
            'class' => 'App\Widgets\ContentTypeStatsWidget'
        ]
    ],
    'pages' => [
        'dashboard' => [
            'blocks' => [
                'stats' => [
                    'content_type_stats'
                ]
            ]
        ]
    ],
    'ui_fields' => [
        [
            'data_type' => 'string',
            'field_name_key' => 'launchcms.fields.string.name',
            'field_desc_key' => 'launchcms.fields.string.desc',
            'icon_class' => 'fa-header',
            'icon_text' => '',

        ],
        [
            'data_type' => 'boolean',
            'field_name_key' => 'launchcms.fields.boolean.name',
            'field_desc_key' => 'launchcms.fields.boolean.desc',
            'icon_class' => '',
            'icon_text' => '0/1',

        ],
        [
            'data_type' => 'double',
            'field_name_key' => 'launchcms.fields.double.name',
            'field_desc_key' => 'launchcms.fields.double.desc',
            'icon_class' => '',
            'icon_text' => '#.',

        ],
        [
            'data_type' => 'integer',
            'field_name_key' => 'launchcms.fields.integer.name',
            'field_desc_key' => 'launchcms.fields.integer.desc',
            'icon_class' => '',
            'icon_text' => '123',

        ],
        [
            'data_type' => 'date',
            'field_name_key' => 'launchcms.fields.date.name',
            'field_desc_key' => 'launchcms.fields.date.desc',
            'icon_class' => 'fa-calendar-o',
            'icon_text' => '',
        ],
        [
            'data_type' => 'datetime',
            'field_name_key' => 'launchcms.fields.datetime.name',
            'field_desc_key' => 'launchcms.fields.datetime.desc',
            'icon_class' => 'fa-clock-o',
            'icon_text' => '',
        ],
        [
            'data_type' => 'embedded',
            'field_name_key' => 'launchcms.fields.embedded.name',
            'field_desc_key' => 'launchcms.fields.embedded.desc',
            'icon_class' => 'fa-code',
            'icon_text' => '',
        ],

        [
            'data_type' => 'embedded_list',
            'field_name_key' => 'launchcms.fields.embedded_list.name',
            'field_desc_key' => 'launchcms.fields.embedded_list.desc',
            'icon_class' => 'fa-list-alt',
            'icon_text' => '',
        ],
        [
            'data_type' => 'media',
            'field_name_key' => 'launchcms.fields.media.name',
            'field_desc_key' => 'launchcms.fields.media.desc',
            'icon_class' => 'fa-file',
            'icon_text' => '',

        ],
        [
            'data_type' => 'reference',
            'field_name_key' => 'launchcms.fields.reference.name',
            'field_desc_key' => 'launchcms.fields.reference.desc',
            'icon_class' => '',
            'icon_text' => '#ID',

        ],

        [
            'data_type' => 'reference_list',
            'field_name_key' => 'launchcms.fields.reference_list.name',
            'field_desc_key' => 'launchcms.fields.reference_list.desc',
            'icon_class' => 'fa-list',
            'icon_text' => '',

        ],

        [
            'data_type' => 'geo',
            'field_name_key' => 'launchcms.fields.geo.name',
            'field_desc_key' => 'launchcms.fields.geo.desc',
            'icon_class' => 'fa-map-marker',
            'icon_text' => '',

        ],

    ],

    'admin_menu' => [
        [
            'name' => 'User',
            'label_key' => 'launchcms.menu.user',
            'url' => ['route_name' => 'cms_user_listing'],
            'permissions' => ['cms_access_user']
        ],
        [
            'name' => 'Media',
            'label_key' => 'launchcms.menu.media',
            'url' => ['route_name' => 'media_management'],
            'permissions' => ['cms_access_media']
        ],
        [
            'name' => 'Content',
            'label_key' => 'launchcms.menu.content',
            'url' => ['route_name' => 'cms_content_landing'],
            'permissions' => ['cms_access_content']
        ],
        [
            'name' => 'Structure',
            'label_key' => 'launchcms.menu.structure',
            'url' => ['route_name' => 'cms_content_type_listing_page'],
            'permissions' => ['cms_access_content_type']
        ]
    ],
    'datetime' => [
        'browser_date_format' => 'm/d/Y',
        'browser_datetime_format' => 'd/m/Y g:i A'

    ],
    'google_map_api_key' => env('GOOGLE_MAP_API_KEY', 'google_api_key'),
    'media' => [
        'webpath' => env('MEDIA_ASSET_WEB_ROOT_PATH', 'http://launchcms.dev/media'),
        //Default path is the path uses for cases that user is not specified for home folder
        'default_path' => '/',
        'auto_create_user_home_folder' => false,
        'user_home_root_path' => '/',
        'allowed_file_extension' => 'jpg;png;gif;docx;ods;xls;pdf'
    ]
];