<?php
return [
    'main_navigation' => 'Main navigation',
    'dashboard' => 'Dashboard',
    'structure' => 'Structure',
    'content_types' => 'Content types',
    'content' => 'Content',
    'data_types' => 'Data types',
    'users' => 'Users',
    'relationships' => 'Relationships',
    'workflow' => 'Workflow',
    'search' => 'Search',
    'buttons' => [
        'close' => 'Close',
        'add'   =>'Add',
        'save'  => 'Save',
        'save_and_modify'   => 'Save and Modify',
        'create'=>'Create',
        'delete' => 'Delete',
        'edit'   => 'Edit',
        'change' => 'Change',
        'move_up' => 'Move up',
        'move_down' => 'Move down',
        'next' => 'Next',
        'prev' => 'Prev'
    ],
    'fields' => [
        'string' => [
            'name' => 'Text',
            'desc' => 'Title, content, name ...'
        ],
        'boolean' => [
            'name' => 'Boolean',
            'desc' => 'Yes or No, True or False'
        ],
        'double' => [
            'name' => 'Decimal',
            'desc' => 'Money amount or precised value'
        ],
        'integer' => [
            'name' => 'Integer',
            'desc' => 'Quantity, number, rating'
        ],
        'date' => [
            'name' => 'Date',
            'desc' => 'Event date, birthday ...'
        ],
        'datetime' => [
            'name' => 'Time',
            'desc' => 'Appointment, specific time'
        ],
        'embedded' => [
            'name' => 'Embedded',
            'desc' => 'ID card of a person, address component of a house'
        ],
        'embedded_list' => [
            'name' => 'Embedded List',
            'desc' => 'Shipping addresses, order details in an order'
        ],
        'media' => [
            'name' => 'Media',
            'desc' => 'File, document, photo'
        ],
        'reference' => [
            'name' => 'Reference',
            'desc' => 'Author of a book, Provider of a product'
        ],
        'reference_list' => [
            'name' => 'Reference List',
            'desc' => 'Authors of a book, Providers of a product'
        ],
        'geo'  => [
            'name' => 'Geolocation',
            'desc' => 'Position of a place'
        ],
    ],
    'content_types_screen' => [
        'screen_title' => 'Manage content type',
        'screen_guideline' => 'Content type is the backbone of your application. Here you can define all content structures to adapt to your business. Firstly, create content type. Then, add more fields to it to match to the requirement of the real life',
        'create_dialog_title' => 'Create Content type',
        'name' => 'Name',
        'name_placeholder' => 'Content type name',
        'alias' => 'Alias',
        'alias_placeholder' => 'Alias - unique name to identify your content type',
        'description' => 'Description',
        'description_placeholder' => 'Please describe more information about this content type',
        'inherit_from' => 'Inherit from',
        'enable_versioning' => 'Enable versioning',
        'enable_localization' => 'Enable localization',
        'primary_locale' => 'Primary content locale',
        'table_display_fields' => 'Fields to display in table view',
        'filter_fields' => 'Fields for filtering',
        'editable_fields' => 'Editable fields',
        'note_for_default_filter_fields' => 'If no field selected, name field will be used',
        'note_for_default_table_display_fields' => 'If no field selected, name field will be used',
        'note_for_default_editable_fields' => 'If no field selected, all fields will be editable',
        'messages' => [
            'content_type_save_successfully' => 'The content type has been saved successfully!',
            'content_type_deleted_successfully' => 'The content type has been deleted successfully!'
        ]
    ],

    'content_landing_screen' => [
        'screen_title' => 'Manage content',
        'screen_guideline' => 'This is the content dashboard - where you can manage all contents that you have permission',
        'guideline_header' => 'Description & guideline',

    ],
    'content_listing_screen' => [
        'screen_title' => 'Manage content',
        'guideline_header' => 'Introduction & guideline',
        'create_dialog_title' => 'Create :content_type',
        'name' => 'Name',
        'name_placeholder' => 'Name of the :content_type',
        'slug' => 'Slug',
        'slug_placeholder' => '(Optional). Can be used for SEO if your app is a website.',
    ],
    'data_types_screen' => [
        'screen_title' => 'Manage data type',
        'screen_guideline' => '<p>Data type is the data structure which can be embedded to other Content Types via Embedded Field or Embedded List Field. </p><p>You can define fields in Data Type similar to Content Type. Nested structure of Data Type is not supported for now.</p>',
        'create_dialog_title' => 'Create Data type',
        'name' => 'Name',
        'name_placeholder' => 'Data type name',
        'alias' => 'Alias',
        'alias_placeholder' => 'Alias - unique name to identify your data type',
        'description' => 'Description',
        'description_placeholder' => 'Please describe more information about this data type',
        'table_display_fields' => 'Fields to display in table view',
        'editable_fields' => 'Editable fields',
        'note_for_default_table_display_fields' => 'If no field selected, all fields (exclude rich text fields) will be used',
        'note_for_default_editable_fields' => 'If no field selected, all fields will be editable',
        'messages' => [
            'data_type_save_successfully' => 'The data type has been saved successfully!',
            'data_type_deleted_successfully' => 'The data type has been deleted successfully!'
        ]
    ],
    'field_dialog' => [
        'title' => 'Field dialog',
        'text_field_title' => 'Text field',
        'boolean_field_title' => 'Boolean field',
        'integer_field_title' => 'Integer field',
        'double_field_title' => 'Decimal field',
        'date_field_title' => 'Date field',
        'geo_field_title' => 'Geo field',
        'datetime_field_title' => 'Date time field',
        'embedded_field_title' => 'Embedded field',
        'embedded_list_field_title' => 'Embedded list field',
        'reference_field_title' => 'Reference field',
        'reference_list_field_title' => 'Reference list field',
        'field_meta_data_tab' => 'Field meta data',
        'field_validation_tab' => 'Validation',
        'field_configuration_tab' => 'Configuration',
        'name_placeholder' => 'User friendly name of the field',
        'name'  => 'Name',
        'alias_placeholder' => 'Unique alias to identify the field inside this content type',
        'alias' => 'Alias',
        'position' => 'Position',
        'position_placeholder' => 'Position index. 0 is the top',
        'description'   => 'Description',
        'description_placeholder' => 'Description to introduce the field to the content editor',
        'allow_full_text_search' => 'Allow full text search on this field',
        'is_indexed' => 'Create indexing for this field',
        'is_unique' => 'Is unique',
        'is_required' => 'Is required',
        'field_group' => 'Field group'
    ],
    'relationship_definition_screen' => [
        'screen_title' => 'Manage relationship definition',
        'screen_guideline' => 'You can define whatever user relationship definition as you like, eg; friend, follow',
        'messages' => [
            'relationship_save_successfully' => 'Relationship has been saved successfully',
            'relationship_deleted_successfully' => 'Relationship has been deleted successfully',
        ],
        'name'  => 'Name',
        'name_placeholder'  => 'Name of the relationship',
        'alias' => 'Alias',
        'alias_placeholder' => 'Alias - a unique identifier for the relationship',
        'inverse' => 'Inverse',
        'need_approval' => 'Need approval',
        'create_dialog_title' => 'Relationship'
    ],
    'workflow_definition_screen' => [
        'screen_title' => 'Manage workflow definition',
        'screen_guideline' => 'Here you can define workflow yourself by using our JSON workflow schema',
        'messages' => [
            'workflow_save_successfully' => 'Workflow has been saved successfully',
            'workflow_deleted_successfully' => 'Workflow has been deleted successfully',
        ],
        'name'  => 'Name',
        'name_placeholder'  => 'Name of the workflow',
        'alias' => 'Alias',
        'alias_placeholder' => 'Alias - a unique identifier for the workflow',
        'json' => 'Workflow JSON',
        'create_dialog_title' => 'Workflow'
    ],

    'content_type_edit_screen' => [
        'header' => ':name',
        'subheader' => 'Content type',
        'screen_title' => 'Content type information',
        'screen_guideline' => 'You can modify content type information, settings and fields to define the structure',
        'inherit_from' => 'inherits from',
        'attached_workflow' => 'Workflow',
        'field_group_table' => [
            'name'  => 'Name',
            'alias' => 'Alias',
            'position' => 'Position',
            'description'   => 'Description',

        ],
        'field_table' => [
            'name'  => 'Name',
            'alias' => 'Alias',
            'position' => 'Position',
            'field_group' => 'Field group',
            'description'   => 'Description',
            'type'  => 'Type'
        ],

        'field_group_dialog' => [
            'title' => 'Field group',
            'name_placeholder' => 'Name of the group',
            'name'  => 'Name',
            'alias_placeholder' => 'Group identifier',
            'alias' => 'Alias',
            'description'   => 'Description',
            'position' => 'Position',
            'position_placeholder' => 'Position index. 0 is the top field group',
            'description_placeholder' => 'Description to introduce the field group to the content editor',
        ],
        'messages' => [
            'field_group_save_successfully' => 'Field group has been saved successfully',
            'field_group_deleted_successfully' => 'Field group has been deleted successfully',
            'field_save_successfully' => 'Field has been saved successfully',
            'field_deleted_successfully' => 'Field has been deleted successfully'
        ]

    ],

    'data_type_edit_screen' => [
        'header' => ':name',
        'subheader' => 'Data type',
        'screen_title' => 'Data type information',
        'screen_guideline' => 'You can modify data type information and fields to define the embedded structure',
        'field_table' => [
            'name'  => 'Name',
            'alias' => 'Alias',
            'position' => 'Position',
            'field_group' => 'Field group',
            'description'   => 'Description',
            'type'  => 'Type'
        ],
        'messages' => [
            'field_group_save_successfully' => 'Field group has been saved successfully',
            'field_group_deleted_successfully' => 'Field group has been deleted successfully',
            'field_save_successfully' => 'Field has been saved successfully',
            'field_deleted_successfully' => 'Field has been deleted successfully'
        ]

    ],

    'content_edit_screen' => [
        'meta_field_group_name' => 'Meta data',
        'default_field_group_name' => 'Content',
        'meta_fields' => [
            'name' => 'Name',
            'name_placeholder' => 'Content name',
            'slug' => 'Slug',
            'slug_placeholder' => 'Slug can be used for SEO for the web application',
            'status' => 'Status',
            'scheduled_time' => 'Scheduled time',
            'created_at' => 'Created',
            'updated_at' => 'Updated'
        ],
        'messages' => [
            'content_save_successfully' => 'Content has been saved successfully.',
            'content_delete_successfully' => 'Content has been saved successfully.'
        ],
        'workflow_dialog' => [
            'title' => 'Change state of content',
            'note' => 'Note',
            'note_placeholder' => 'Workflow note',
            'schedule_time' => 'Scheduled time'
        ]
    ],
    'media_screen' => [
        'messages' => [
            'folder_has_been_deleted_successfully' => 'Folder :folder has been deleted successfully',
            'media_items_has_been_deleted_successfully' => 'Media item(s) has been deleted successfully',
            'file_has_been_upload_successfully' => 'File :file has been uploaded successfully',
            'file_has_been_deleted_successfully' => 'File :file has been deleted successfully',
            'folder_has_been_created' => 'Folder has been created'
        ]
    ],
    'login' => [
        'login_message' => 'Sign in to start your session',
        'signin'    => 'Sign In',
        'error' => 'Error',
    ],
    'menu' => [
        'content' => 'Content',
        'content_type' => 'Content Type',
        'structure' => 'Structure',
        'user' => 'User',
        'media' => 'Media'

    ],
    'messages' => [
        'invalid_credential' => 'Invalid credential',
        'not_admin_user'    => 'You are not admin user!',
        'are_you_sure'  => 'Are you sure?',
        'are_you_sure_to_delete'  => 'Are you sure to delete',
        'processing' => 'Data is processing. Please keep calm!',
        'confirm_delete_media_paths' => 'Are you sure to delete media path(s):',
        'cannot_use_local_storage' => 'Local storage is not supported by your browser. Please disable "Private Mode", or upgrade to a modern browser.',
        'global_validation_error' => 'There are still some validation errors on the current page. Please check them before doing the next action.',
        'built_in_type_guidance' => '<i class="fa fa-star"></i> <span>is built-in type. You can modify, but cannot delete or change its alias.</span>'
    ],

    'pager' => [
        'next' => 'Next',
        'prev' => 'Prev',
        'showing' => 'Showing',
        'to' => 'to',
        'of' => 'of',
        'entries' => 'entries'
    ],
    'common_label' => [
        'error' => 'Error',
        'please_select' => 'Please select',
        'none' => 'None',
        'action' => 'Action',
        'yes' => 'Yes',
        'no'  => 'No'

    ],
    'confirmation_dialog' => [
        'title' => 'Confirmation',
        'cancel_button' => 'Cancel',
        'confirm_button' => 'Confirm'
    ],
    'content_builtin_field' => [
        '_id' => 'ID',
        'name' => 'Name',
        'slug' => 'Slug',
        'last_version_number' => 'Last version',
        'created_user_id' => 'Created user',
        'created_time' => 'Created time',
        'last_updated_time' => 'Last updated time',
        'trashed' => 'Trashed',
        'workflow_status' => 'Workflow status'
    ],
    'workflow_status' => [
        'draft' => 'Draft',
        'published' => 'Published',
        'unpublished' => 'Unpublished'
    ],
    'cms_errors' => [
        /** Content exception  **/
        'content_empty_name' => 'You cannot set empty name for content',
        'invalid_content' => 'The content field value is violate data constraints',
        'content_not_found' => 'Content not found',
        /** End content exception  **/

        /** Content type exception  **/
        'content_type_empty_alias'  => 'You cannot set empty alias for content type.',
        'content_type_empty_name'   => 'You cannot set empty name for content type.',
        'invalid_inherit_content_type' => 'The inherit content type is invalid.',
        'delete_inherit_content_type' => 'This content type is inherited by others. You cannot delete it until you delete all child content types.',
        'content_type_not_found'    => 'Content type cannot be found in the system.',
        'content_type_alias_invalid' => 'Content type alias is invalid. It must follow variable naming style and cannot be duplicated with builtin collections',
        'primary_locale_required_when_localization_enabled' => 'Primary locale is required when localization is enabled',

        /** End content type exception  **/

        /** Data type exception  **/
        'data_type_alias_invalid' => 'Data type alias is invalid. Please make data type alias as an variable name',
        'duplicated_data_type' => 'Duplicated data type alias',
        'data_type_alias_empty' => 'You cannot set empty value for data type alias',
        'data_type_not_found' => 'Data type not found',
        'data_type_still_have_reference' => 'Data type still have reference. You cannot delete it',
        'field_invalid_type' => 'This field type cannot be used for data type',
        /** End data type exception  **/


        /** Field exception  **/
        'duplicated_field'  => 'Duplicated field',
        'field_alias_empty' => 'You cannot set empty alias for the field.',
        'field_not_found' => 'Field not found',
        'invalid_validation_rule' => 'Invalid validation rule.',
        'field_alias_invalid'       => 'Field alias is invalid. It must follow variable naming style and cannot be builtin fields',
        'invalid_field_data_type' => 'Invalid field data type',
        'invalid_reference_content_type' => 'Invalid reference content type',
        'invalid_reference_data_type' => 'Invalid reference data type',
        'cannot_make_unique_index' => 'Your data on this field violates unique constraint. Please make sure data of the field is not duplicated',
        'cannot_make_index' => 'Cannot make indexing on this field because of runtime error. Please do this using Mongo console to see the log detail',
        'cannot_drop_index' => 'Cannot drop index on this field because of runtime error. Please do this using Mongo console to see the log detail',
        /** End field exception  **/


        /** Field group exception  **/
        'duplicated_group' => 'Duplicated field group',
        'field_group_alias_empty' => 'You cannot set empty alias for group.',
        'field_group_not_found' => 'Field group cannot be found in the system.',
        'field_group_alias_invalid' => 'Field group alias is invalid. It must follow variable naming style.',
        /** End field group exception  **/

        /** Organization exception  **/
        'invalid_site_alias' => 'Invalid site alias',
        'invalid_site_name' => 'Invalid site name',
        'duplicated_site_alias' => 'Duplicated site alias',
        'site_not_found' => 'Site not found',
        'invalid_group_alias' => 'Invalid group alias',
        'invalid_group_name' => 'Invalid group name',
        'duplicated_group_alias' => 'Duplicated group alias',
        'group_not_found' => 'Group not found',
        /** End organization exception  **/

        /** User exception  **/
        'relationship_not_found'  => 'Relationship not found',
        'connection_request_not_found' => 'Connection request cannot be found.',
        'user_email_is_invalid' => 'User email is empty or invalid',
        'user_password_is_invalid' => 'User password is empty or invalid',
        'cannot_set_empty_alias_for_relationship_definition' => 'Cannot set empty alias for relationship definition',
        'invalid_relationship_definition_alias' => 'Invalid relationship definition alias. It must be named as variable name and in lower case',
        'duplicated_relationship_definition_alias' => 'Duplicated relationship definition alias',
        /** End user exception  **/

        /** Workflow exception  **/
        'invalid_workflow_json_format' => 'Invalid workflow json format/schema',
        'invalid_workflow_state'  => 'Invalid workflow state',
        'no_trigger_rule_to_the_new_state' => 'No transition rule can trigger this state',
        'cannot_set_empty_workflow_alias' => 'You cannot set empty for workflow alias',
        'invalid_workflow_alias' => 'Workflow alias name must follow variable naming style and all characters are in lowercase',
        'workflow_still_have_references' => 'You cannot delete current workflow as it still has references from other content types',
        'workflow_not_found' => 'Workflow not found',
        'schedule_time_is_required_for_this_state' => 'Schedule time is required for this state',
        /** End workflow exception  **/

        /** Media exception  **/
        'no_permission_to_access_path' => 'You do not have enough permission to access this folder'
    ],
    'workflow_errors' => [
        'invalid_json_format' => 'Bad JSON format',
        'missing_states_section' => 'JSON is missing states section',
        'missing_transitions_section' => 'JSON is missing transitions section',
        'invalid_state_data' => 'Invalid state data. State must contains alias, name (or trans_key) and mapped_system_state',
        'invalid_mapped_system_state' => 'Mapped system state must be in ["draft", "published", "rejected", "scheduled_publish", "unpublished"]',
        'invalid_transition_data' => 'Transition must contains from, to, name (or trans_key)',
        'transition_point_to_non_exist_state' => 'Transition is pointed to a non exist state',
        'duplicated_state' => 'Duplicated state declaration',
    ],
    'validation' => [
        'invalid_data_type' => 'Invalid data for :type type'
    ],
    'default_workflow' => [
        'states' => [
            'scheduled_publish' => 'Scheduled to publish',
            'draft' => 'Draft',
            'published' => 'Published'
        ],
        'transitions' => [
            'send_to_draft' => 'Send to draft',
            'send_to_publish' => 'Send to publish',
            'schedule_to_publish' => 'Schedule to publish',
            'send_back_to_draft' => 'Send back to draft'
        ]
    ]

];
