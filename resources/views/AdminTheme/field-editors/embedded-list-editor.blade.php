<hr/>
<h3>{{ $field->getName() }} </h3>
<p><small>{{$field->getDescription()}}</small></p>
<div class="row">
    <span class="pull-right">
        <a href="#" class="btn btn-primary margin-bottom btn-create-embedded-list-item"
           data-modal-id="#data-type-{{$dataType->getAlias()}}-modal"
           data-field-alias="{{$field->getAlias()}}"
           data-form-id="#data-type-{{$dataType->getAlias()}}-form">
            <i class="fa fa-plus"></i> New {{$dataType->getName()}}</a>
    </span>
</div>


<div class="row">

    <table class="table table-bordered" id="table-{{$field->getAlias()}}">
        <thead>
        @foreach($displayFields as $displayField)
            <th>{{$displayField->getName()}}</th>
        @endforeach

        <th style="width: 160px">{{trans("launchcms.common_label.action")}}</th>
        </thead>
        <tbody>
            @foreach($dataRows as $dataRow)
                <tr>
                @foreach($dataRow as $key => $value)
                    @if($key !== '_id')
                    <td>
                        {{$value}}
                    </td>
                    @endif
                @endforeach
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#" class="btn-embedded-record-edit"
                                       data-record-id="{{$dataRow['_id']}}"
                                       data-modal-id="#data-type-{{$dataType->getAlias()}}-modal"
                                       data-field-alias="{{$field->getAlias()}}"
                                       data-form-id="#data-type-{{$dataType->getAlias()}}-form">Edit</a></li>
                                <li><a href="#"
                                        data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete_current_record')}} ?"
                                        data-record-id="{{$dataRow['_id']}}"
                                        data-field-alias="{{$field->getAlias()}}"
                                       class="btn-embedded-record-delete">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
</div>

<hr/>

<script type="ejs-template" id="tpl-{{$field->getAlias()}}-body">
    <% for(var i=0; i<data.length; i++) {%>
    <tr>

            @foreach($displayFields as $displayField)
            <td><%= data[i].{{$displayField->getAlias()}} %></td>
            @endforeach

            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a class="btn-embedded-record-edit"
                               data-record-id="<%=data[i]['id']%>"
                               data-modal-id="#data-type-{{$dataType->getAlias()}}-modal"
                               data-field-alias="{{$field->getAlias()}}"
                               data-form-id="#data-type-{{$dataType->getAlias()}}-form">Edit</a>
                        </li>

                        <li>
                            <a href="#"
                                data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete_current_record')}} ?"
                                data-record-id="<%=data[i]['id']%>"
                                data-field-alias="{{$field->getAlias()}}"
                                class="btn-embedded-record-delete">Delete</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <% } %>
    </script>