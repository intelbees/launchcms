<div class="form-group">
    <label class="col-sm-2 control-label">{{ $field->getName() }}</label>
    <div class="col-sm-10">

        @if($editorType === 'text_field')
        <div class="row input-row">
        <input type="text"
               @if(empty($embeddedFieldAlias))
               name="{{$field->getAlias()}}"
               @else
               name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
               @endif

               class="form-control field-element"
               @if($field->isRequired())
               required="" data-parsley-required="true"
               data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
               @endif
               @if($value != null) value="{{$value}}" @endif
               placeholder="{{$field->getDescription()}}">
         </div>
        @elseif($editorType ==='plain_text_area')
        <div class="row input-row">
            <textarea
                    @if(empty($embeddedFieldAlias))
                    name="{{$field->getAlias()}}"
                    @else
                    name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
                    @endif
                    @if($field->isRequired())
                    required="" data-parsley-required="true"
                    data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
                    @endif
               class="form-control field-element text-area">@if($value != null) {{$value}} @endif</textarea>
        </div>

        @elseif($editorType ==='rich_text')
            <div class="row input-row">
                <textarea id="ckeditor-{{$field->_id}}"
                          data-editor-id="ckeditor-{{$field->_id}}"
                      @if(empty($embeddedFieldAlias))
                        name="{{$field->getAlias()}}"
                      @else
                        name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
                      @endif

                      @if($field->isRequired())
                      required="" data-parsley-required="true"
                      data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
                      @endif
                      class="form-control field-element rich-text ckeditor">@if($value != null) {{$value}}@endif</textarea>


            </div>

        @endif
        <div class="row">
            <small>{{$field->getDescription()}}</small>
        </div>
        <div class="row">
            <span id="field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"></span>
        </div>
    </div>
</div>