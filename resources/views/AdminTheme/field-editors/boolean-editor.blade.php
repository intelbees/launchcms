<div class="form-group">
    <label class="col-sm-2 control-label">{{ $field->getName() }}</label>
    <div class="col-sm-10">
        <div class="row input-row">
            <select
                    @if(empty($embeddedFieldAlias))
                    name="{{$field->getAlias()}}"
                    @else
                    name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
                    @endif
                    @if($field->isRequired())
                    required="" data-parsley-required="true"
                    data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
                    @endif

                    class="form-control field-element boolean-field">
                    <option value="">{{trans('launchcms.common_label.please_select')}}</option>
                    <option value="true">{{trans('launchcms.common_label.yes')}}</option>
                    <option value="false">{{trans('launchcms.common_label.no')}}</option>
            </select>
        </div>
        <div class="row">
            <small>{{$field->getDescription()}}</small>
        </div>
        <div class="row">
            <span id="field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"></span>
        </div>
    </div>
</div>