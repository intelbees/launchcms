@inject('editorFactory', 'App\FieldEditors\EditorFactory')
<hr/>
<h3>{{ $field->getName() }}</h3>
<small>{{$field->getDescription()}}</small>
<div class="field-panel">
    @foreach($dataTypeFields as $dataTypeField)
        @if ($editorFactory->getEditor($dataTypeField, $embeddedField) != null)
            {!! $editorFactory->getEditor($dataTypeField, $embeddedField)->render($content) !!}
        @endif
    @endforeach

</div>
<hr/>