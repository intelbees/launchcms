<div class="form-group">
    <label class="col-sm-2 control-label">{{ $field->getName() }}</label>
    <div class="col-sm-10">
        <div class="row input-row">
        <select style="width: 300px"
               @if(empty($embeddedFieldAlias))
               name="{{$field->getAlias()}}"
               @else
               name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
               @endif
               data-content-type-id="{{$contentTypeID}}"
               data-search-url="{{$searchURL}}"
               class="form-control field-element reference-field"
               @if($field->isRequired())
               required="" data-parsley-required="true"
               data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
               @endif

               >
            @if(!empty($selectedContent))
            <option value="{{$selectedContent->id}}" selected="selected">{{$selectedContent->name}}</option>
            @endif
        </select>
        </div>
        <div class="row">
            <small>{{$field->getDescription()}}</small>
        </div>
        <div class="row">
            <span id="field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"></span>
        </div>
    </div>
</div>