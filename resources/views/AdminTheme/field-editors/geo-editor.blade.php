<div class="form-group field-geo">
    <label class="col-sm-2 control-label">{{ $field->getName() }}</label>
    <div class="col-sm-10">
        <div class="row input-row">
            <input type="text" class="form-control pull-right field-geo-input"
                   data-map-canvas-id="map_canvas_{{(string) $field->_id}}"
                   data-label-field-id="geo-label-{{(string) $field->_id}}"
                   data-hidden-field="geo_hidden_{{(string) $field->_id}}"
            />
            <input type="hidden" id="geo_hidden_{{(string) $field->_id}}"
                    @if($value != null) value="{{$value['lat']}},{{$value['lng']}}" @endif
                    @if(empty($embeddedFieldAlias))
                    name="{{$field->getAlias()}}"
                    @else
                    name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
                    @endif

                    @if($field->isRequired())
                    required="" data-parsley-required="true"
                    data-parsley-trigger="input focusin focusout"
                    data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
                    @endif

            />
        </div>

        <div style="height:100%; width:100%;">
            <div class="map_canvas" id="map_canvas_{{(string) $field->_id}}">

            </div>
        </div>
        <div class="row input-row">

            <span class="col-sm-12" id="geo-label-{{(string) $field->_id}}">
                @if($value != null)  Longitude = {{$value['lng']}}. Latitude = {{$value['lat']}}  @endif
            </span>
        </div>

        <div class="row">
            <small>{{$field->getDescription()}}</small>
        </div>
        <div class="row">
            <span id="field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"></span>
        </div>
    </div>
</div>