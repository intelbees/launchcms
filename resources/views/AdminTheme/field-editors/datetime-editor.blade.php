<div class="form-group">
    <label class="col-sm-2 control-label">{{ $field->getName() }}</label>
    <div class="col-sm-10">
        <div class="row input-row">
            <div class="input-group datetime">
                <input type="text" class="form-control pull-right field-datetime-picker"
                       @if($field->isRequired())
                       required="" data-parsley-required="true"
                       data-parsley-trigger="input focusin focusout"
                       data-parsley-errors-container="#field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"
                       @endif
                       @if($value != null) value="{{$value}}" @endif
                       @if(empty($embeddedFieldAlias))
                       name="{{$field->getAlias()}}"
                       @else
                       name="{{$embeddedFieldAlias.'['. $field->getAlias().']'}}"
                        @endif
                       />
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <small>{{$field->getDescription()}}</small>
        </div>
        <div class="row">
            <span id="field_{{$embeddedFieldAlias}}{{$field->getAlias()}}_error"></span>
        </div>
    </div>
</div>