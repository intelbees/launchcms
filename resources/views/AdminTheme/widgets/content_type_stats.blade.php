<div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-database"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"> <a href="{{route('cms_content_type_listing_page')}}"> {{ trans('launchcms.content_types') }}</a></span>
        <span class="info-box-number">{{ $countContentType }}<small></small></span>
    </div><!-- /.info-box-content -->
</div><!-- /.info-box -->
