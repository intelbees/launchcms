@foreach($contentTypes as $contentType)
    @if(!$contentType->isBuiltinType())
        <li>
            <a href="{{route('cms_content_listing', ['content_type_alias' => $contentType->getAlias() ])}}">
                {{ $contentType->getName() }}
            </a>
        </li>
    @endif
@endforeach