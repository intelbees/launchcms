<ul class="nav navbar-nav">
    @foreach ($adminMenu as $menuItem)
        @if(!empty($menuItem['permissions']) && $user->hasAccess($menuItem['permissions']))
            @if (empty($menuItem['children']) && !isset($menuItem['item_renderer']))
            <li><a href="{{$menuItem['url']}}">{{$menuItem['label']}}</a></li>
            @else
            <li class="dropdown">
                <a href="{{$menuItem['url']}}" class="dropdown-toggle" data-toggle="dropdown">{{$menuItem['label']}}<span class="caret"></span></a>

                <ul class="dropdown-menu" role="menu">
                    @if(isset($menuItem['children']))
                        @foreach ($menuItem['children'] as $secondLevelItem)
                        <li><a href="{{$secondLevelItem['url']}}">{{$secondLevelItem['label']}}</a></li>
                        @endforeach
                    @else
                        @if(isset($menuItem['item_renderer']))
                        {!! $menuItem['item_renderer']->render() !!}
                        @endif
                    @endif

                </ul>
            </li>
            @endif
        @endif

    @endforeach

</ul>