<?php
$themeClass = config('launchcms.admin_color_scheme');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LaunchCMS | Administration</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{Theme::url('bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{Theme::url('dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{Theme::url('dist/css/skins/'.$themeClass.'.min.css')}}">
    <link rel="stylesheet" href="{{Theme::url('dist/css/custom.css')}}">
    @section('page_css')

    @show

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition sidebar-mini {{$themeClass}}">
<div class="wrapper">

    @include('layout.header')
    @yield('sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')

    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2016 <a href="#">IntelBees</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">

    </aside><!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<div class="modal" id="confirmation-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{trans('launchcms.confirmation_dialog.title')}}</h4>
            </div>
            <div class="modal-body">
                <p class="message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left btn-cancel" data-dismiss="modal">{{trans('launchcms.confirmation_dialog.cancel_button')}}</button>
                <button type="button" class="btn btn-warning btn-confirm">{{trans('launchcms.confirmation_dialog.confirm_button')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery 2.1.4 -->
<script src="{{Theme::url('plugins/jQuery/jquery-2.1.4.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="{{Theme::url('bootstrap/js/bootstrap.min.js')}}"></script>

<!-- AdminLTE App -->

<script src="{{Theme::url('plugins/storejs/store.min.js')}}"></script>
<script src="{{Theme::url('plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="{{Theme::url('plugins/ejs/ejs_production.js')}}"></script>
<script src="{{Theme::url('dist/js/app.min.js')}}"></script>
<script src="{{Theme::url('dist/js/core.js')}}"></script>
@include('support.localization')
<script language="javascript">
    launchcms.adminSlug = '{{config('launchcms.admin_slug')}}';
</script>
@section('page_script')

@show

<input id="page_token" type="hidden" value="{!! csrf_token() !!}" />
</body>
</html>
