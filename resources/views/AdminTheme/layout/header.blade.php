<header class="main-header">
    <!-- Logo -->
    <a href="{{route('cms_dashboard')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><i class="fa fa-rocket"></i>CMS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><i class="fa fa-rocket"></i>&nbsp;<b>Launch</b>CMS</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        @include('layout.header-menu')
    </nav>
</header>