<header class="main-header">


    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
            <!-- Logo -->
            <a href="{{route('cms_dashboard')}}" class="logo navbar-brand">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><i class="fa fa-rocket"></i>CMS</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><i class="fa fa-rocket"></i>&nbsp;<b>Launch</b>CMS</span>
            </a>

        </div>
        @include('layout.header-menu')

    </nav>
</header>