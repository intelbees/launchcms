<div class="navbar-custom-menu">
    @include('layout.main_menu')

    <ul class="nav navbar-nav">
        <!-- include layout notification section here. Now removed to reduce scope-->

        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{Theme::url('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{ Sentinel::getUser()->email }}</span>
            </a>
            <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                    <img src="{{Theme::url('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                    <p>
                        {{$user->name}}
                        <small>Member since {{$user->created_at->toFormattedDateString()}}</small>
                    </p>
                </li>
                <!-- Menu Body -->
                <!--li class="user-body">
                    <div class="col-xs-4 text-center">

                    </div>
                    <div class="col-xs-4 text-center">

                    </div>
                    <div class="col-xs-4 text-center">

                    </div>
                </li-->
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-left">
                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('cms_logout')}}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                </li>
            </ul>
        </li>

    </ul>
</div>