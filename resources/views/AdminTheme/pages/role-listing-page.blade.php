@extends('pages.content-type-based-listing-page')
@section('sidebar')
@include('pages.commons.user-sidebar', ['activeMenuItem' => 'roles'])
@overwrite

@section('page_header')
        <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $contentType->getName() }}
            <small>entries</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{trans('launchcms.users')}}</li>
        </ol>
    </section>
@endsection

@section('content_header')
    <input type="hidden" id="content_type_alias" value="{{$contentType->getAlias()}}"/>
    <div class="box-header with-border">
        @if(!empty($contentType->description))
            <p class="content-type-intro">
                <small>{{$contentType->description}}</small>
            </p>

        @endif
        <div class="pull-right">
            @if($user->hasAccess('cms_create_role'))
                <a class="btn bg-olive" data-toggle="modal" data-target="#create-content-modal" ><i class="fa fa-edit"></i> {{trans('launchcms.buttons.create')}}</a>
            @endif
        </div>
    </div><!-- /.box-header -->
@overwrite

@section('content_modal')
    <div class="modal" id="create-content-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- form start -->
                <form class="form-horizontal"
                      id="create-content-form">
                    <input type="hidden" name="content_type_alias" value="{{$contentType->getAlias()}}"/>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{trans('launchcms.content_listing_screen.create_dialog_title', ['content_type' => $contentType->getName()])}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{trans('launchcms.content_listing_screen.name')}}(*)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control"
                                           required="" data-parsley-required="true"
                                           placeholder="Name of role">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Slug(*)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="slug" class="form-control"
                                           required="" data-parsley-required="true"
                                           placeholder="Role slug">
                                </div>
                            </div>
                            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                        </div><!-- /.box-body -->
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                        <button type="button" class="btn btn-primary btn-save-modify">{{trans('launchcms.buttons.save_and_modify')}}</button>
                        <button type="button" class="btn btn-default btn-save">{{trans('launchcms.buttons.save')}}</button>
                    </div>
                    <div class="alert alert-error hidden error-panel">
                        <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('ejs_template')
    <script type="ejs-template" id="tpl-content-body">
        <% for (var i=0; i<data.length; i++) {%>
        <tr>
            <% for (var fieldIndex=0; fieldIndex < displayFields.length; fieldIndex++) {
            var fieldAlias = displayFields[fieldIndex].alias;
            if (displayFields[fieldIndex].type == 'reference') {
            fieldAlias = displayFields[fieldIndex].alias + '_text';
            }
            %>
            <td><%= data[i][fieldAlias] %></td>
            <% } %>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<%= data[i].edit_url %>" class="btn-content-edit">Edit</a></li>
                        <li><a href="#"  data-id="<%= data[i]._id %>"
                         data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete')}}: <%=data[i].name %> ?"
                        class="btn-content-delete">Delete</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <% } %>
    </script>
@endsection