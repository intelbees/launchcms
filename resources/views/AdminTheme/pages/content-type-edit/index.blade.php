@extends('layout.layout')

@section('page_css')

    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/edit-content-type-page.js')}}"></script>

    <script src="{{Theme::url('dist/js/ui-field.js')}}"></script>

@endsection

    @section('sidebar')
        @include('pages.commons.structure-sidebar', ['activeMenuItem' => 'content_type'])
    @endsection
    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{trans('launchcms.content_type_edit_screen.header', ['name' => $contentType->name])}}
            <small> {{trans('launchcms.content_type_edit_screen.subheader')}}
                @if(!empty($inheritContentType))
                    - {{trans('launchcms.content_type_edit_screen.inherit_from')}}
                    <em><a href="{{route('cms_edit_content_type_page',['alias' => $inheritContentType->getAlias()])}}"> {{$inheritContentType->name}}</a></em>
                @endif
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{trans('launchcms.structure')}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <input type="hidden" id="content_type_alias" value="{{$contentType->alias}}"/>
        <!-- Main row -->
        <div class="row">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    <li><a href="#tab_settings" data-toggle="tab">Entry settings</a></li>
                    <li><a href="#tab_fields" data-toggle="tab">Fields</a></li>
                    <li><a href="#tab_field_groups" data-toggle="tab">Field Groups</a></li>
                    <li class="active"><a href="#tab_general" data-toggle="tab">General</a></li>

                    <li class="pull-left header"><i class="fa fa-th"></i> {{trans('launchcms.content_type_edit_screen.screen_title')}}</li>
                </ul>
                <div class="tab-content">
                    @include('pages.content-type-edit.settings-tab')
                    @include('pages.content-type-edit.field-tab')
                    @include('pages.content-type-edit.field-groups-tab')
                    @include('pages.content-type-edit.general-tab')

                </div><!-- /.tab-content -->

            </div><!-- nav-tabs-custom -->
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->



    <input type="hidden" id="structure_field_save_api_alias" value="content-type-field-save"/>


@endsection
