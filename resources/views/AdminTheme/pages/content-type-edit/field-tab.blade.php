<div class="tab-pane" id="tab_fields">
    <div class="box">
        <div class="box-header with-border">
            <div class="pull-right">

                <a class="btn bg-olive btn-create-field"><i class="fa fa-edit"></i> {{trans('launchcms.buttons.create')}}</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered" id="table-field">
                <thead>
                    <th>{{trans('launchcms.content_type_edit_screen.field_table.name')}}</th>
                    <th>{{trans('launchcms.content_type_edit_screen.field_table.alias')}}</th>
                    <th>{{trans('launchcms.content_type_edit_screen.field_table.type')}}</th>
                    <th>{{trans('launchcms.content_type_edit_screen.field_table.field_group')}}</th>
                    <th>{{trans('launchcms.content_type_edit_screen.field_table.position')}}</th>
                    <th style="width: 160px">{{trans("launchcms.common_label.action")}}</th>
                </thead>
                <tbody>

                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-footer clearfix">

        </div>
    </div><!-- /.box -->
</div><!-- /.tab-pane -->


@include('pages.commons.create-field-dialog')

@include('pages.commons.fields.string-field')
@include('pages.commons.fields.boolean-field')
@include('pages.commons.fields.integer-field')
@include('pages.commons.fields.double-field')
@include('pages.commons.fields.date-field')
@include('pages.commons.fields.datetime-field')
@include('pages.commons.fields.embedded-field')
@include('pages.commons.fields.embedded-list-field')
@include('pages.commons.fields.reference-field')
@include('pages.commons.fields.reference-list-field')
@include('pages.commons.fields.geo-field')

<script type="ejs-template" id="tpl-table-field-body">
    <% for(var i=0; i<data.length; i++) {%>
    <tr>
            <td>
                <%= data[i].name %>
                <% if(data[i].builtin) { %>
                    <i class="fa fa-star"></i>
                <% } %>
            </td>
            <td><%= data[i].alias %></td>
            <td><%= data[i].data_type_text %></td>
            <td>
                <%= data[i].field_group_name %>
            </td>
            <td>
                <%= data[i].position %>
            </td>
            <td>
                <% if(!data[i].builtin) { %>
                <div class="btn-group">
                    <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#" data-alias="<%= data[i].alias %>" class="btn-field-edit">{{trans('launchcms.buttons.edit')}}</a></li>
                        <li><a href="#"  data-alias="<%= data[i].alias %>"
                         data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete')}} <%=data[i].name %> ?"
                        class="btn-field-delete">{{trans('launchcms.buttons.delete')}}</a></li>
                    </ul>
                </div>
                <% } %>
            </td>
        </tr>
        <% } %>
</script>


<script type="ejs-template" id="tpl-table-rule-body">
    <% for(var i=0; i<data.length; i++) {%>
    <tr id="validation-rule-<%=i%>">
        <td><%= data[i] %></td>
        <td>
            <input type="hidden" name="validation_rules[]" value="<%=data[i]%>"/>
            <button type="button" data-index="<%=i%>" data-value="<%=data[i]%>"

            class="btn btn-info btn-delete-validation-rule">{{trans("launchcms.buttons.delete")}}</button>
        </td>
    </tr>
    <% } %>
</script>