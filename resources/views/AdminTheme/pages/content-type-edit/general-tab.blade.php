<div class="tab-pane active" id="tab_general">
    <form class="form-horizontal"
          id="content-type-general-field-form">
        <div class="box">
            <div class="box-header with-border">

            </div><!-- /.box-header -->
            <div class="box-body">

                <input type="hidden" name="content_type_alias" value="{{$contentType->alias}}"/>
                <div class="form-group">
                    <label for="txt-content-type-name" class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.name')}}(*)</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control"
                               required="" data-parsley-required="true"
                               value="{{$contentType->name}}"
                               @if($contentType->isBuiltInType()) readonly="readonly" @endif
                               id="txt-content-type-name" placeholder="{{trans('launchcms.content_types_screen.name_placeholder')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="txt-content-type-alias" class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.alias')}}(*)</label>
                    <div class="col-sm-10">
                        <input type="text" name="alias" class="form-control"
                               value="{{$contentType->alias}}"
                               required="" required data-parsley-required="true"
                               @if($contentType->isBuiltInType()) readonly="readonly" @endif
                               id="txt-content-type-alias" placeholder="{{trans('launchcms.content_types_screen.alias_placeholder')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.description')}}</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="description" rows="3" placeholder="{{trans('launchcms.content_types_screen.description_placeholder')}}">{{$contentType->description}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.inherit_from')}}</label>
                    <div class="col-sm-10">
                        @if (!empty($inheritContentType))
                            <input type="text" name="alias" class="form-control" disabled="true"
                                   value="{{$inheritContentType->name}}">
                        @else
                            <input type="text" name="alias" class="form-control" disabled="true"
                                   value="N/A">
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        {{trans('launchcms.content_types_screen.enable_versioning')}}
                    </label>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label>
                                @if($contentType->getEnableVersioning())
                                    <input type="checkbox" name="enable_versioning"
                                           @if($contentType->isBuiltInType()) disabled="disabled" @endif
                                           checked="checked"/>
                                @else
                                    <input type="checkbox"
                                           @if($contentType->isBuiltInType()) disabled="disabled" @endif
                                           name="enable_versioning" />
                                @endif
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        {{trans('launchcms.content_types_screen.enable_localization')}}
                    </label>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label>
                                @if($contentType->getEnableLocalization())
                                    <input type="checkbox" class="checkbox_enable_localization" name="enable_localization"
                                           @if($contentType->isBuiltInType()) disabled="disabled" @endif
                                           checked="checked">
                                @else
                                    <input type="checkbox" class="checkbox_enable_localization"
                                           @if($contentType->isBuiltInType()) disabled="disabled" @endif
                                           name="enable_localization">
                                @endif
                            </label>
                        </div>
                    </div>
                </div>
                <div
                        @if(!$contentType->getEnableLocalization())
                        class="form-group hide"
                        @else
                        class="form-group"
                        @endif
                        id="form-group-primary-locale">
                    <label class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.primary_locale')}}</label>
                    <div class="col-sm-10">
                        <select name="primary_locale" class="form-control select2" style="width: 100%;"
                                @if($contentType->isBuiltInType()) disabled="disabled" @endif>
                            <option value="none">{{trans('launchcms.common_label.please_select')}}</option>
                            @foreach($locales as $localeKey => $localeLabel)
                                @if($localeKey === $contentType->getPrimaryLocale())
                                    <option value="{{$localeKey}}" selected="true">{{$localeLabel}}</option>
                                @else
                                    <option value="{{$localeKey}}">{{$localeLabel}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{trans('launchcms.content_type_edit_screen.attached_workflow')}}</label>
                    <div class="col-sm-10">
                        @include('pages.commons.workflow-selector', ['params' => ['style' => 'width: 100%;', 'name' => 'workflow_id', 'value' => $contentType->getWorkflowID() ]])

                    </div>
                </div>


            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <div class="pull-right">
                    <a class="btn bg-olive btn-save-content-type"><i class="fa fa-edit"></i> {{trans('launchcms.buttons.save')}}</a>
                </div>
            </div>
            <div class="alert alert-error hidden error-panel">
                <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
            </div>

        </div><!-- /.box -->
    </form>
</div><!-- /.tab-pane -->
