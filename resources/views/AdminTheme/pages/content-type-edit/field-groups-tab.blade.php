<div class="tab-pane" id="tab_field_groups">
    <div class="box">
        <div class="box-header with-border">
            <div class="pull-right">

                <a class="btn bg-olive btn-create-field-group" ><i class="fa fa-edit"></i> {{trans('launchcms.buttons.create')}}</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered" id="table-field-group">
                <thead>
                <th>{{trans('launchcms.content_type_edit_screen.field_group_table.name')}}</th>
                <th>{{trans('launchcms.content_type_edit_screen.field_group_table.alias')}}</th>
                <th>{{trans('launchcms.content_type_edit_screen.field_group_table.description')}}</th>
                <th>{{trans('launchcms.content_type_edit_screen.field_group_table.position')}}</th>
                <th style="width: 160px">{{trans('launchcms.common_label.action')}}</th>
                </thead>
                <tbody>

                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-footer clearfix">

        </div>
    </div><!-- /.box -->
</div><!-- /.tab-pane -->


<div class="modal" id="field-group-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- form start -->
            <form class="form-horizontal"
                  id="field-group-form" >
                <input type="hidden" name="content_type_alias" value="{{$contentType->alias}}"/>
                <input type="hidden" name="field_group_alias" value=""/>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{trans('launchcms.content_type_edit_screen.field_group_dialog.title')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="txt-field-group-name" class="col-sm-2 control-label">{{trans('launchcms.content_type_edit_screen.field_group_dialog.name')}}(*)</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control"
                                       required="" data-parsley-required="true"
                                       id="txt-field-group-name" placeholder="{{trans('launchcms.content_type_edit_screen.field_group_dialog.name_placeholder')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txt-field-group-alias" class="col-sm-2 control-label">{{trans('launchcms.content_type_edit_screen.field_group_dialog.alias')}}(*)</label>
                            <div class="col-sm-10">
                                <input type="text" name="alias" class="form-control"
                                       required="" required data-parsley-required="true"
                                       id="txt-field-group-alias" placeholder="{{trans('launchcms.content_type_edit_screen.field_group_dialog.alias_placeholder')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txt-field-group-position" class="col-sm-2 control-label">{{trans('launchcms.content_type_edit_screen.field_group_dialog.position')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="position" class="form-control"
                                       type= "number"
                                       data-parsley-type="integer"
                                       id="txt-field-group-position" placeholder="{{trans('launchcms.content_type_edit_screen.field_group_dialog.position_placeholder')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{trans('launchcms.content_type_edit_screen.field_group_dialog.description')}}</label>
                            <div class="col-sm-10">
                                <textarea name="description" id="txt-field-group-description" class="form-control" rows="3" placeholder="{{trans('launchcms.content_type_edit_screen.field_group_dialog.description_placeholder')}}"></textarea>
                            </div>
                        </div>


                    </div><!-- /.box-body -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                    <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                </div>
                <div class="alert alert-error hidden error-panel">
                    <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="ejs-template" id="tpl-table-field-group-body">
    <% for(var i=0; i<data.length; i++) {%>
    <tr>
        <td><%= data[i].name %></td>
            <td><%= data[i].alias %></td>
            <td>
                <%= data[i].description %>
            </td>
            <td>
                <%= data[i].position %>
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info">{{trans('launchcms.common_label.action')}}</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#" data-alias="<%= data[i].alias %>" class="btn-field-group-edit">{{trans('launchcms.buttons.edit')}}</a></li>
                        <li><a href="#"  data-alias="<%= data[i].alias %>"
                         data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete')}} <%=data[i].name %> ?"
                        class="btn-field-group-delete">{{trans('launchcms.buttons.delete')}}</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <% } %>
</script>