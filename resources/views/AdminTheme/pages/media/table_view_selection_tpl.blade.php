<script type="ejs-template" id="tpl-media-folder-info-body">
<% for (var i=0; i < subfolders.length; i++) {
    var subfolder = subfolders[i];
%>
<tr>
    <td><input type="checkbox" name="selected-media-items" data-path="<%=subfolder.path %>" value="[d]<%=subfolder.path%>"></td>
    <td>
        <a href="#" data-path="<%=subfolder.path%>" class="media-folder">
            <i class="fa fa-folder fa-lg fa-fw"></i>
            <%= subfolder.name %>
        </a>
    </td>
    <td>Folder</td>
    <td>-</td>
    <td>-</td>

</tr>
<% }%>

<% for(var i=0; i < files.length; i++) {
    var file = files[i];
%>
<tr data-url='<%=file.webPath %>'
            data-file-name=<%= file.name %>
            data-file-size=<%=file.size%>
            <% if (file.isImage) { %> data-image='yes'  <%} else {%> data-image='no' <%}%>
            class="media-file selectable-media-file">
    <td><input type="checkbox" name="selected-media-items" data-path="<%=file.fullPath %>" value="[f]<%=file.fullPath %>"></td>
    <td>
        <a href="#" data-url='<%=file.webPath %>'
            data-file-name=<%= file.name %>
            data-file-size=<%=file.size%>
            <% if (file.isImage) { %> data-image='yes'  <%} else {%> data-image='no' <%}%>
            class="media-file">
            <% if (file.isImage) { %>
                <i class="fa fa-file-image-o fa-lg fa-fw"></i>
             <%} else {%>
                <i class="fa fa-file-o fa-lg fa-fw"></i>
            <%}%>
            <%= file.name %>
        </a>
    </td>
    <td><%=file.mimeType%></td>
    <td><%=file.modified%></td>
    <td><%=file.size%></td>

</tr>
<% } %>

</script>

<script type="ejs-template" id="tpl-media-breadcrumb">
<% for (var i=0; i < breadcrumbs.length; i++) {
    var item = breadcrumbs[i];
%>
    <li><a href="#" data-path="<%=item.path%>" class="media-folder"><%=item.name%></a></li>

<% } %>
    <li class="active"><%=currentFolderName %></li>
</script>