<?php
$themeClass = config('launchcms.admin_color_scheme');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LaunchCMS | Administration</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{Theme::url('bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{Theme::url('dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{Theme::url('dist/css/skins/'.$themeClass.'.min.css')}}">
    <link rel="stylesheet" href="{{Theme::url('dist/css/custom.css')}}">
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{Theme::url('plugins/bootstrap-fileinput/css/fileinput.min.css')}}">
    @section('page_css')

    @show

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="layout-top-nav {{$themeClass}}">
<div class="wrapper">

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Media
                <small>management</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container" id="media-panel-container">
                <input type="hidden" value="{{$homePath}}" id="home-path"/>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row box">
                            <section class="box-header">
                                <ol class="breadcrumb"></ol>
                            </section>

                            <div class="box-body table-responsive no-padding">
                                <div class="mailbox-controls">
                                    <button type="button" class="btn btn-default btn-sm new-folder-button"><i class="fa fa-folder"></i></button>
                                    <button type="button" class="btn btn-default btn-sm upload-file-button"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-default btn-sm btn-media-item-delete"><i class="fa fa-trash-o"></i></button>
                                    <!--div class="btn-group">


                                    </div-->
                                    <!-- /.btn-group -->
                                    <button type="button" class="btn btn-default btn-sm refresh-button"><i class="fa fa-refresh"></i></button>


                                </div>

                                <table id="media-table" class="table table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <td><input id="check-all-media-items" type="checkbox"></td>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Size</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row box">
                            <div class="box-body no-padding">

                                <div class="media-item hide" id="selected-media-item">
                                    <input type="hidden" id="ckeditor-callback" value="{{$ckeditorCallBack}}"/>
                                    <span class="media-item-icon has-img">
                                        <img id="preview-image" class="img-responsive" src="{{Theme::url('/dist/img/photo1.png')}}" alt="Attachment"></span>

                                    <div class="media-item-info">
                                        <a href="#" class="media-item-name"><i class="fa fa-camera"></i> <span id="preview-file-name"></span></a>
                                        <span class="media-item-size">
                                          <span id="preview-file-size"></span>

                                        </span>

                                    </div>
                                    <a href="#" class="btn btn-primary btn-xs btn-block" id="btn-select-file">Select</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="upload-file-modal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Upload file</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row well-sm">
                                    <input name="media_file" type="file" class="media-input-file">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- new modal -->
                <div id="new-media-folder" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>New folder</h3>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal"
                                      id="create-media-folder-form">
                                    <label for="new-path">Folder name</label>
                                    <input type="text" class="form-control" name="new_folder"
                                           data-parsley-type="alphanum"
                                           required="" required data-parsley-required="true"/>
                                    <input name="path" type="hidden" value="" />
                                    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                                </form>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                                <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                            </div>
                            <div class="alert alert-error hidden error-panel">
                                <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2016 <a href="#">IntelBees</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">

    </aside><!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<div class="modal" id="confirmation-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{trans('launchcms.confirmation_dialog.title')}}</h4>
            </div>
            <div class="modal-body">
                <p class="message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left btn-cancel" data-dismiss="modal">{{trans('launchcms.confirmation_dialog.cancel_button')}}</button>
                <button type="button" class="btn btn-warning btn-confirm">{{trans('launchcms.confirmation_dialog.confirm_button')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery 2.1.4 -->
<script src="{{Theme::url('plugins/jQuery/jquery-2.1.4.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="{{Theme::url('bootstrap/js/bootstrap.min.js')}}"></script>

<!-- AdminLTE App -->

<script src="{{Theme::url('plugins/storejs/store.min.js')}}"></script>
<script src="{{Theme::url('plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="{{Theme::url('plugins/ejs/ejs_production.js')}}"></script>
<script src="{{Theme::url('dist/js/app.min.js')}}"></script>
<script src="{{Theme::url('dist/js/core.js')}}"></script>
@include('support.localization')

@include('pages.media.table_view_selection_tpl')

<script language="javascript">
    launchcms.adminSlug = '{{config('launchcms.admin_slug')}}';
</script>

<input id="page_token" type="hidden" value="{!! csrf_token() !!}" />
<script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
<script src="{{Theme::url('plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{Theme::url('dist/js/pages/media.js')}}"></script>
<script>
    $(function () {
        launchcms.MediaPage.initialize();
        launchcms.NewFolderDialog.initialize();
    });
</script>
</body>
</html>
