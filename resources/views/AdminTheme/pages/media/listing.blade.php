@extends('layout.no-sidebar-layout')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{Theme::url('plugins/bootstrap-fileinput/css/fileinput.min.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/media.js')}}"></script>
    <script>
        $(function () {
            launchcms.MediaPage.initialize();
            launchcms.NewFolderDialog.initialize();
        });
    </script>
    @endsection


    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Media
            <small>management</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container" id="media-panel-container">
            <input type="hidden" value="{{$homePath}}" id="home-path"/>
            <div class="row box">
                <section class="box-header">
                    <ol class="breadcrumb"></ol>
                </section>

                <div class="box-body table-responsive no-padding">
                    <div class="mailbox-controls">
                        <button type="button" class="btn btn-default btn-sm new-folder-button"><i class="fa fa-folder"></i></button>
                        <button type="button" class="btn btn-default btn-sm upload-file-button"><i class="fa fa-plus"></i></button>
                        <button type="button" class="btn btn-default btn-sm btn-media-item-delete"><i class="fa fa-trash-o"></i></button>
                        <!--div class="btn-group">


                        </div-->
                        <!-- /.btn-group -->
                        <button type="button" class="btn btn-default btn-sm refresh-button"><i class="fa fa-refresh"></i></button>


                    </div>

                    <table id="media-table" class="table table-hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <td><input id="check-all-media-items" type="checkbox"></td>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Size</th>
                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>

            </div>

            <div id="upload-file-modal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>Upload file</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row well-sm">
                                <input name="media_file" type="file" class="media-input-file">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- new modal -->
            <div id="new-media-folder" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>New folder</h3>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal"
                                  id="create-media-folder-form">
                                <label for="new-path">Folder name</label>
                                <input type="text" class="form-control" name="new_folder"
                                       data-parsley-type="alphanum"
                                       required="" required data-parsley-required="true"/>
                                <input name="path" type="hidden" value="" />
                                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                            <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                        </div>
                        <div class="alert alert-error hidden error-panel">
                            <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                        </div>
                    </div>

                </div>
            </div>





        </div>

    </section><!-- /.content -->
    @include('pages.media.table_view_crud_tpl')

@endsection
