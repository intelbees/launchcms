<select class="form-control select2 content-type-selector"
        @if(isset($params['name'])) name="{{$params['name']}}" @endif
        @if(isset($params['style'])) style="{{$params['style']}}" @endif
        @if(isset($params['required'])) required="{{$params['required']}}" @endif
        @if(isset($params['data-parsley-required'])) data-parsley-required="{{$params['data-parsley-required']}}" @endif
        >
    <option value="">None</option>
    @foreach($contentTypes as $contentType)
        <option value="{{$contentType->_id}}">{{$contentType->name}}</option>
    @endforeach
</select>