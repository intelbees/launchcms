<div class="tab-pane field-validation">
    <div class="box-body">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add validation rule</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Validation rule</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control txt-validation-rule"
                                    placeholder="Validation rule following Laravel style">
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-add-validation-rule">{{trans('launchcms.buttons.add')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered validation-rule-table">
            <thead>
            <th>Validation rule</th>
            <th style="width: 160px">{{trans("launchcms.common_label.action")}}</th>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div><!-- /.box-body -->
</div><!-- /.tab-pane -->



