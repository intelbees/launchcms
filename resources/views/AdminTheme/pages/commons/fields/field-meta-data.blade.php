<div class="tab-pane active field-meta-data">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{trans('launchcms.field_dialog.name')}}(*)</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control txt-field-name"
                                   required="" data-parsley-required="true"
                                   placeholder="{{trans('launchcms.field_dialog.name_placeholder')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{trans('launchcms.field_dialog.alias')}}(*)</label>
                        <div class="col-sm-8">
                            <input type="text" name="alias" class="form-control txt-field-alias"
                                   required="" required data-parsley-required="true"
                                   placeholder="{{trans('launchcms.field_dialog.alias_placeholder')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{trans('launchcms.field_dialog.position')}}</label>
                        <div class="col-sm-8">
                            <input type="text" name="position" class="form-control txt-field-position"
                                   type= "number"
                                   data-parsley-type="integer"
                                   placeholder="{{trans('launchcms.field_dialog.position_placeholder')}}">
                        </div>
                    </div>
                    @if(!in_array('field_group', $hideFields) && !in_array('field_group', $attributeToHideInFieldMetaData))
                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{trans('launchcms.field_dialog.field_group')}}</label>
                        <div class="col-sm-8">
                            <select class="form-control field-group-select" name="field_group">
                            </select>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            <div class="col-sm-6">
                <div class="row">
                    @if(!in_array('is_indexed', $hideFields) && !in_array('is_indexed', $attributeToHideInFieldMetaData))
                    <div class="form-group">
                        <label class="col-sm-8 control-label">
                            {{trans('launchcms.field_dialog.is_indexed')}}
                        </label>
                        <div class="col-sm-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_indexed"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!in_array('allow_full_text_search', $hideFields) && !in_array('allow_full_text_search', $attributeToHideInFieldMetaData))
                    <div class="form-group">
                        <label class="col-sm-8 control-label">
                            {{trans('launchcms.field_dialog.allow_full_text_search')}}
                        </label>
                        <div class="col-sm-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="allow_full_text_search"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!in_array('is_unique', $hideFields) && !in_array('is_unique', $attributeToHideInFieldMetaData))
                    <div class="form-group">
                        <label class="col-sm-8 control-label">
                            {{trans('launchcms.field_dialog.is_unique')}}
                        </label>
                        <div class="col-sm-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_unique"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!in_array('is_required', $hideFields) && !in_array('is_required', $attributeToHideInFieldMetaData))
                    <div class="form-group">
                        <label class="col-sm-8 control-label">
                            {{trans('launchcms.field_dialog.is_required')}}
                        </label>
                        <div class="col-sm-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_required"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{trans('launchcms.field_dialog.description')}}</label>
                        <div class="col-sm-8">
                            <textarea name="description" id="txt-field-description" class="form-control" rows="3"
                                      placeholder="{{trans('launchcms.field_dialog.description_placeholder')}}"></textarea>
                        </div>
                    </div>
                </div>

            </div>

        </div>





    </div><!-- /.box-body -->
</div><!-- /.tab-pane -->