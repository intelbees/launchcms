<div class="modal" id="boolean-field-modal">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <!-- form start -->
            <form class="form-horizontal"
                  id="boolean-field-form" >
                <input type="hidden" name="structure_type_alias" value="{{$structure->alias}}"/>
                <input type="hidden" name="data_type" value="boolean"/>
                <input type="hidden" name="field_alias" value=""/>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{trans('launchcms.field_dialog.boolean_field_title')}}</h4>
                </div>
                <div class="modal-body">

                        <div class="row">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs pull-right">
                                    <li class="active"><a href="#" data-target="#boolean-field-form .field-meta-data"  data-toggle="tab">
                                            {{trans('launchcms.field_dialog.field_meta_data_tab')}}</a></li>
                                </ul>
                                <div class="tab-content">
                                    @include('pages.commons.fields.field-meta-data', ['hideFields' => ['is_unique']])
                                </div><!-- /.tab-content -->

                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.row (main row) -->

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                    <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                </div>
                <div class="alert alert-error hidden error-panel">
                    <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->