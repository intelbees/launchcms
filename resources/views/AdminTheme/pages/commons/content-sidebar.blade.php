<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{trans('launchcms.search')}}...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{trans('launchcms.main_navigation')}}</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Content</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach($contentTypes as $contentTypeNav)
                        @if(!$contentTypeNav->isBuiltinType())
                        <li @if($contentTypeNav->_id === $activeMenuItem) class="active" @endif>
                            <a href="{{route('cms_content_listing', ['content_type_alias' => $contentTypeNav->getAlias() ])}}">
                                {{ $contentTypeNav->getName() }}
                            </a>
                        </li>
                        @endif
                    @endforeach

                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
