<select class="form-control select2 workflow-selector"
        @if(isset($params['value']) && $params['value'] != null) value="{{$params['value']}}" @endif
        @if(isset($params['name'])) name="{{$params['name']}}" @endif
        @if(isset($params['style'])) style="{{$params['style']}}" @endif
        @if(isset($params['required'])) required="{{$params['required']}}" @endif
        @if(isset($params['data-parsley-required'])) data-parsley-required="{{$params['data-parsley-required']}}" @endif
        >
    <option value="" @if(!isset($params['value']) || $params['value'] == null) selected="selected"  @endif>None</option>
    @foreach($workflows as $workflow)
        <option value="{{$workflow->_id}}"
                @if(isset($params['value']) && $params['value'] === (string) $workflow->_id) selected="selected" @endif>{{$workflow->name}}</option>
    @endforeach
</select>