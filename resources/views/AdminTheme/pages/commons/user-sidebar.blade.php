<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Users & Organization</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if($user->hasAccess('cms_access_user'))
                    <li @if($activeMenuItem==='users') class="active" @endif>
                        <a href="{{route('cms_user_listing')}}">
                            Users
                        </a>
                    </li>
                    @endif

                    @if($user->hasAccess('cms_access_role'))
                    <li @if($activeMenuItem==='roles') class="active" @endif>
                        <a href="{{route('cms_role_listing')}}">
                            Roles
                        </a>
                    </li>
                    @endif
                    <li @if($activeMenuItem==='relationship') class="active" @endif>
                        <a href="{{route('cms_relationship_definition_listing')}}">
                            Relationship
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>