<div class="modal" id="field-modal">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <!-- form start -->
            <form class="form-horizontal">

                <input type="hidden" name="structure_type_alias" value="{{$structure->alias}}"/>
                <input type="hidden" name="field_group_alias" value=""/>
                <input type="hidden" name="data_type" value="string"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new field</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body field-dialog-body">
                        @for($index=0; $index < $rowToShowFields; $index++)
                            <div class="row">
                                @for($fieldIndex=($index * 3); $fieldIndex < count($uiFields) && $fieldIndex <=  ($index * 3) + 2; $fieldIndex++)
                                    <div class="col-sm-4">
                                        <div class="info-box field-select-box" data-type="{{$uiFields[$fieldIndex]['data_type']}}">

                                            <!-- Apply any bg-* class to to the icon to color it -->
                                            <a href="#">
                                                <span class="info-box-icon bg-aqua"><i class="fa {{$uiFields[$fieldIndex]['icon_class']}}">{{$uiFields[$fieldIndex]['icon_text']}}</i></span>
                                            </a>
                                            <div class="info-box-content">
                                                <span class="info-box-text"><strong>{{ trans($uiFields[$fieldIndex]['field_name_key']) }}</strong></span>
                                                <span>{{ trans($uiFields[$fieldIndex]['field_desc_key'])}}</span>
                                            </div><!-- /.info-box-content -->

                                        </div><!-- /.info-box -->
                                    </div>
                                @endfor
                            </div>
                        @endfor
                    </div><!-- /.box-body -->
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
