@extends('layout.layout')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{Theme::url('plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">

@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/momentjs/moment.js')}}"></script>
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{Theme::url('plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script src="{{Theme::url('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
    <script src="{{Theme::url('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
    <script src="{{Theme::url('plugins/input-mask/jquery.inputmask.numeric.extensions.js')}}"></script>


    <script src="{{Theme::url('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key={{config('launchcms.google_map_api_key')}}"></script>

    <script src="{{Theme::url('plugins/geo-complete/geo-complete.min.js')}}"></script>

    <script src="{{Theme::url('dist/js/field-editors.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/edit-content-page.js')}}"></script>


@endsection


    @section('content')
        @section('content_header')
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <span id="content-name-on-title">{{$content->name}}</span>
                    <small>{{$contentType->getName()}}</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">{{trans('launchcms.content')}}</li>
                </ol>
            </section>
        @show

    <!-- Main content -->
    <section class="content" id="content-input-panel">
        <input id="content-id" type="hidden" value="{{$content->id}}" />
        <input id="content-type-alias" type="hidden"  value="{{$contentType->getAlias()}}" />
        <!-- Main row -->
        <div class="row">

            <div class="box box-primary">

                <div class="box-body">
                    <form class="form-horizontal"
                          id="content-form">
                        <input type="hidden" name="id" value="{{ (string) $content->id}}"/>
                        <input type="hidden" name="content_type_alias" value="{{$contentType->getAlias()}}" />
                        <input type="hidden" name="workflow_status" value="{{$content->getWorkflowStatus()}}" />
                        <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                        <div class="nav-tabs-custom">
                            @section('main_tabs')
                            <ul class="nav nav-tabs pull-right">
                                @foreach($fieldGroupTabs as $fieldGroupTab)
                                    <li><a href="javascript:void(0)" data-target="#tab-{{$fieldGroupTab['alias']}}" data-toggle="tab">{{$fieldGroupTab['name']}}</a></li>
                                @endforeach

                                <li class="active"><a href="javascript:void(0)"  data-target="#tab_meta" data-toggle="tab">{{trans('launchcms.content_edit_screen.meta_field_group_name')}}</a></li>


                            </ul>
                            <div class="tab-content">
                                @include('pages.content-edit.meta-tab')

                                @foreach($fieldGroupTabs as $fieldGroupTab)
                                    @include('pages.content-edit.field-group-tab', ['fieldGroupTab' => $fieldGroupTab])
                                @endforeach

                            </div><!-- /.tab-content -->
                            @show
                        </div><!-- nav-tabs-custom -->
                    </form>

                    <div class="alert alert-error hidden error-panel clearfix" role="alert">
                        <div class="hidden" id="server-validation-message">
                            <strong>{{trans('launchcms.common_label.error')}}:</strong>
                            <span class="error-message"></span>
                        </div>
                        <div class="hidden" id="global-validation-message">
                            <strong>{{trans('launchcms.common_label.error')}}:</strong>
                            <span class="validation-error-message">
                                {{trans('launchcms.messages.global_validation_error')}}
                            </span>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <div class="pull-right">
                        @if(isset($nextTransitions) && !empty($nextTransitions))
                        <div class="btn-group">
                            <button type="button" class="btn btn-info">Workflow action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                @foreach($nextTransitions as $transition)
                                <li><a href="#"  data-to-alias="{{$transition['to']}}"
                                       data-transition-name="{{$transition['name']}}"
                                       data-destination-system-state="{{$transition['destination_system_state']}}"
                                       class="btn-state-change">{{$transition['name']}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <a class="btn bg-olive btn-save-content" ><i class="fa fa-edit"></i> {{trans('launchcms.buttons.save')}}</a>

                    </div>

                </div>
            </div><!-- /.box -->
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->




    @include('pages.content-edit.data-type-dialogs')

    <div class="modal" id="workflow-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- form start -->
                <form class="form-horizontal"
                      id="workflow-form">
                    <input type="hidden" name="content_type_alias" value="{{$contentType->getAlias()}}"/>
                    <input type="hidden" name="content_id" value="{{ (string) $content->id}}"/>
                    <input type="hidden" name="next_state" value=""/>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{trans('launchcms.content_edit_screen.workflow_dialog.title')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{trans('launchcms.content_edit_screen.workflow_dialog.note')}}</label>
                                <div class="col-sm-9">
                                    <textarea type="text" name="workflow_data[note]" class="form-control"
                                            placeholder="{{trans('launchcms.content_edit_screen.workflow_dialog.note_placeholder')}}">@if(!empty($workflowNote)){{$workflowNote}}@endif</textarea>

                                </div>
                            </div>
                            <div class="form-group schedule_field hide">
                                <label class="col-sm-3 control-label">{{trans('launchcms.content_edit_screen.workflow_dialog.schedule_time')}}</label>
                                <div class="col-sm-9">
                                    <div class="input-group datetime">
                                        <input type="text" class="form-control pull-right field-datetime-picker"
                                               name="workflow_data[scheduled_time]"
                                               @if(!empty($scheduledTime)) value="{{$scheduledTime}}" @endif
                                        />
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                        </div><!-- /.box-body -->
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                        <button type="button" class="btn btn-primary btn-save-workflow-state">{{trans('launchcms.buttons.change')}}</button>
                    </div>
                    <div class="alert alert-error hidden error-panel">
                        <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
