<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{trans('launchcms.main_navigation')}}</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>{{trans('launchcms.structure')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if($user->hasAccess('cms_access_data_type'))
                    <li @if($activeMenuItem=='data_type') class="active" @endif>
                        <a href="{{route('cms_data_type_listing_page')}}">

                            {{trans('launchcms.data_types')}}
                        </a>
                    </li>
                    @endif
                    @if($user->hasAccess('cms_access_content_type'))
                    <li @if($activeMenuItem=='content_type') class="active" @endif>
                        <a href="{{route('cms_content_type_listing_page')}}">

                            {{trans('launchcms.content_types')}}
                        </a>
                    </li>
                    @endif
                    <li @if($activeMenuItem=='workflow') class="active" @endif>
                        <a href="{{route('cms_workflow_definition_listing')}}">
                            Workflows
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>