@extends('pages.commons.content-type-based-edit-page')


@section('sidebar')
    @include('pages.commons.user-sidebar', ['activeMenuItem' => 'roles'])
@overwrite

@section('content_header')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <span id="content-name-on-title">{{$content->name}}</span>
        <small>{{$contentType->getName()}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{trans('launchcms.users')}}</li>
    </ol>
</section>
@overwrite
@section('main_tabs')
    <ul class="nav nav-tabs pull-right">
        @foreach($fieldGroupTabs as $fieldGroupTab)
            <li><a href="#tab-{{$fieldGroupTab['alias']}}" data-toggle="tab">{{$fieldGroupTab['name']}}</a></li>
        @endforeach
            <li><a href="#tab_permission" data-toggle="tab">Permissions</a></li>
            <li class="active"><a href="#tab_meta" data-toggle="tab">Role information</a></li>


    </ul>
    <div class="tab-content">
        @include('pages.role-edit.meta-tab')
        @include('pages.role-edit.permission-tab')

        @foreach($fieldGroupTabs as $fieldGroupTab)
            @include('pages.content-edit.field-group-tab', ['fieldGroupTab' => $fieldGroupTab])
        @endforeach

    </div><!-- /.tab-content -->
@overwrite