<div class="tab-pane" id="tab_permission">
    <div class="field-panel">
        <h3>System permissions</h3>
        <div class="form-group">
            <label class="col-sm-3 control-label">Permissions</label>
            <div class="col-sm-9">
                <select style="width: 500px" class="form-control select2" multiple="multiple"
                        name="permissions[]">
                    @foreach($permissions as $key => $value)
                        <option
                                @if(in_array($key, $selectedPermissions)) selected="selected" @endif
                                value="{{$value['alias']}}">{{$value['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr/>
        <h3>Permissions on content types</h3>
        @if(empty($contentTypes))
            <p>Sorry, there is no content type in the system to assign permission</p>
        @endif
        @foreach($contentTypes as $contentType)
            <div class="form-group">
                <label class="col-sm-3 control-label">On [{{$contentType->name}}], can:</label>
                <div class="col-sm-9">
                    <select style="width: 500px" class="form-control select2" multiple="multiple"
                            name="content_permission_matrix[{{$contentType->_id}}][]">
                        @foreach($allContentPermissions as $key => $value)
                            <option
                            @if($role->hasContentPermission($key, (string) $contentType->_id)) selected="selected" @endif
                            value="{{$value['alias']}}">{{$value['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endforeach
    </div>

</div><!-- /.tab-pane -->