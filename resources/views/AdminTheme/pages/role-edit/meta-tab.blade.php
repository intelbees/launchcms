<div class="tab-pane active" id="tab_meta">
    <div class="field-panel">
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.name')}}(*)</label>
            <div class="col-sm-10">
                <input type="text" name="name"
                       class="form-control field-element"
                       required="" data-parsley-required="true"
                       value="{{$content->name}}"
                       placeholder="{{trans('launchcms.content_edit_screen.meta_fields.name_placeholder')}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Slug(*)</label>
            <div class="col-sm-10">
                <input type="text" name="slug"
                       class="form-control field-element"
                       required="" data-parsley-required="true"
                       value="{{$content->slug}}"
                       placeholder="Slug for the role">
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.created_at')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>{{$content->createdTime->diffForHumans()}}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.updated_at')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>{{$content->lastUpdatedTime->diffForHumans()}}</span>
            </div>
        </div>


    </div>
</div><!-- /.tab-pane -->