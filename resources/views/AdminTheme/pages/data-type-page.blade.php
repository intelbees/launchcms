@extends('layout.layout')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/data-type-page.js')}}"></script>


@endsection

    @section('sidebar')
        @include('pages.commons.structure-sidebar', ['activeMenuItem' => 'data_type'])
    @endsection
    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{trans('launchcms.structure')}}
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{trans('launchcms.structure')}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('launchcms.data_types_screen.screen_title')}}</h3>
                    <div class="alert " role="alert">
                        <p>
                            {!! trans('launchcms.data_types_screen.screen_guideline') !!}
                        </p>
                    </div>

                    <div class="pull-right">

                        <a class="btn bg-olive" data-toggle="modal" data-target="#create-data-type-modal" ><i class="fa fa-edit"></i> {{trans('launchcms.buttons.create')}}</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p>
                        {!! trans('launchcms.messages.built_in_type_guidance') !!}
                    </p>
                    <table class="table table-bordered" id="table-data-type">
                        <thead>
                            <th>{{trans('launchcms.data_types_screen.name')}}</th>
                            <th>{{trans('launchcms.data_types_screen.alias')}}</th>
                            <th>{{trans('launchcms.data_types_screen.description')}}</th>
                            <th style="width: 160px">{{trans("launchcms.common_label.action")}}</th>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>

                </div><!-- /.box-body -->
                <div class="box-footer clearfix">

                </div>
            </div><!-- /.box -->
        </div><!-- /.row (main row) -->




    </section><!-- /.content -->

    <div class="modal" id="create-data-type-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- form start -->
                <form class="form-horizontal"
                      id="create-data-type-form">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{trans('launchcms.data_types_screen.create_dialog_title')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="txt-data-type-name" class="col-sm-2 control-label">{{trans('launchcms.data_types_screen.name')}}(*)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control"
                                           required="" data-parsley-required="true"
                                           id="txt-data-type-name" placeholder="{{trans('launchcms.data_types_screen.name_placeholder')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txt-data-type-alias" class="col-sm-2 control-label">{{trans('launchcms.data_types_screen.alias')}}(*)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="alias" class="form-control"
                                           required="" required data-parsley-required="true"
                                           id="txt-data-type-alias" placeholder="{{trans('launchcms.data_types_screen.alias_placeholder')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{trans('launchcms.data_types_screen.description')}}</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" name="description" placeholder="{{trans('launchcms.data_types_screen.description_placeholder')}}"></textarea>
                                </div>
                            </div>
                            
                            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                        </div><!-- /.box-body -->
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                        <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                    </div>
                    <div class="alert alert-error hidden error-panel">
                        <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script type="ejs-template" id="tpl-data-type-body">
        <% for(var i=0; i<data.length; i++) {%>
        <tr>
            <td>
                <%= data[i].name %>
                <% if(data[i].builtin) { %>
                    <i class="fa fa-star"></i>
                <% } %>
            </td>
            <td><%= data[i].alias %></td>
            <td>
                <%= data[i].description %>
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<%= data[i].edit_url %>" class="btn-data-type-edit">Edit</a></li>
                        <% if(!data[i].builtin) { %>
                        <li><a href="#"  data-alias="<%= data[i].alias %>"
                            data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete')}} <%=data[i].name %> ?"
                            class="btn-data-type-delete">Delete</a></li>
                        <% } %>
                    </ul>
                </div>
            </td>
        </tr>
        <% } %>
    </script>


@endsection
