<div class="tab-pane" id="tab_settings">
    <form class="form-horizontal"
          id="data-type-settings-form">
        <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
        <input name="data_type_alias" type="hidden"  value="{{$dataType->alias}}"/>
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-4 control-label">{{trans('launchcms.data_types_screen.table_display_fields')}}</label>
                    <div class="col-sm-8">
                        <select class="form-control multi-field-selector"
                                multiple="multiple"
                                style="width: 400px"
                                name="table_display_fields[]">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-8">
                        <p>{{trans('launchcms.data_types_screen.note_for_default_table_display_fields')}}</p>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{trans('launchcms.data_types_screen.editable_fields')}}</label>
                    <div class="col-sm-8">
                        <select class="form-control multi-field-selector"
                                multiple="multiple"
                                style="width: 400px"
                                name="editable_fields[]">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-8">
                        <p>{{trans('launchcms.data_types_screen.note_for_default_editable_fields')}}</p>
                    </div>

                </div>
            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <div class="pull-right">
                    <a class="btn bg-olive btn-save-data-type-settings"><i class="fa fa-edit"></i> {{trans('launchcms.buttons.save')}}</a>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.tab-pane -->
