<div class="tab-pane active" id="tab_general">
    <form class="form-horizontal"
          id="data-type-general-field-form">
        <div class="box">
            <div class="box-header with-border">

            </div><!-- /.box-header -->
            <div class="box-body">

                <input type="hidden" name="data_type_alias" value="{{$dataType->alias}}"/>
                <div class="form-group">
                    <label for="txt-data-type-name" class="col-sm-2 control-label">{{trans('launchcms.data_types_screen.name')}}(*)</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control"
                               required="" data-parsley-required="true"
                               value="{{$dataType->name}}"
                               @if($dataType->isBuiltInType()) readonly="readonly" @endif
                               id="txt-data-type-name" placeholder="{{trans('launchcms.data_types_screen.name_placeholder')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="txt-data-type-alias" class="col-sm-2 control-label">{{trans('launchcms.data_types_screen.alias')}}(*)</label>
                    <div class="col-sm-10">
                        <input type="text" name="alias" class="form-control"
                               value="{{$dataType->alias}}"
                               @if($dataType->isBuiltInType()) readonly="readonly" @endif
                               required="" required data-parsley-required="true"
                               id="txt-data-type-alias" placeholder="{{trans('launchcms.data_types_screen.alias_placeholder')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{trans('launchcms.data_types_screen.description')}}</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="description" rows="3" placeholder="{{trans('launchcms.data_types_screen.description_placeholder')}}">{{$dataType->description}}</textarea>
                    </div>
                </div>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <div class="pull-right">
                    <a class="btn bg-olive btn-save-data-type"><i class="fa fa-edit"></i> {{trans('launchcms.buttons.save')}}</a>
                </div>
            </div>
            <div class="alert alert-error hidden error-panel">
                <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
            </div>

        </div><!-- /.box -->
    </form>
</div><!-- /.tab-pane -->
