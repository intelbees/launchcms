<div class="tab-pane active" id="tab_meta">
    <div class="field-panel">
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.name')}}(*)</label>
            <div class="col-sm-10">
                <input type="text" name="name"
                       class="form-control field-element"
                       required="" data-parsley-required="true"
                       value="{{$content->name}}"
                       placeholder="{{trans('launchcms.content_edit_screen.meta_fields.name_placeholder')}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Email(*)</label>
            <div class="col-sm-10">
                <input type="text" name="email"
                       class="form-control field-element"
                       required="" data-parsley-required="true"
                       type="email" data-parsley-type="email"
                       value="{{$content->email}}"
                       placeholder="Email of the user">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Home</label>
            <div class="col-sm-10">
                <input type="text" name="home"
                       class="form-control field-element"
                       value="{{$content->home}}"
                       placeholder="Home path of the user">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="password" name="password"
                       class="form-control field-element"
                       placeholder="Leave empty if you want to keep the current password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.created_at')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>{{$content->createdTime->diffForHumans()}}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.updated_at')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>{{$content->lastUpdatedTime->diffForHumans()}}</span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Activated</label>
            <div class="col-sm-10">
                <select style="width: 300px" class="form-control"
                        name="activated">
                        <option value="activated" @if($activated) selected="selected" @endif>Activated</option>
                        <option value="inactivated" @if(!$activated) selected="selected" @endif>Inactivated</option>
                </select>

            </div>
        </div>
    </div>
</div><!-- /.tab-pane -->