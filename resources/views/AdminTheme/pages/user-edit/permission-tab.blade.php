<div class="tab-pane" id="tab_permission">
    <div class="field-panel">
        <div class="form-group">
            <label class="col-sm-2 control-label">Permissions</label>
            <div class="col-sm-10">
                <select style="width: 400px" class="form-control select2" multiple="multiple"
                        name="permissions[]">
                    @foreach($permissions as $key => $value)
                        <option
                                @if(in_array($key, $selectedPermissions)) selected="selected" @endif
                                value="{{$value['alias']}}">{{$value['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Roles</label>
            <div class="col-sm-10">
                <select style="width: 400px" class="form-control select2" multiple="multiple"
                        name="roles[]">
                    @foreach($roles as $role)
                        <option @if(in_array($role->_id, $selectedRoles)) selected="selected" @endif
                                value="{{(string) $role->_id}}">{{$role->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Backend access</label>
            <div class="col-sm-10">
                <select style="width: 300px" class="form-control"
                        name="admin_access">
                    <option value="yes" @if($adminAccess) selected="selected" @endif>Yes</option>
                    <option value="no" @if(!$adminAccess) selected="selected" @endif>No</option>
                </select>

            </div>
        </div>


    </div>
</div><!-- /.tab-pane -->