@extends('pages.content-type-based-listing-page')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{Theme::url('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/content-listing-page.js')}}"></script>
@endsection

@section('sidebar')
    @include('pages.commons.content-sidebar', ['activeMenuItem' => $contentType->_id]);
@overwrite


