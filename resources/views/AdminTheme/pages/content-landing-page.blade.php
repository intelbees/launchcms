@extends('layout.layout')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>



@endsection

    @section('sidebar')
        @include('pages.commons.content-sidebar', ['activeMenuItem' => '']);
    @endsection
    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{trans('launchcms.content')}}
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{trans('launchcms.content')}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('launchcms.content_landing_screen.screen_title')}}</h3>
                    <div class="alert " role="alert">
                        <p>
                            {{trans('launchcms.content_landing_screen.screen_guideline')}}
                        </p>
                    </div>

                </div><!-- /.box-header -->
                <div class="box-body">


                </div><!-- /.box-body -->
                <div class="box-footer clearfix">

                </div>
            </div><!-- /.box -->
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->

@endsection
