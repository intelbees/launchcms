@extends('layout.layout')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/content-type-page.js')}}"></script>


@endsection

    @section('sidebar')
        @include('pages.commons.structure-sidebar', ['activeMenuItem' => 'content_type'])
    @endsection
    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{trans('launchcms.structure')}}
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{trans('launchcms.structure')}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('launchcms.content_types_screen.screen_title')}}</h3>
                    <div class="alert " role="alert">
                        <p>
                            {{trans('launchcms.content_types_screen.screen_guideline')}}
                        </p>
                    </div>

                    <div class="pull-right">
                        @if($user->hasAccess('cms_create_content_type'))
                            <a class="btn bg-olive" data-toggle="modal" data-target="#create-content-type-modal" ><i class="fa fa-edit"></i> {{trans('launchcms.buttons.create')}}</a>
                        @endif
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>
                            {!! trans('launchcms.messages.built_in_type_guidance') !!}
                        </p>
                        <table class="table table-bordered" id="table-content-type">
                            <thead>
                                <th>{{trans('launchcms.content_types_screen.name')}}</th>
                                <th>{{trans('launchcms.content_types_screen.alias')}}</th>
                                <th>{{trans('launchcms.content_types_screen.description')}}</th>
                                <th style="width: 160px">{{trans("launchcms.common_label.action")}}</th>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>

                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">

                    </div>
                </div><!-- /.box -->
            </div><!-- /.row (main row) -->




        </section><!-- /.content -->

        <div class="modal" id="create-content-type-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- form start -->
                    <form class="form-horizontal"
                          id="create-content-type-form">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">{{trans('launchcms.content_types_screen.create_dialog_title')}}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="txt-content-type-name" class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.name')}}(*)</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control"
                                               required="" data-parsley-required="true"
                                               id="txt-content-type-name" placeholder="{{trans('launchcms.content_types_screen.name_placeholder')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txt-content-type-alias" class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.alias')}}(*)</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="alias" class="form-control"
                                               required="" required data-parsley-required="true"
                                               id="txt-content-type-alias" placeholder="{{trans('launchcms.content_types_screen.alias_placeholder')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.description')}}</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="description" placeholder="{{trans('launchcms.content_types_screen.description_placeholder')}}"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{trans('launchcms.content_types_screen.inherit_from')}}</label>
                                    <div class="col-sm-10">
                                        @include('pages.commons.content-type-selector', ['params' => ['style' => 'width: 100%;', 'name' => 'inherit']])

                                    </div>
                                </div>
                                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                            </div><!-- /.box-body -->
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                            <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                        </div>
                        <div class="alert alert-error hidden error-panel">
                            <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="ejs-template" id="tpl-content-type-body">
            <% for(var i=0; i<data.length; i++) {%>
            <tr>
                <td>
                    <%= data[i].name %>
                    <% if(data[i].builtin) { %>
                        <i class="fa fa-star"></i>
                    <% } %>
                </td>
                <td><%= data[i].alias %></td>
                <td>
                    <%= data[i].description %>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            @if($user->hasAccess('cms_create_content_type'))
                            <li><a href="<%= data[i].edit_url %>" class="btn-content-type-edit">Edit</a></li>
                            @endif
                            @if($user->hasAccess('cms_delete_content_type'))
                            <% if(!data[i].builtin) { %>
                            <li><a href="#"  data-alias="<%= data[i].alias %>"
                             data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete')}} <%=data[i].name %> ?"
                            class="btn-content-type-delete">Delete</a></li>
                            <% } %>
                            @endif
                        </ul>
                    </div>
                </td>
            </tr>
            <% } %>
        </script>


    @endsection
