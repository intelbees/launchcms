@extends('layout.no-sidebar-layout')
@section('page_css')
        <!-- iCheck -->
<link rel="stylesheet" href="{{Theme::url('plugins/iCheck/flat/blue.css')}}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{Theme::url('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{Theme::url('plugins/datepicker/datepicker3.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{Theme::url('plugins/daterangepicker/daterangepicker-bs3.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{Theme::url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@endsection

@section('page_script')
        <!-- Sparkline -->
<script src="{{Theme::url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{Theme::url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{Theme::url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{Theme::url('plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{Theme::url('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{Theme::url('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{Theme::url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{Theme::url('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{Theme::url('plugins/fastclick/fastclick.min.js')}}"></script>
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('launchcms.dashboard')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{trans('launchcms.dashboard')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

        @foreach ($statWidgets as $widget)
            <div class="col-md-4 col-sm-6 col-xs-12">
                {!! $widget->render() !!}
            </div><!-- ./col -->
        @endforeach

    </div><!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            @foreach ($mainWidgets as $widget)
                {!! $widget->render() !!}
            @endforeach

        </section><!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">
            @foreach ($sidebarWidgets as $widget)
                {!! $widget->render() !!}
            @endforeach
        </section><!-- right col -->
    </div><!-- /.row (main row) -->
</section><!-- /.content -->

@endsection
