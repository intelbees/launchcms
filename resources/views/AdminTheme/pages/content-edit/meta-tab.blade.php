<div class="tab-pane active" id="tab_meta">
    <div class="field-panel">
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.name')}}(*)</label>
            <div class="col-sm-10">
                <input type="text" name="name"
                       id="content-name"
                       class="form-control field-element"
                       required="" data-parsley-required="true"
                       value="{{$content->name}}"
                       placeholder="{{trans('launchcms.content_edit_screen.meta_fields.name_placeholder')}}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.status')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>
                    @if(isset($stateButtonWidgetHtml))
                    {!! $stateButtonWidgetHtml !!}
                    @endif
                </span>
            </div>
        </div>
        @if(!empty($currentState) && $currentState['mapped_system_state'] === 'scheduled')
            <div class="form-group">
                <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.scheduled_time')}}</label>
                <div class="col-sm-10">
                    <span class='form-control form-readonly-span'>
                        @if(!empty($scheduledTime))
                            {{$scheduledTime}}
                        @endif
                    </span>
                </div>
            </div>
        @endif
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.created_at')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>{{$content->createdTime->diffForHumans()}}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{trans('launchcms.content_edit_screen.meta_fields.updated_at')}}</label>
            <div class="col-sm-10">
                <span class='form-control form-readonly-span'>{{$content->lastUpdatedTime->diffForHumans()}}</span>
            </div>
        </div>
    </div>
</div><!-- /.tab-pane -->