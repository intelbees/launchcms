@inject('editorFactory', 'App\FieldEditors\EditorFactory')
<div class="tab-pane" id="tab-{{$fieldGroupTab['alias']}}">
    <div class="field-panel">
        @if(!empty($fieldGroupTab['description']))
            <p><small>{{$fieldGroupTab['description']}}</small></p>
        @endif
        @foreach($fieldGroupTab['fields'] as $field)
            @if ($editorFactory->getEditor($field) != null)
            {!! $editorFactory->getEditor($field)->render($content) !!}
            @endif
        @endforeach
    </div><!-- /.box-body -->
</div><!-- /.tab-pane -->