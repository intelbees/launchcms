@extends('pages.commons.content-type-based-edit-page')


@section('sidebar')
    @include('pages.commons.content-sidebar', ['activeMenuItem' => $contentType->_id]);
@overwrite
