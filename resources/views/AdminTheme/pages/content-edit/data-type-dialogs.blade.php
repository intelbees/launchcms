@inject('editorFactory', 'App\FieldEditors\EditorFactory')
@foreach($dataTypesToPrepareForDialogs as $dataType)
    <div class="modal" id="data-type-{{$dataType->getAlias()}}-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- form start -->
                <form class="form-horizontal"
                      id="data-type-{{$dataType->getAlias()}}-form">
                    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                    <input name="content_id" type="hidden" value="{{$content->id}}" />
                    <input name="content_type_alias" type="hidden" value="{{$contentType->getAlias()}}" />
                    <input name="field_alias" type="hidden" value="" />
                    <input name="record_id" type="hidden" value="" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{$dataType->getName()}}</h4>
                    </div>
                    <div class="modal-body">
                        @foreach($dataType->getAllFields() as $field)

                            @if ($editorFactory->getEditorInDialogMode($field) != null)
                                {!! $editorFactory->getEditor($field)->render(null) !!}
                            @endif
                        @endforeach
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                        <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                    </div>
                    <div class="alert alert-error hidden error-panel">
                        <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endforeach