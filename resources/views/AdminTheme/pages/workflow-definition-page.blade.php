@extends('layout.layout')

@section('page_css')
    <link rel="stylesheet" href="{{Theme::url('plugins/select2/select2.min.css')}}">
@endsection

@section('page_script')
    <script src="{{Theme::url('plugins/parsley/parsley.min.js')}}"></script>
    <script src="{{Theme::url('plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{Theme::url('dist/js/pages/workflow-definition-page.js')}}"></script>


@endsection

    @section('sidebar')
        @include('pages.commons.structure-sidebar', ['activeMenuItem' => 'workflow'])
    @endsection
    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{trans('launchcms.workflow')}}
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{trans('launchcms.workflow')}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('launchcms.workflow_definition_screen.screen_title')}}</h3>
                    <div class="alert " role="alert">
                        <p>
                            {{trans('launchcms.workflow_definition_screen.screen_guideline')}}
                        </p>
                    </div>

                    <div class="pull-right">
                        @if($user->hasAccess('cms_create_workflow_definition'))
                            <a class="btn bg-olive btn-create-workflow" ><i class="fa fa-edit"></i> {{trans('launchcms.buttons.create')}}</a>
                        @endif
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered" id="table-workflow">
                            <thead>
                                <th>{{trans('launchcms.workflow_definition_screen.name')}}</th>
                                <th>{{trans('launchcms.workflow_definition_screen.alias')}}</th>
                                <th style="width: 160px">{{trans("launchcms.common_label.action")}}</th>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>

                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">

                    </div>
                </div><!-- /.box -->
            </div><!-- /.row (main row) -->




        </section><!-- /.content -->

        <div class="modal" id="create-workflow-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- form start -->
                    <form class="form-horizontal"
                          id="create-workflow-form">
                        <input type="hidden" name="workflow_alias">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">{{trans('launchcms.workflow_definition_screen.create_dialog_title')}}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{trans('launchcms.workflow_definition_screen.name')}}(*)</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control"
                                               required="" data-parsley-required="true"
                                               placeholder="{{trans('launchcms.workflow_definition_screen.name_placeholder')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ trans('launchcms.workflow_definition_screen.alias') }}(*)</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="alias" class="form-control"
                                               required="" required data-parsley-required="true"
                                               placeholder="{{ trans('launchcms.workflow_definition_screen.alias_placeholder') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{trans('launchcms.workflow_definition_screen.json')}}</label>
                                    <div class="col-sm-10">
                                        <textarea name="json" required="" required data-parsley-required="true" class="form-control field-element text-area" rows="6"></textarea>
                                    </div>
                                </div>
                                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                            </div><!-- /.box-body -->
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('launchcms.buttons.close')}}</button>
                            <button type="button" class="btn btn-primary btn-save">{{trans('launchcms.buttons.save')}}</button>
                        </div>
                        <div class="alert alert-error hidden error-panel">
                            <div class="general-error">
                                <strong>{{trans('launchcms.common_label.error')}}:</strong> <span class="error-message"></span>
                            </div>
                            <div>
                                <span class="detail-error-message"></span>
                            </div>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="ejs-template" id="tpl-workflow-body">
            <% for(var i=0; i<data.length; i++) {%>
            <tr>
                <td>
                    <%= data[i].name %>
                </td>
                <td><%= data[i].alias %></td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info">{{trans("launchcms.common_label.action")}}</button>
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            @if($user->hasAccess('cms_create_workflow_definition'))
                            <li><a href="#" data-alias="<%= data[i].alias %>" data-id="<%= data[i].id %>" class="btn-workflow-edit">Edit</a></li>
                            @endif
                            @if($user->hasAccess('cms_delete_workflow_definition'))
                            <li><a href="#"  data-alias="<%= data[i].alias %>" data-id="<%= data[i].id %>"
                             data-confirm-message="{{trans('launchcms.messages.are_you_sure_to_delete')}} <%=data[i].name %> ?"
                            class="btn-workflow-delete">Delete</a></li>
                            @endif
                        </ul>
                    </div>
                </td>
            </tr>
            <% } %>
        </script>


    @endsection
