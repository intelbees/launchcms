launchcms.CreateContentTypeDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            form[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content-type.created');
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    };
    var onSaveButtonClick = function() {
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('content-type-save'),
            form.serialize(), onSaveCallBack);
    };
    module.initialize = function() {
        dialog = $('#create-content-type-modal');
        form = $('#create-content-type-form');
        form.parsley();
        $('.btn-save', dialog).on('click', onSaveButtonClick);
    };
    return module;
}());


launchcms.ContentTypePage = (function () {
    var module = {};
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content-type.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }

    };
    var onDeleteContentType = function() {
        var contentTypeAlias = $(this).attr('data-alias');
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('content-type-delete'),
                    {'alias': contentTypeAlias,
                        '_token': launchcms.getCSRFToken()}, onDeleteCallBack);
            }
        });
    };
    module.refreshPage = function() {
        $.getJSON(
            launchcms.getApiUrl('content-type-list'),
            function(result) {
                if(result.success) {
                    var html = launchcms.Template.renderWithInPageTemplate('#tpl-content-type-body', {data: result.data});
                    $('#table-content-type').find('tbody').html(html);
                }
            }
        );
    };
    module.initialize = function() {
        launchcms.Events.on("content-type.created", this.refreshPage , this);
        launchcms.Events.on("content-type.deleted", this.refreshPage , this);
        $('body').on('click', '.btn-content-type-delete', onDeleteContentType);
        module.refreshPage();
    };
    return module;
}());


$(function () {

    //we need to init select2 component before intializing parsley. If not, it will throw exception:
    //JS error uncaught range error: maximum call stack size exceeded. Still don't know the reason yet.
    $(".select2").select2();
    launchcms.CreateContentTypeDialog.initialize();
    launchcms.ContentTypePage.initialize();
    launchcms.Notification.unqueueAll();
});