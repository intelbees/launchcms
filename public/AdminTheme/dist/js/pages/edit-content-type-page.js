launchcms.FieldGroupSelector = (function () {
    var module = {};
    module.loadData = function(contentType) {
        var fieldGroups = [];
        if(contentType) {
            fieldGroups = contentType.field_groups;
        } else {
            contentType = launchcms.EditContentTypePage.getContentTypeData();
            if(contentType == null) {
                return;
            }
            fieldGroups = contentType.field_groups;
        }
        var htmlForSelect = "<option value=''></option>";
        for(var i=0; i< fieldGroups.length; i++) {
            htmlForSelect += "<option value='" + fieldGroups[i].alias +"'>" + fieldGroups[i].name + "</option>";
        }
        var fieldGroupSelect = $('.field-group-select');
        fieldGroupSelect.html(htmlForSelect);
    };
    return module;
}());

launchcms.FieldGroupDialog = (function () {
    var dialog;
    var fieldGroupForm;
    var module = {};
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            fieldGroupForm[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('field-group.saved');
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    };
    var onSaveFieldGroup = function() {
        if(!fieldGroupForm.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('content-type-field-group-save'),
            $(fieldGroupForm).serialize(), onSaveCallBack);
    };
    module.showDialog = function(fieldGroup) {
        fieldGroupForm[0].reset();
        $('input[name="field_group_alias"]', fieldGroupForm).val('');
        if(fieldGroup) {
            $('input[name="name"]', fieldGroupForm).val(fieldGroup.name);
            $('input[name="alias"]', fieldGroupForm).val(fieldGroup.alias);
            $('textarea[name="description"]', fieldGroupForm).val(fieldGroup.description);
            $('input[name="field_group_alias"]', fieldGroupForm).val(fieldGroup.alias);
            $('input[name="position"]', fieldGroupForm).val(fieldGroup.position);
        }
        dialog.modal('show');

    };
    module.initialize = function() {
        fieldGroupForm = $('#field-group-form');
        fieldGroupForm.parsley();
        dialog = $('#field-group-modal');
        $('.btn-save', dialog).on('click', onSaveFieldGroup);

    };
    return module;
}());

launchcms.EditContentTypePage = (function () {
    var module = {};
    var contentTypeForm;
    var tableFieldGroup;
    var tableField;
    var contentType = null;
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content-type.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }

    };
    var onDeleteContentType = function() {
        var contentTypeAlias = $(this).attr('data-alias');
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('content-type-delete'),
                    {'alias': contentTypeAlias,
                        '_token': launchcms.csrf_token()}, onDeleteCallBack);
            }
        });
    };

    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            if(result['alias_changed']) {
                location.href = result['content_type_url'];
            }
            $('.error-panel', contentTypeForm).addClass('hidden');
            launchcms.Events.trigger('content-type.updated');
        } else {
            $('.error-panel', contentTypeForm).removeClass('hidden');
            $('.error-message', contentTypeForm).html(result.error);
        }
    };
    var onSaveContentType = function() {
        if(!contentTypeForm.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('content-type-save'),
            $(contentTypeForm).serialize(), onSaveCallBack);
    };

    var reloadPageElements = function() {
        var html = launchcms.Template.renderWithInPageTemplate('#tpl-table-field-group-body', {data: contentType.own_field_groups});
        tableFieldGroup.find('tbody').html(html);

        html = launchcms.Template.renderWithInPageTemplate('#tpl-table-field-body', {data: contentType.fields});
        tableField.find('tbody').html(html);

        launchcms.FieldGroupSelector.loadData();
        launchcms.ContentTypeSettingTab.loadData(contentType);
        launchcms.Events.trigger('page.reload-data', contentType);
    };

    var findFieldGroup = function(alias) {
        for(var i=0;i<contentType.field_groups.length; i++) {
            if(contentType.field_groups[i].alias === alias) {
                return contentType.field_groups[i];
            }
        }
        return null;
    };
    var findField = function(alias) {
        for(var i=0;i<contentType.fields.length; i++) {
            if(contentType.fields[i].alias === alias) {
                return contentType.fields[i];
            }
        }
        return null;
    };

    var onEditFieldGroup = function() {
        var fieldGroupAlias = $(this).attr('data-alias');
        var fieldGroup = findFieldGroup(fieldGroupAlias);
        if(fieldGroup != null) {
            launchcms.FieldGroupDialog.showDialog(fieldGroup);
        }
    };
    var onDeleteFieldGroupCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('field-group.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }
    };
    var onDeleteFieldGroup = function() {
        var fieldGroupAlias = $(this).attr('data-alias');

        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('content-type-field-group-delete'),
                    {
                        'content_type_alias' : contentType.alias,
                        'field_group_alias': fieldGroupAlias,
                        '_token': launchcms.getCSRFToken()
                    }, onDeleteFieldGroupCallBack);
            }
        });
    };
    var onEditField = function() {
        var fieldAlias = $(this).attr('data-alias');
        var field = findField(fieldAlias);
        if(field != null) {
            var dataType = field.data_type;
            if(launchcms.Fields[dataType]) {
                launchcms.Fields[dataType].showDialog(field);
            }

        }
    };
    var onDeleteFieldCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('',
                result.message);
            launchcms.Events.trigger('field.deleted');
        } else {
            launchcms.Notification.notifyError('', launchcms.getMessage(result.error));
        }
    };
    var onDeleteField = function() {
        var fieldAlias = $(this).attr('data-alias');

        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('content-type-field-delete'),
                    {
                        'content_type_alias' : contentType.alias,
                        'field_alias': fieldAlias,
                        '_token': launchcms.getCSRFToken()
                    }, onDeleteFieldCallBack);
            }
        });
    };
    module.loadPageData = function() {
        var contentTypeAlias = $('#content_type_alias').val();
        var fetchContentTypeApi = launchcms.getApiUrl('content-type-get') + '/' + contentTypeAlias;
        $.getJSON(fetchContentTypeApi,
            function(result) {
                if(result.success) {
                    contentType = result.data;
                    reloadPageElements();
                }
            }
        );
    };
    module.initialize = function() {
        contentTypeForm = $('#content-type-general-field-form');
        contentTypeForm.parsley();
        launchcms.Events.on("content-type.deleted", this.loadPageData , this);
        launchcms.Events.on("field-group.saved", this.loadPageData , this);
        launchcms.Events.on("field-group.deleted", this.loadPageData , this);
        launchcms.Events.on("field.saved", this.loadPageData , this);
        launchcms.Events.on("field.deleted", this.loadPageData , this);


        $('.btn-content-type-delete').on('click', onDeleteContentType);
        $('.btn-save-content-type').on('click', onSaveContentType);
        $('.checkbox_enable_localization', contentTypeForm).change(function() {
            var isChecked = $(this).is(':checked')
            if(isChecked) {
                $('#form-group-primary-locale').removeClass('hide');
            } else {
                $('#form-group-primary-locale').addClass('hide');
            }
        });
        $('.btn-create-field-group').on('click', function() {
            launchcms.FieldGroupDialog.showDialog();
        });

        $('.btn-create-field').on('click', function() {
            launchcms.FieldDialog.showDialog();
        });
        $('body').on('click', '.btn-field-group-edit', onEditFieldGroup);
        $('body').on('click', '.btn-field-group-delete', onDeleteFieldGroup);
        $('body').on('click', '.btn-field-edit', onEditField);
        $('body').on('click', '.btn-field-delete', onDeleteField);


        tableFieldGroup = $('#table-field-group');
        tableField = $('#table-field');
        module.loadPageData();
    };

    module.getContentTypeData = function() {
        return contentType;
    };
    return module;
}());

launchcms.ContentTypeSettingTab = (function () {
    var module = {};
    var settingTab;
    var settingForm;

    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            $('.error-panel', settingForm).addClass('hidden');
            launchcms.Events.trigger('content-type.updated');
        } else {
            $('.error-panel', settingForm).removeClass('hidden');
            $('.error-message', settingForm).html(result.error);
        }
    };
    var onSaveSettings = function() {
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('content-type-save-settings'),
            $(settingForm).serialize(), onSaveCallBack);
    };
    var loadFieldSettings = function(contentType, fields, settingKey) {
        var userSelectedValues = [];
        if(contentType.settings[settingKey]) {
            userSelectedValues = contentType.settings[settingKey];
        }
        var htmlForSelect = "";
        for(var i=0; i< fields.length; i++) {
            if(userSelectedValues.indexOf(fields[i].alias) >= 0) {
                htmlForSelect += "<option selected='selected' value='" + fields[i].alias +"'>" + fields[i].name + "</option>";
            } else {
                htmlForSelect += "<option value='" + fields[i].alias +"'>" + fields[i].name + "</option>";
            }
        }
        var settingFieldElement = $('select[name="' + settingKey + '[]"]', settingTab);
        settingFieldElement.html(htmlForSelect);
        launchcms.UIHelper.makeOrderingSelect(settingFieldElement);
    };
    module.loadData = function(contentType) {

        if(typeof(contentType) == 'undefined') {
            contentType = launchcms.EditContentTypePage.getContentTypeData();
        }
        if(contentType == null) {
            return;
        }
        loadFieldSettings(contentType, contentType.configurable_table_display_fields, 'table_display_fields');
        loadFieldSettings(contentType, contentType.configurable_table_display_fields, 'filter_fields');
        loadFieldSettings(contentType, contentType.configurable_editable_fields, 'editable_fields');

    };
    module.initialize = function() {
        settingTab = $('#tab_settings');
        settingForm = $('#content-type-settings-form');
        $('.btn-save-content-type-settings', settingTab).on('click', onSaveSettings);
        module.loadData();
    };
    return module;
}());

$(function () {

    $(".select2").select2();
    launchcms.FieldGroupDialog.initialize();
    launchcms.ContentTypeSettingTab.initialize();
    launchcms.EditContentTypePage.initialize();
});