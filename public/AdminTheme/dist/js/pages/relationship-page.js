launchcms.CreateRelationshipDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            form[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('relationship.created');
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    };
    var onSaveButtonClick = function() {
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('relationship-save'),
            form.serialize(), onSaveCallBack);
    };
    module.show = function(relationship) {
        form[0].reset();
        $('input[name="alias"]', form).attr('readonly', false);
        if(relationship) {
            $('input[name="relation_definition_alias"]', form).val(relationship.alias);
            $('input[name="name"]', form).val(relationship.name);
            $('input[name="alias"]', form).val(relationship.alias);
            $('input[name="alias"]', form).attr('readonly', 'readonly');
            $('select[name="inverse"]', form).val(relationship.inverse);
            $('select[name="need_approval"]', form).val(relationship.need_approval);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        dialog = $('#create-relationship-modal');
        form = $('#create-relationship-form');
        form.parsley();
        $('.btn-save', dialog).on('click', onSaveButtonClick);
    };
    return module;
}());


launchcms.RelationshipPage = (function () {
    var module = {};
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('relationship.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }

    };

    var onDeleteRelationship = function() {
        var relationshipAlias = $(this).attr('data-alias');
        console.log(relationshipAlias);
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('relationship-delete'),
                    {'alias': relationshipAlias,
                        '_token': launchcms.getCSRFToken()}, onDeleteCallBack);
            }
        });
    };
    var onEditRelationship = function() {
        var relationshipAlias = $(this).attr('data-alias');
        console.log(relationshipAlias);
        var apiToGetDetail = launchcms.getApiUrl('relationship-detail') + '/' + relationshipAlias;
        $.getJSON(
            apiToGetDetail,
            function(result) {
                if(result.success) {
                    launchcms.CreateRelationshipDialog.show(result.data);
                }
            }
        );
    };
    module.refreshPage = function() {
        $.getJSON(
            launchcms.getApiUrl('relationship-list'),
            function(result) {
                if(result.success) {
                    var html = launchcms.Template.renderWithInPageTemplate('#tpl-relationship-body', {data: result.data});
                    $('#table-relationship').find('tbody').html(html);
                }
            }
        );
    };
    module.initialize = function() {
        launchcms.Events.on("relationship.created", this.refreshPage , this);
        launchcms.Events.on("relationship.deleted", this.refreshPage , this);
        $('.btn-create-relationship').on('click', function() {
            launchcms.CreateRelationshipDialog.show();
        });
        $('body').on('click', '.btn-relationship-delete', onDeleteRelationship);
        $('body').on('click', '.btn-relationship-edit', onEditRelationship);

        module.refreshPage();
    };
    return module;
}());


$(function () {

    //we need to init select2 component before intializing parsley. If not, it will throw exception:
    //JS error uncaught range error: maximum call stack size exceeded. Still don't know the reason yet.
    $(".select2").select2();
    launchcms.CreateRelationshipDialog.initialize();
    launchcms.RelationshipPage.initialize();

});