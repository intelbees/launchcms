launchcms.CreateContentDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var processAfterSave = function(result, redirectToContentPage) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            form[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content.created');
            if(redirectToContentPage == true) {
                window.location.href = result.content_url;
            }
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    }
    var onSaveCallBack = function(result) {
        processAfterSave(result, false);
    };
    var onSaveAndModifyCallBack = function(result) {
        processAfterSave(result, true);
    };

    var onSaveButtonClick = function() {
        saveContent(onSaveCallBack);
    };

    var onSaveAndModifyButtonClick = function () {
        saveContent(onSaveAndModifyCallBack);
    };

    var saveContent = function(callbackFunc) {
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('content-save-draft'),
            form.serialize(), callbackFunc);
    }
    module.initialize = function() {
        dialog = $('#create-content-modal');
        form = $('#create-content-form');
        form.parsley();
        $('.btn-save', dialog).on('click', onSaveButtonClick);
        $('.btn-save-modify', dialog).on('click', onSaveAndModifyButtonClick);
    };
    return module;
}());


launchcms.ContentListingPage = (function () {
    var module = {};
    var renderPageCallback = function(result) {
        if(result.success) {
            var html = launchcms.Template.renderWithInPageTemplate('#tpl-content-body',
                {data: result.result, displayFields: result.display_fields}
            );
            launchcms.MainPager.currentPage = result.current_page;
            launchcms.MainPager.totalPage = result.total_page;
            launchcms.MainPager.reload();
            launchcms.DataTableInfo.reload(result.current_page, result.page_size, result.result_count, result.total_record)
            $('#table-content').find('tbody').html(html);
        }
    };
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }

    };
    var onDeleteContent = function() {
        console.log('here');
        var contentID = $(this).attr('data-id');
        var confirmationMessage = $(this).attr('data-confirm-message');

        var onConfirmCompleted = function(result) {
            if(!result) {
                return;
            }
            launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
            var contentTypeAlias = $('#content_type_alias').val();
            var apiURL = launchcms.getApiUrl('content-delete');
            var postData = {
                'id': contentID,
                'content_type_alias': contentTypeAlias,
                '_token': launchcms.getCSRFToken()
            };
            $.post(apiURL, postData, onDeleteCallBack);
        };
        launchcms.ConfirmationDialog.show(confirmationMessage, onConfirmCompleted);

    };
    module.refreshPage = function() {
        var contentTypeAlias = $('#content_type_alias').val();
        var apiURL = '/' +  launchcms.adminSlug + '/ajax/content/' + contentTypeAlias + '/list';
        var currentPage = launchcms.MainPager.currentPage;
        if(currentPage != 1) {
            apiURL += '?current=' + currentPage;
        }
        $.getJSON(apiURL, renderPageCallback);
    };
    module.initialize = function() {
        launchcms.Events.on("content.created", this.refreshPage , this);
        launchcms.Events.on("content.deleted", this.refreshPage , this);
        $('body').on('click', '.btn-content-delete', onDeleteContent);
        module.refreshPage();
    };
    return module;
}());

launchcms.DataTableInfo = (function () {
    var module = {};
    var infoPanel;
    var fromRecordEl, toRecordEl, totalRecordEl;
    module.initialize = function() {
        infoPanel = $('.table_info');
        fromRecordEl = $('.from_record', infoPanel);
        toRecordEl = $('.to_record', infoPanel);
        totalRecordEl = $('.total_record', infoPanel);
    };
    module.reload = function(currentPage, pageSize, resultCount, totalRecord) {
        var prevPage = currentPage - 1 >= 0 ? currentPage - 1: 0;
        var fromRecord = (prevPage * pageSize) + 1;
        var toRecord = (fromRecord + resultCount) -1;
        if(resultCount === 0) {
            fromRecord = 0;
        }
        fromRecordEl.html(fromRecord);
        toRecordEl.html(toRecord);
        totalRecordEl.html(totalRecord);
    };
    return module;
}());
launchcms.MainPager = (function () {
    var module = {};
    var mainPager;
    var prevPageButton;
    var nextPageButton;
    module.currentPage = 1;
    module.totalPage = 1;


    var onPrevPage = function() {
        if(module.currentPage - 1 >= 1) {
            module.currentPage -= 1;
            launchcms.ContentListingPage.refreshPage();
        }
    };

    var onNextPage = function() {
        if(module.currentPage + 1 <= module.totalPage) {
            module.currentPage += 1;
            launchcms.ContentListingPage.refreshPage();
        }
    };
    module.reload = function() {
        if(module.currentPage == 1) {
            prevPageButton.addClass('hide');
        } else {
            prevPageButton.removeClass('hide');
        }

        if(module.currentPage >= module.totalPage) {
            nextPageButton.addClass('hide');
        } else {
            nextPageButton.removeClass('hide');
        }
    };
    module.initialize = function() {
        mainPager = $('.main_pager');
        prevPageButton = $('.previous-page', mainPager);
        nextPageButton = $('.next-page', mainPager);
        prevPageButton.on('click', onPrevPage);
        nextPageButton.on('click', onNextPage);
    };
    return module;
}());
$(function () {

    //we need to init select2 component before initializing parsley. If not, it will throw exception:
    //JS error uncaught range error: maximum call stack size exceeded. Still don't know the reason yet.
    $(".select2").select2();
    launchcms.CreateContentDialog.initialize();
    launchcms.MainPager.initialize();
    launchcms.DataTableInfo.initialize();
    launchcms.ContentListingPage.initialize();
});