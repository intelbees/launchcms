launchcms.CreateWorkflowDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            form[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('workflow.created');
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
            if(result.detail_errors) {
                var firstError = result.detail_errors[0];
                $('.detail-error-message', dialog).html(firstError);
            }
        }
    };
    var onSaveButtonClick = function() {
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('workflow-save'),
            form.serialize(), onSaveCallBack);
    };
    module.show = function(workflow) {
        form[0].reset();
        $('input[name="workflow_alias"]', form).val('');
        $('input[name="alias"]', form).attr('readonly', false);
        if(workflow) {
            $('input[name="workflow_alias"]', form).val(workflow.alias);
            $('input[name="name"]', form).val(workflow.name);
            $('input[name="alias"]', form).val(workflow.alias);
            $('input[name="alias"]', form).attr('readonly', 'readonly');
            $('textarea[name="json"]', form).val(workflow.json);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        dialog = $('#create-workflow-modal');
        form = $('#create-workflow-form');
        form.parsley();
        $('.btn-save', dialog).on('click', onSaveButtonClick);
    };
    return module;
}());


launchcms.WorkflowPage = (function () {
    var module = {};
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('workflow.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }

    };

    var onDeleteWorkflow = function() {
        var workflowID = $(this).attr('data-id');
        console.log(workflowID);
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('workflow-delete'),
                    {'id': workflowID,
                        '_token': launchcms.getCSRFToken()}, onDeleteCallBack);
            }
        });
    };
    var onEditWorkflow = function() {
        var workflowID = $(this).attr('data-id');
        var apiToGetDetail = launchcms.getApiUrl('workflow-detail') + '/' + workflowID;
        $.getJSON(
            apiToGetDetail,
            function(result) {
                if(result.success) {
                    launchcms.CreateWorkflowDialog.show(result.data);
                }
            }
        );
    };
    module.refreshPage = function() {
        $.getJSON(
            launchcms.getApiUrl('workflow-list'),
            function(result) {
                if(result.success) {
                    var html = launchcms.Template.renderWithInPageTemplate('#tpl-workflow-body', {data: result.data});
                    $('#table-workflow').find('tbody').html(html);
                }
            }
        );
    };
    module.initialize = function() {
        launchcms.Events.on("workflow.created", this.refreshPage , this);
        launchcms.Events.on("workflow.deleted", this.refreshPage , this);
        $('.btn-create-workflow').on('click', function() {
            launchcms.CreateWorkflowDialog.show();
        });
        $('body').on('click', '.btn-workflow-delete', onDeleteWorkflow);
        $('body').on('click', '.btn-workflow-edit', onEditWorkflow);

        module.refreshPage();
    };
    return module;
}());


$(function () {

    //we need to init select2 component before intializing parsley. If not, it will throw exception:
    //JS error uncaught range error: maximum call stack size exceeded. Still don't know the reason yet.
    $(".select2").select2();
    launchcms.CreateWorkflowDialog.initialize();
    launchcms.WorkflowPage.initialize();

});