launchcms.EditDataTypePage = (function () {
    var module = {};
    var dataTypeForm;
    var tableField;
    var dataType = null;
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('data-type.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }
    };
    var onDeleteDataType = function() {
        var dataTypeAlias = $(this).attr('data-alias');
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('data-type-delete'),
                    {'alias': dataTypeAlias,
                        '_token': launchcms.csrf_token()}, onDeleteCallBack);
            }
        });
    };

    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            if(result['alias_changed']) {
                location.href = result['data_type_url'];
            }
            $('.error-panel', dataTypeForm).addClass('hidden');
            launchcms.Events.trigger('data-type.updated');
        } else {
            $('.error-panel', dataTypeForm).removeClass('hidden');
            $('.error-message', dataTypeForm).html(result.error);
        }
    };
    var onSaveDataType = function() {
        if(!dataTypeForm.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('data-type-save'),
            $(dataTypeForm).serialize(), onSaveCallBack);
    };

    var reloadPageElements = function() {
        html = launchcms.Template.renderWithInPageTemplate('#tpl-table-field-body', {data: dataType.fields});
        tableField.find('tbody').html(html);
        launchcms.DataTypeSettingTab.loadData(dataType);
        launchcms.Events.trigger('page.reload-data', dataType);
    };


    var findField = function(alias) {
        for(var i=0;i<dataType.fields.length; i++) {
            if(dataType.fields[i].alias === alias) {
                return dataType.fields[i];
            }
        }
        return null;
    };


    var onEditField = function() {
        var fieldAlias = $(this).attr('data-alias');
        var field = findField(fieldAlias);
        if(field != null) {
            var dataType = field.data_type;
            if(launchcms.Fields[dataType]) {
                launchcms.Fields[dataType].showDialog(field);
            }

        }
    };
    var onDeleteFieldCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('',
                result.message);
            launchcms.Events.trigger('field.deleted');
        } else {
            launchcms.Notification.notifyError('', launchcms.getMessage(result.error));
        }
    };
    var onDeleteField = function() {
        var fieldAlias = $(this).attr('data-alias');

        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('data-type-field-delete'),
                    {
                        'data_type_alias' : dataType.alias,
                        'field_alias': fieldAlias,
                        '_token': launchcms.getCSRFToken()
                    }, onDeleteFieldCallBack);
            }
        });
    };
    module.loadPageData = function() {
        var dataTypeAlias = $('#data_type_alias').val();
        var fetchDataTypeApi = launchcms.getApiUrl('data-type-get') + '/' + dataTypeAlias;
        $.getJSON(fetchDataTypeApi,
            function(result) {
                if(result.success) {
                    dataType = result.data;
                    reloadPageElements();
                }
            }
        );
    };
    module.initialize = function() {
        dataTypeForm = $('#data-type-general-field-form');
        dataTypeForm.parsley();
        launchcms.Events.on("data-type.deleted", this.loadPageData , this);
        launchcms.Events.on("field.saved", this.loadPageData , this);
        launchcms.Events.on("field.deleted", this.loadPageData , this);


        $('.btn-data-type-delete').on('click', onDeleteDataType);
        $('.btn-save-data-type').on('click', onSaveDataType);

        $('.btn-create-field').on('click', function() {
            launchcms.FieldDialog.showDialog();
        });
        $('body').on('click', '.btn-field-edit', onEditField);
        $('body').on('click', '.btn-field-delete', onDeleteField);
        tableField = $('#table-field');
        module.loadPageData();
    };

    module.getDataTypeData = function() {
        return dataType;
    };
    return module;
}());

launchcms.DataTypeSettingTab = (function () {
    var module = {};
    var settingTab;
    var settingForm;

    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            $('.error-panel', settingForm).addClass('hidden');
            launchcms.Events.trigger('content-type.updated');
        } else {
            $('.error-panel', settingForm).removeClass('hidden');
            $('.error-message', settingForm).html(result.error);
        }
    };
    var onSaveSettings = function() {
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('data-type-save-settings'),
            $(settingForm).serialize(), onSaveCallBack);
    };
    var loadFieldSettings = function(dataType, fields, settingKey) {
        var userSelectedValues = [];
        if(dataType.settings[settingKey]) {
            userSelectedValues = dataType.settings[settingKey];
        }
        var htmlForSelect = "";
        for(var i=0; i< fields.length; i++) {
            if(userSelectedValues.indexOf(fields[i].alias) >= 0) {
                htmlForSelect += "<option selected='selected' value='" + fields[i].alias +"'>" + fields[i].name + "</option>";
            } else {
                htmlForSelect += "<option value='" + fields[i].alias +"'>" + fields[i].name + "</option>";
            }
        }
        var settingFieldElement = $('select[name="' + settingKey + '[]"]', settingTab);
        settingFieldElement.html(htmlForSelect);
        launchcms.UIHelper.makeOrderingSelect(settingFieldElement);
    };
    module.loadData = function(dataType) {

        if(typeof(dataType) == 'undefined') {
            dataType = launchcms.EditDataTypePage.getDataTypeData();
        }
        if(dataType == null) {
            return;
        }
        loadFieldSettings(dataType, dataType.configurable_fields, 'table_display_fields');
        loadFieldSettings(dataType, dataType.all_fields, 'editable_fields');

    };
    module.initialize = function() {
        settingTab = $('#tab_settings');
        settingForm = $('#data-type-settings-form');
        $('.btn-save-data-type-settings', settingTab).on('click', onSaveSettings);
        module.loadData();
    };
    return module;
}());


$(function () {
    $(".select2").select2();
    launchcms.EditDataTypePage.initialize();
    launchcms.DataTypeSettingTab.initialize();
});