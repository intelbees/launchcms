launchcms.CreateDataTypeDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            form[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('data-type.created');
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    };
    var onSaveButtonClick = function() {
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('data-type-save'),
            form.serialize(), onSaveCallBack);
    };
    module.initialize = function() {
        dialog = $('#create-data-type-modal');
        form = $('#create-data-type-form');
        form.parsley();
        $('.btn-save', dialog).on('click', onSaveButtonClick);
    };
    return module;
}());


launchcms.DataTypePage = (function () {
    var module = {};
    var onDeleteCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('data-type.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }

    };
    var onDeleteDataType = function() {
        var dataTypeAlias = $(this).attr('data-alias');
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
                $.post(launchcms.getApiUrl('data-type-delete'),
                    {'alias': dataTypeAlias,
                        '_token': launchcms.getCSRFToken()}, onDeleteCallBack);
            }
        });
    };
    module.refreshPage = function() {
        $.getJSON(
            launchcms.getApiUrl('data-type-list'),
            function(result) {
                if(result.success) {
                    var html = launchcms.Template.renderWithInPageTemplate('#tpl-data-type-body', {data: result.data});
                    $('#table-data-type').find('tbody').html(html);
                }
            }
        );
    };
    module.initialize = function() {
        launchcms.Events.on("data-type.created", this.refreshPage , this);
        launchcms.Events.on("data-type.deleted", this.refreshPage , this);
        $('body').on('click', '.btn-data-type-delete', onDeleteDataType);
        module.refreshPage();
    };
    return module;
}());


$(function () {

    //we need to init select2 component before intializing parsley. If not, it will throw exception:
    //JS error uncaught range error: maximum call stack size exceeded. Still don't know the reason yet.
    $(".select2").select2();
    launchcms.CreateDataTypeDialog.initialize();
    launchcms.DataTypePage.initialize();
    launchcms.Notification.unqueueAll();
});