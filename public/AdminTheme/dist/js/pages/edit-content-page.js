launchcms.EditContentPage = (function () {
    var module = {};
    var contentForm;
    var contentInputPanel;

    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content.saved');
        } else {
            $('.error-panel', contentInputPanel).removeClass('hidden');
            $('.error-message', contentInputPanel).html(result.error);
        }
    };
    var onSaveContent = function() {
        module.saveMainForm();
    };

    var showClientSideErrorPanel = function() {
        $('.error-panel', contentInputPanel).removeClass('hidden');
        $('#server-validation-message').addClass('hidden');
        $('#global-validation-message').removeClass('hidden');
    };
    var hideClientSideErrorPanel = function() {
        $('.error-panel', contentInputPanel).addClass('hidden');
        $('#global-validation-message').addClass('hidden');
        $('#server-validation-message').removeClass('hidden');
    };

    module.validateMainForm = function() {
        var validateResult = contentForm.parsley().validate();
        if(!validateResult) {
            showClientSideErrorPanel();
            return;
        } else {
            hideClientSideErrorPanel();
        }
        return validateResult;
    };
    module.saveMainForm = function(silence) {
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].updateElement();
        }
        if(!module.validateMainForm()) {
            return;
        }
        var contentSaveApi = launchcms.getApiUrl('content-save');
        if(silence) {
            $.post(contentSaveApi,
                $(contentForm).serialize(), function() {
                    launchcms.Events.trigger('content.saved');
                });
        } else {
            launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
            $.post(contentSaveApi,
                $(contentForm).serialize(), onSaveCallBack);
        }

    };
    module.initialize = function() {
        contentForm = $('#content-form');
        contentInputPanel =$('#content-input-panel');
        $(".select2").select2();
        $('#content-name').change(function() {
            $('#content-name-on-title').html($(this).val());
        });

        $('.btn-save-content', contentInputPanel).on('click', onSaveContent);
        $('.btn-state-change').on('click', function() {
            var workflowData = {};
            workflowData.transitionName = $(this).attr('data-transition-name');
            workflowData.state = $(this).attr('data-to-alias');
            workflowData.systemState = $(this).attr('data-destination-system-state');
            launchcms.WorkflowDialog.showDialog(workflowData);
        });
        //Google map will not display in tab if the tab is not shown/activated.
        //Below is the only workaround way - we catch the event when tab is activate and trigger resizing the map.
        $('.nav-tabs a').on('shown.bs.tab', function(){
            launchcms.FieldEditors.GeoEditor.refreshMap();
        });
    };
    return module;
}());

launchcms.WorkflowDialog = (function () {
    var dialog;
    var workflowForm;
    var module = {};
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            workflowForm[0].reset();
            $(dialog).modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            window.location.reload();
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    };
    var onSaveWorkflow = function() {
        if(!workflowForm.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('content-change-status'),
            $(workflowForm).serialize(), onSaveCallBack);
    };
    module.showDialog = function(workflowData) {
        workflowForm[0].reset();
        if(workflowData) {
            $('.modal-title', dialog).html(workflowData.transitionName);
            $('input[name="next_state"]', dialog).val(workflowData.state);
            console.log(workflowData);
            if(workflowData.systemState === 'scheduled') {
                $('.schedule_field', dialog).removeClass('hide');
            } else {
                $('.schedule_field', dialog).addClass('hide');
            }
        }
        dialog.modal('show');

    };
    module.initialize = function() {
        workflowForm = $('#workflow-form');
        workflowForm.parsley();
        dialog = $('#workflow-modal');
        $('.btn-save-workflow-state', dialog).on('click', onSaveWorkflow);

    };
    return module;
}());

$(function () {
    launchcms.EditContentPage.initialize();
    launchcms.FieldEditors.TextFieldEditor.initialize();
    launchcms.FieldEditors.DatePickerEditor.initialize();
    launchcms.FieldEditors.TimePickerEditor.initialize();
    launchcms.FieldEditors.ReferenceEditor.initialize();
    launchcms.FieldEditors.ReferenceListEditor.initialize();
    launchcms.FieldEditors.EmbeddedListEditor.initialize();
    launchcms.FieldEditors.IntegerEditor.initialize();
    launchcms.FieldEditors.DoubleEditor.initialize();
    launchcms.FieldEditors.GeoEditor.initialize();

    launchcms.WorkflowDialog.initialize();
});