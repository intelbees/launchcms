launchcms.MediaDialog = (function () {
    var dialog;
    var form;
    var module = {};

    module.initialize = function() {

    };
    return module;
}());

function getUrlParam(paramName)
{
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;

    return (match && match.length > 1) ? match[1] : '' ;
}

launchcms.NewFolderDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var currentPath;
    var onCreateFolderCallBack = function(result) {

        if(result.success) {
            dialog.modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('media-folder.created');
        } else {
            $('.error-panel', dialog).removeClass('hidden');
            $('.error-message', dialog).html(result.error);
        }
    };

    var onCreateFolder = function() {
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.post(launchcms.getApiUrl('media-create-folder'),
            form.serialize(), onCreateFolderCallBack);

    };
    module.showDialog = function(path) {
        currentPath = path;
        $('input[name="path"]').val(currentPath);
        dialog.modal('show');
    };
    module.initialize = function() {
        dialog = $('#new-media-folder');
        form = $('#create-media-folder-form');
        form.parsley();
        $('.btn-save', dialog).on('click', onCreateFolder);
    };
    return module;
}());


launchcms.MediaPage = (function () {
    var module = {};
    var mediaTable;
    var mediaContainer;
    var currentPath;
    var tmpPath;
    var fetchMediaFolderCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(!result.success) {
            launchcms.Notification.notifyError('', result.error);
            return;
        }
        currentPath = tmpPath;
        var html = launchcms.Template.renderWithInPageTemplate('#tpl-media-folder-info-body',
            {'files': result.data.files, 'subfolders': result.data.subfolders});
        $('#media-table').find('tbody').html(html);
        var html = launchcms.Template.renderWithInPageTemplate('#tpl-media-breadcrumb',
            {'breadcrumbs': result.data.breadcrumbs, 'currentFolderName': result.data.folderName});

        mediaContainer.find('.breadcrumb').html(html);
        $('.media-input-file').fileinput('destroy');
        var uploadFileUrl = launchcms.getApiUrl('media-upload');
        $('.media-input-file').fileinput({
            'dropZoneEnabled': false,
            'previewFileTypes': false,
            'showPreview' : true,
            'allowedPreviewMimeTypes': false,
            'uploadUrl': uploadFileUrl,
            'uploadExtraData': {'_token': launchcms.getCSRFToken(), 'path': currentPath}
        }).off('fileuploaded').on('fileuploaded', function(event, data) {
            launchcms.Notification.notifySuccess('', data.response.message);
            $('.media-input-file').fileinput('clear').fileinput('enable');;
            launchcms.Events.trigger('media-file.created');
            $('#upload-file-modal').modal('hide');
        });
    };
    module.openFolder = function(path) {
        tmpPath = path;

        var mediaAPI = launchcms.getApiUrl('media-info') + "?folder=" + path;
        launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
        $.getJSON(mediaAPI, fetchMediaFolderCallBack);

    };



    var onDeleteFileCallBack = function(result) {
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('media-file.deleted');
        } else {
            launchcms.Notification.notifyError('', result.error);
        }
    };

    var onDeleteMediaItems = function() {
        var paths = $('input[name="selected-media-items"]:checked').map(function() {
            return this.value;
        }).get();
        var htmlPathPrint = '<ul>';
        var displayPaths = $('input[name="selected-media-items"]:checked').map(function() {
            return $(this).attr('data-path');
        }).get();
        for(var i=0; i<displayPaths.length; i++) {
            htmlPathPrint += '<li>' + displayPaths[i] + '</li>';
        }
        htmlPathPrint += '</ul>'
        var confirmationMessage = '<p>' + launchcms.getMessage('launchcms.messages.confirm_delete_media_paths') + '</p>' + htmlPathPrint;
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(result) {
                $.post(launchcms.getApiUrl('delete-media-items'),
                    {'paths': paths,
                        '_token': launchcms.getCSRFToken()}, onDeleteFileCallBack);
            }
        });

    };


    var onOpenFolder = function() {
        var path = $(this).attr('data-path');
        module.openFolder(path);
    };

    var onShowUploadFileDialog = function() {
        var dialog = $('#upload-file-modal');
        dialog.modal('show');
    };

    var onRefresh = function() {
        module.openFolder(currentPath);
    };

    var onOpenFile = function() {

    };

    var onCreateNewFolder = function() {
        launchcms.NewFolderDialog.showDialog(currentPath);
    };
    module.initialize = function() {
        launchcms.Events.on("media-file.created", onRefresh , this);
        launchcms.Events.on("media-file.deleted", onRefresh , this);
        launchcms.Events.on("media-folder.created", onRefresh , this);
        launchcms.Events.on("media-folder.deleted", onRefresh , this);

        mediaTable = $('#media-table');
        mediaContainer = $('#media-panel-container');
        mediaContainer.on('click', '.new-folder-button', onCreateNewFolder);
        mediaContainer.on('click', '.upload-file-button', onShowUploadFileDialog);
        mediaContainer.on('click', '.btn-media-item-delete', onDeleteMediaItems);
        mediaContainer.on('click', '.refresh-button', onRefresh);
        mediaContainer.on('click', '.media-folder', onOpenFolder);
        mediaContainer.on('click', '.media-file', onOpenFile);

        $('#check-all-media-items').on('change', function() {
            var checked = $(this).is(':checked');
            $('input[name="selected-media-items"]').prop('checked', checked);
        });
        $('body').on('click', '.selectable-media-file', function() {
            $('#selected-media-item').removeClass('hide');
            var imageURL = $(this).attr('data-url');
            var fileName =$(this).attr('data-file-name');
            var fileSize =$(this).attr('data-file-size');
            $('#preview-file-name').html(fileName);
            $('#preview-file-size').html(fileSize);
            $('#preview-image').attr('src', imageURL);
            $('#btn-select-file').attr('data-image-url', imageURL);
        });
        $('body').on('click', '#btn-select-file', function() {
            var imageUrl = $(this).attr('data-image-url');
            var funcNum = $('#ckeditor-callback').val();
            window.opener.CKEDITOR.tools.callFunction(funcNum, imageUrl);
            window.close();
        });


        var homePath = $('#home-path').val();
        module.openFolder(homePath);
    };
    return module;
}());


