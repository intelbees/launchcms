launchcms.FieldDialog = (function () {
    var dialog;

    var module = {};
    module.showDialog = function() {
        dialog.modal('show');

    };
    var mouseOverField = function() {
        $('.field-select-box', dialog).removeClass('bg-green');
        $('.info-box-icon ', dialog).addClass('bg-aqua');
        $(this).addClass('bg-green');
        $('.info-box-icon ', $(this)).removeClass('bg-aqua');
    };
    var onFieldClick = function() {
        var dataDialogClass =  $(this).attr('data-type');
        if(launchcms.Fields[dataDialogClass]) {
            dialog.modal('hide');
            launchcms.Fields[dataDialogClass].showDialog();
        }
    };
    module.initialize = function() {
        dialog = $('#field-modal');
        $('.field-select-box', dialog).mouseover(mouseOverField);
        $('.field-select-box', dialog).on('click', onFieldClick);
    };
    return module;
}());

launchcms.Fields = {};

launchcms.FieldDialogHelper = function(form, dialog) {
    var thisForm = form;
    var thisDialog = dialog;
    var thisValidationRules = [];
    thisForm.parsley();
    var loadFieldMetaDataTab = function(field) {
        $('input[name="name"]', thisForm).val(field.name);
        $('input[name="alias"]', thisForm).val(field.alias);
        $('textarea[name="description"]', thisForm).val(field.description);
        $('input[name="field_alias"]', thisForm).val(field.alias);
        if($('input[name="position"]', thisForm).length > 0) {
            $('input[name="position"]', thisForm).val(field.position);
        }
        if($('input[name="is_required"]', thisForm).length > 0) {
            $('input[name="is_required"]', thisForm).attr("checked",field.is_required);
        }
        if($('input[name="is_unique"]', thisForm).length > 0) {
            $('input[name="is_unique"]', thisForm).attr("checked",field.is_unique);

        }
        if($('input[name="allow_full_text_search"]', thisForm).length > 0) {
            $('input[name="allow_full_text_search"]', thisForm).attr("checked", field.allow_full_text_search);
        }
        if($('input[name="is_indexed"]', thisForm).length > 0) {
            $('input[name="is_indexed"]', thisForm).attr("checked", field.is_indexed);
        }
        if($('select[name="field_group"]', thisForm).length > 0) {
            $('select[name="field_group"]', thisForm).val(field.field_group);
        }
    };
    var loadFieldValidationTab = function(field) {

        if(field) {
            thisValidationRules = field.validation_rules;
        }
        var validationTable = $('.validation-rule-table', thisForm);
        var htmlOutput = launchcms.Template.renderWithInPageTemplate('#tpl-table-rule-body', {data: thisValidationRules});
        validationTable.find('tbody').html(htmlOutput);
    };


    this.loadFieldMetaData = function(field) {
        loadFieldMetaDataTab(field);
        loadFieldValidationTab(field);
    };
    this.defaultSaveHandler = function() {
        if(!thisForm.parsley().validate()) {
            return;
        }
        var onSaveCallBack = function(result) {
            if(result.success) {
                thisForm[0].reset();
                $('input[name="alias"]', thisForm).val('');
                $(thisDialog).modal('hide');
                launchcms.Notification.notifySuccess('', result.message);
                launchcms.Events.trigger('field.saved');
            } else {
                $('.error-panel', thisDialog).removeClass('hidden');
                $('.error-message', thisDialog).html(result.error);
            }
        };
        var fieldSaveApiAlias = $('#structure_field_save_api_alias').val();
        var fieldSaveAPI = launchcms.getApiUrl(fieldSaveApiAlias);
        $.post(fieldSaveAPI,$(thisForm).serialize(), onSaveCallBack);

    };

    var onAddValidationRule = function() {
        var rule = $('.txt-validation-rule', thisForm).val();
        rule = rule.trim();
        if(rule != '') {
            $('.txt-validation-rule', thisForm).val('');
            thisValidationRules.push(rule);
            loadFieldValidationTab();
        }

    };
    var onDeleteValidationRule = function() {
        var ruleIndex = $(this).attr('data-index');
        $('#validation-rule-' + ruleIndex, thisForm).remove();
    };
    $('.btn-add-validation-rule', thisDialog).on('click', onAddValidationRule);
    $('body').on('click', '.btn-delete-validation-rule', onDeleteValidationRule);
    $('.btn-save', thisDialog).on('click', this.defaultSaveHandler);
};



launchcms.StringFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        if(field.settings) {
            var settings = field.settings;
            $('select[name="settings[editor]"]', form).val(settings.editor);
        }
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#string-field-form');
        dialog = $('#string-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.BooleanFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#boolean-field-form');
        dialog = $('#boolean-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.IntegerFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);

    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#integer-field-form');
        dialog = $('#integer-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());


launchcms.DoubleFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#double-field-form');
        dialog = $('#double-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());


launchcms.DateFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        if(field.settings) {
            var settings = field.settings;
            $('select[name="settings[editor]"]', form).val(settings.editor);
        }
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#date-field-form');
        dialog = $('#date-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());


launchcms.DateTimeFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        if(field.settings) {
            var settings = field.settings;
            $('select[name="settings[editor]"]', form).val(settings.editor);
        }
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#datetime-field-form');
        dialog = $('#datetime-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.EmbeddedFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        var settings = field.settings;
        $('select[name="settings[data_type_id]"]', form).val(settings.data_type_id).trigger('change');
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#embedded-field-form');
        dialog = $('#embedded-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.EmbeddedListFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        var settings = field.settings;
        $('select[name="settings[data_type_id]"]', form).val(settings.data_type_id).trigger('change');
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#embedded-list-field-form');
        dialog = $('#embedded-list-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.ReferenceFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        var settings = field.settings;
        $('select[name="settings[content_type_id]"]', form).val(settings.content_type_id).trigger('change');
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#reference-field-form');
        dialog = $('#reference-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.ReferenceListFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
        var settings = field.settings;
        $('select[name="settings[content_type_id]"]', form).val(settings.content_type_id).trigger('change');
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#reference-list-field-form');
        dialog = $('#reference-list-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

launchcms.GeoFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        defaultDialogHandler.loadFieldMetaData(field);
    };

    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
        form = $('#geo-field-form');
        dialog = $('#geo-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

$(function () {
    launchcms.FieldDialog.initialize();
    launchcms.StringFieldDialog.initialize();
    launchcms.BooleanFieldDialog.initialize();
    launchcms.IntegerFieldDialog.initialize();
    launchcms.DoubleFieldDialog.initialize();
    launchcms.DateFieldDialog.initialize();
    launchcms.DateTimeFieldDialog.initialize();
    launchcms.EmbeddedFieldDialog.initialize();
    launchcms.EmbeddedListFieldDialog.initialize();
    launchcms.ReferenceFieldDialog.initialize();
    launchcms.ReferenceListFieldDialog.initialize();
    launchcms.GeoFieldDialog.initialize();
    launchcms.Fields['string'] = launchcms.StringFieldDialog;
    launchcms.Fields['boolean'] = launchcms.BooleanFieldDialog;
    launchcms.Fields['integer'] = launchcms.IntegerFieldDialog;
    launchcms.Fields['double'] = launchcms.DoubleFieldDialog;
    launchcms.Fields['date'] = launchcms.DateFieldDialog;
    launchcms.Fields['datetime'] = launchcms.DateTimeFieldDialog;
    launchcms.Fields['embedded'] = launchcms.EmbeddedFieldDialog;
    launchcms.Fields['embedded_list'] = launchcms.EmbeddedListFieldDialog;
    launchcms.Fields['reference'] = launchcms.ReferenceFieldDialog;
    launchcms.Fields['reference_list'] = launchcms.ReferenceListFieldDialog;
    launchcms.Fields['geo'] = launchcms.GeoFieldDialog;
});