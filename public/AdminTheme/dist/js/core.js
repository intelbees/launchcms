var launchcms = (function() {
    var module = {};
    module.adminSlug = '/';
    module.messages = {};
    module.csrf_token = '';
    module.getAdminAjaxURL = function(slug) {
        return '/' + module.adminSlug + '/' + slug;
    };
    module.ajax = {
        'content-type-save' : 'ajax/content-type/save',
        'content-type-delete' : 'ajax/content-type/delete',
        'content-type-list': 'ajax/content-type/list',
        'content-type-get': 'ajax/content-type/get',
        'content-type-save-settings': 'ajax/content-type/save-settings',
        'content-type-field-group-save': 'ajax/content-type/field-group/save',
        'content-type-field-group-delete': 'ajax/content-type/field-group/delete',
        'content-type-field-save': 'ajax/content-type/field/save',
        'content-type-field-delete': 'ajax/content-type/field/delete',

        'data-type-save' : 'ajax/data-type/save',
        'data-type-delete' : 'ajax/data-type/delete',
        'data-type-list': 'ajax/data-type/list',
        'data-type-get': 'ajax/data-type/get',
        'data-type-save-settings': 'ajax/data-type/save-settings',
        'data-type-field-save': 'ajax/data-type/field/save',
        'data-type-field-delete': 'ajax/data-type/field/delete',
        'content-save-draft': 'ajax/content/save-draft',
        'content-save': 'ajax/content/save-content',
        'content-delete': 'ajax/content/delete-content',
        'content-change-status': 'ajax/content/change-status',
        'content-save-embedded-record': 'ajax/content/save-embedded-record',
        'relationship-list': 'ajax/relationship/list',
        'relationship-delete': 'ajax/relationship/delete',
        'relationship-save': 'ajax/relationship/save',
        'relationship-detail': 'ajax/relationship/detail',

        'workflow-list': 'ajax/workflow/list',
        'workflow-delete': 'ajax/workflow/delete',
        'workflow-save': 'ajax/workflow/save',
        'workflow-detail': 'ajax/workflow/detail',
        'media-info': 'ajax/media',
        'media-upload': 'ajax/media/upload',
        'delete-media-items': 'ajax/media/delete-media-items',
        'media-create-folder': 'ajax/media/create-folder'


    };
    module.getApiUrl = function(apiName) {
        return '/' +  module.adminSlug + '/' + module.ajax[apiName];
    };
    module.initialize = function() {
        if (!store.enabled) {
            module.notifyInfo(module.getMessage('launchcms.messages.cannot_use_local_storage'));
            return;
        }
        module.csrf_token = $('#page_token').val();
    };
    module.getCSRFToken = function() {
        module.csrf_token = $('#page_token').val();
        return module.csrf_token;
    };

    module.getMessage = function(key) {
        return module.messages[key];
    };

    return module;
}());

launchcms.Template = (function() {
    var module = {};
    module.renderWithTemplateURL = function(templateFile, data) {
        var url = "/" + launchcms.adminSlug + "/" + templateFile;
        return new EJS({url: url}).render(data);
    };
    module.renderText = function (text, data) {
        return new EJS({text: text}).render(data);
    };
    module.renderWithInPageTemplate = function(selector, data) {
        var text = $(selector).html();
        return new EJS({text: text}).render(data);
    };
    return module;

}());

launchcms.UIHelper = (function() {
    var module = {};
    module.makeOrderingSelect = function(selectElement) {
        selectElement.select2({

        });
        selectElement.on("select2:select", function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);

            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
    };
    return module;

}());

launchcms.Events = (function(){
    var module = {},
        idCounter = 0,
        uniqueId = function(prefix) {
            var id = ++idCounter + '';
            return prefix ? prefix + id : id;
        },
        keys = Object.keys || function(obj) {
                if (obj !== Object(obj)) throw new TypeError('Invalid object');
                var keys = [];
                for (var key in obj) if (hasOwnProperty.call(obj, key)) keys.push(key);
                return keys;
            },
        eventSplitter = /\s+/,
        eventsApi = function(obj, action, name, rest) {
            if (!name) return true;

            // Handle event maps.
            if (typeof name === 'object') {
                for (var key in name) {
                    obj[action].apply(obj, [key, name[key]].concat(rest));
                }
                return false;
            }

            // Handle space separated event names.
            if (eventSplitter.test(name)) {
                var names = name.split(eventSplitter);
                for (var i = 0, l = names.length; i < l; i++) {
                    obj[action].apply(obj, [names[i]].concat(rest));
                }
                return false;
            }

            return true;
        },
        triggerEvents = function(events, args) {
            var ev, i = -1, l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
            switch (args.length) {
                case 0: while (++i < l) (ev = events[i]).callback.call(ev.ctx); return;
                case 1: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1); return;
                case 2: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2); return;
                case 3: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2, a3); return;
                default: while (++i < l) (ev = events[i]).callback.apply(ev.ctx, args);
            }
        },
        listenMethods = {listenTo: 'on', listenToOnce: 'once'};

    module.initialize = function(options) {
        $.each(listenMethods, function(method, implementation) {
            module[method] = function(obj, name, callback) {
                var listeners = this._listeners || (this._listeners = {});
                var id = obj._listenerId || (obj._listenerId = uniqueId('l'));
                listeners[id] = obj;
                if (typeof name === 'object') callback = this;
                obj[implementation](name, callback, this);
                return this;
            };
        });

        $.extend(module, {

            // Bind an event to a `callback` function. Passing `"all"` will bind
            // the callback to all events fired.
            on: function(name, callback, context) {
                if (!eventsApi(this, 'on', name, [callback, context]) || !callback) return this;
                this._events || (this._events = {});
                var events = this._events[name] || (this._events[name] = []);
                events.push({callback: callback, context: context, ctx: context || this});
                return this;
            },

            // Bind an event to only be triggered a single time. After the first time
            // the callback is invoked, it will be removed.
            once: function(name, callback, context) {
                if (!eventsApi(this, 'once', name, [callback, context]) || !callback) return this;
                var self = this;
                var once = $.once(function() {
                    self.off(name, once);
                    callback.apply(this, arguments);
                });
                once._callback = callback;
                return this.on(name, once, context);
            },

            // Remove one or many callbacks. If `context` is null, removes all
            // callbacks with that function. If `callback` is null, removes all
            // callbacks for the event. If `name` is null, removes all bound
            // callbacks for all events.
            off: function(name, callback, context) {
                var retain, ev, events, names, i, l, j, k;
                if (!this._events || !eventsApi(this, 'off', name, [callback, context])) return this;
                if (!name && !callback && !context) {
                    this._events = {};
                    return this;
                }

                names = name ? [name] : keys(this._events);
                for (i = 0, l = names.length; i < l; i++) {
                    name = names[i];
                    if (events = this._events[name]) {
                        this._events[name] = retain = [];
                        if (callback || context) {
                            for (j = 0, k = events.length; j < k; j++) {
                                ev = events[j];
                                if ((callback && callback !== ev.callback && callback !== ev.callback._callback) ||
                                    (context && context !== ev.context)) {
                                    retain.push(ev);
                                }
                            }
                        }
                        if (!retain.length) delete this._events[name];
                    }
                }

                return this;
            },

            // Trigger one or many events, firing all bound callbacks. Callbacks are
            // passed the same arguments as `trigger` is, apart from the event name
            // (unless you're listening on `"all"`, which will cause your callback to
            // receive the true name of the event as the first argument).
            trigger: function(name) {
                if (!this._events) return this;
                var args = Array.prototype.slice.call(arguments, 1);
                if (!eventsApi(this, 'trigger', name, args)) return this;
                var events = this._events[name];
                var allEvents = this._events.all;
                if (events) triggerEvents(events, args);
                if (allEvents) triggerEvents(allEvents, arguments);
                return this;
            },

            // Tell this object to stop listening to either specific events ... or
            // to every object it's currently listening to.
            stopListening: function(obj, name, callback) {
                var listeners = this._listeners;
                if (!listeners) return this;
                var deleteListener = !name && !callback;
                if (typeof name === 'object') callback = this;
                if (obj) (listeners = {})[obj._listenerId] = obj;
                for (var id in listeners) {
                    listeners[id].off(name, callback, this);
                    if (deleteListener) delete this._listeners[id];
                }
                return this;
            }

        });
    };

    module.initialize();
    return module;
}());

launchcms.Notification = (function(){
    var module = {};
    var notify = function(messsageType, title, message, delay) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-'+ messsageType + '-sign',
            title: title,
            message: message
        },{
            type: messsageType,
            allow_dismiss: true,
            newest_on_top: false,
            placement: {
                from: "top",
                align: "right"
            },

            delay: delay,
            timer: 1000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
    };

    module.notifyError = function(title, message) {
        notify('error', title, message, 3 * 1000);
    };

    module.notifyInfo = function (title, message) {
        notify('info', title, message, 3 * 1000);
    };

    module.notifySuccess = function (title, message) {
        notify('success', title, message, 3 * 1000);
    };
    module.queue = function(title, message, type) {
        var notifications = store.get('notifications');
        if(typeof notifications ==='undefined'  || notifications == null) {
            notifications = [];
        }
        notifications.push({'title':title, 'message': message, 'type': type});
        store.set('notifications', notifications);
    };

    module.unqueueAll = function() {
        var notifications = store.get('notifications');
        if(typeof notifications ==='undefined' ||  notifications === null) {
            return;
        }
        for(var i=notifications.length - 1; i >=0 ; i--) {
            var notification = notifications[i];
            var message = notification.message;
            var title = notification.title;

            if(notification.type == 'info') {
                module.notifyInfo(title, message);
            }
            else if(notification.type == 'success') {
                module.notifySuccess(title, message);
            }
            else if(notification.type == 'error') {
                module.notifyError(title, message);
            }
        }
        store.set('notifications', []);
    };

    return module;
}());

launchcms.ConfirmationDialog = (function(){
    var module = {};
    module.show = function (message, callback) {
        var dialog = $('#confirmation-dialog');
        $('.message', dialog).html(message);
        $('.btn-confirm', dialog).off('click').on('click', function () {
            dialog.modal('hide');
            callback(true);
        });
        $('.btn-cancel', dialog).off('click').on('click', function () {
            dialog.modal('hide');
            callback(false);
        });
        dialog.modal('show');
    };

    return module;
}());
launchcms.WaitingDialog = (function(){
    var module = {};
    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
        '<div class="modal-header"><span class="glyphicon glyphicon-time"></span> <span class="title"></span></div>' +
        '<div class="modal-body">' +
        '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
        '</div>' +
        '</div></div></div>');
    /**
     * Opens our dialog
     * @param message Custom message
     * @param options Custom options:
     * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
     * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
     */
    module.show = function (message, options) {
        // Assigning defaults
        if (typeof options === 'undefined') {
            options = {'dialogSize': 'sm'};
        }
        if (typeof message === 'undefined') {
            message = 'Loading';
        }
        var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
        }, options);

        // Configuring dialog
        $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
        $dialog.find('.progress-bar').attr('class', 'progress-bar');
        if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
        }
        $dialog.find('.title').text(message);
        // Adding callbacks
        if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                settings.onHide.call($dialog);
            });
        }
        // Opening dialog
        $dialog.modal();
    };
    /**
     * Closes dialog
     */
    module.hide = function () {
        $dialog.modal('hide');
    };
    return module;
}());

