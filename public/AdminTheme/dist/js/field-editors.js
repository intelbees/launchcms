launchcms.ModalFieldLoader = (function () {
    var module = {};
    module.loadDataToForm = function(modal, data, fields) {
        $('input[name="record_id"]', modal).val(data['id']);
        for(var i=0; i<fields.length; i++) {
            var field = fields[i];
            var inputElementQuery = null;

            if(field.type === 'string') {
                if(field.settings.editor === 'text_field') {
                    inputElementQuery = 'input[name="' + field.alias + '"]';
                }
                else {
                    inputElementQuery = 'textarea[name="' + field.alias + '"]';
                }
            }
            else if(field.type === 'boolean') {
                inputElementQuery = 'select[name="' + field.alias + '"]';
            }
            else if(field.type === 'reference' || field.type === 'reference_list') {
                inputElementQuery = 'select[name="' + field.alias + '"]';
            }
            else {
                inputElementQuery = 'input[name="' + field.alias + '"]';
            }
            if(inputElementQuery != null) {
                $(inputElementQuery, modal).val(data[field.alias]);
            }
        }
        $('.ckeditor').each(function () {
            var textarea = $(this);
            var editorID = textarea.attr('data-editor-id');
            CKEDITOR.instances[editorID].setData(textarea.val());
        });

    };
    return module;
}());


launchcms.FieldEditors = {};
launchcms.FieldEditors.TextFieldEditor = (function () {
    var module = {};
    module.initialize = function() {
        $(".rich-text").each(function() {
            var editorID = $(this).attr('data-editor-id');
            CKEDITOR.replace(editorID, {
                filebrowserBrowseUrl: '/admin/file-browser'

            });
        });
    };
    return module;
}());

launchcms.FieldEditors.DatePickerEditor = (function () {
    var module = {};
    module.initialize = function() {
        $('.field-date-picker').datepicker({
            autoclose: true
        });
        $('.field-date-mask').inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    };
    return module;
}());


launchcms.FieldEditors.TimePickerEditor = (function () {
    var module = {};
    module.initialize = function() {
        $('.field-datetime-picker').datetimepicker();


    };
    return module;
}());

launchcms.FieldEditors.IntegerEditor = (function () {
    var module = {};
    module.initialize = function() {
        $('.field-integer').inputmask({ alias: "integer"});
    };
    return module;
}());

launchcms.FieldEditors.DoubleEditor = (function () {
    var module = {};
    module.initialize = function() {
        $('.field-double').inputmask({ alias: "decimal"});
    };
    return module;
}());

launchcms.FieldEditors.ReferenceEditor = (function () {
    var module = {};
    module.initialize = function() {
        $('.reference-field').each(function() {
            var dataSearchURL = $(this).attr('data-search-url');
            $(this).select2({
                ajax: {
                    url: dataSearchURL,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }

                },
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 1

            });

        });
    };
    return module;
}());

launchcms.FieldEditors.ReferenceListEditor = (function () {
    var module = {};
    module.initialize = function() {
        $('.reference-list-field').each(function() {
            var dataSearchURL = $(this).attr('data-search-url');
            $(this).select2({
                ajax: {
                    url: dataSearchURL,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }

                },
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 1

            });

        });
    };
    return module;
}());



launchcms.EmbeddedItemDialog = function(modalID, formID) {
    var form =$(formID);
    var modal = $(modalID);

    form.parsley();
    this.showDialog = function(fieldAlias, embeddedRecord, fields) {
        launchcms.EditContentPage.saveMainForm(true);
        if(!launchcms.EditContentPage.validateMainForm()) {
            return;
        }
        form[0].reset();
        $('input[name="field_alias"]', modal).val(fieldAlias);

        if(embeddedRecord) {
            $('input[name="record_id"]', modal).val(embeddedRecord.id);
            launchcms.ModalFieldLoader.loadDataToForm(modal, embeddedRecord, fields);
        }
        modal.modal('show');
    };
    var onSaveCallBack = function(result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            modal.modal('hide');
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('content.saved');
            var fieldAlias = $('input[name="field_alias"]', modal).val();
            launchcms.FieldEditors.EmbeddedListEditor.reloadData(fieldAlias);
        } else {
            $('.error-panel', modal).removeClass('hidden');
            $('.error-message', modal).html(result.error);
        }

    };
    var onSaveRecord = function(result) {
        //below fix the bug of CKEditor cannot update textarea. #ref: http://stackoverflow.com/questions/3147670/ckeditor-update-textarea
        for(var instanceName in CKEDITOR.instances) {
            CKEDITOR.instances[instanceName].updateElement();
        }
        if(!form.parsley().validate()) {
            return;
        }
        launchcms.WaitingDialog.show(result.message);
        var contentSaveApi = launchcms.getApiUrl('content-save-embedded-record');
        $.post(contentSaveApi, $(form).serialize(), onSaveCallBack);


    };
    $('.btn-save', modal).unbind('click').bind('click',onSaveRecord);
};

launchcms.FieldEditors.EmbeddedListEditor = (function () {
    var module = {};
    var getRecordsFromServer = function(contentType, fieldAlias, contentID, callback){
        var apiURLTogetEmbeddedRecords = '/' +  launchcms.adminSlug
            + '/ajax/content/get-embedded-records/' + contentType + '/' + fieldAlias + '/' + contentID;
        $.get(apiURLTogetEmbeddedRecords, callback);
    };
    var onEditRecord = function(){
        var recordID = $(this).attr('data-record-id');
        var modalID = $(this).attr('data-modal-id');
        var formID = $(this).attr('data-form-id');
        var fieldAlias = $(this).attr('data-field-alias');
        var contentType = $('#content-type-alias').val();
        var contentID = $('#content-id').val();
        getRecordsFromServer(contentType, fieldAlias, contentID, function(result) {
            if(!result.success) {
                return;
            }
            var data = result.data;
            var recordRow = null;
            for(var i=0; i<data.length; i++) {
                if(data[i].id == recordID) {
                    recordRow = data[i];
                    break;
                }
            }
            var dialog = new launchcms.EmbeddedItemDialog(modalID, formID);
            dialog.showDialog(fieldAlias, recordRow, result.fields);
        });
    };
    var onDeleteCallBack = function(fieldAlias, result) {
        launchcms.WaitingDialog.hide();
        if(result.success) {
            launchcms.Notification.notifySuccess('', result.message);
            launchcms.Events.trigger('data-type.deleted');
            module.reloadData(fieldAlias);
        } else {
            launchcms.Notification.notifyError('', result.error);
        }
    };
    var onDeleteRecord = function() {
        var recordID = $(this).attr('data-record-id');
        var fieldAlias = $(this).attr('data-field-alias');
        console.log(recordID);
        var contentTypeAlias = $('#content-type-alias').val();
        var contentID = $('#content-id').val();
        var confirmationMessage = $(this).attr('data-confirm-message');
        launchcms.ConfirmationDialog.show(confirmationMessage, function(result) {
            if(!result)
            {
                return;
            }
            launchcms.WaitingDialog.show(launchcms.getMessage('launchcms.messages.processing'));
            var apiURLDeleteEmbeddedRecords = '/' +  launchcms.adminSlug + '/ajax/content/delete-embedded-record';
            $.post(apiURLDeleteEmbeddedRecords,
                {
                    'content_id': contentID,
                    'content_type_alias': contentTypeAlias,
                    'field_alias': fieldAlias,
                    'record_id': recordID,
                    '_token': launchcms.getCSRFToken()
                }, function(result) {
                    onDeleteCallBack(fieldAlias, result);
                });
        });
    };

    module.initialize = function() {
        $('.btn-create-embedded-list-item').each(function() {
            var modalID = $(this).attr('data-modal-id');
            var formID = $(this).attr('data-form-id');
            var dialog = new launchcms.EmbeddedItemDialog(modalID, formID);
            $(this).bind('click', function() {
                var fieldAlias = $(this).attr('data-field-alias');
                dialog.showDialog(fieldAlias);
            });
        });

        $('body').on('click', '.btn-embedded-record-edit', onEditRecord);
        $('body').on('click', '.btn-embedded-record-delete', onDeleteRecord);
    };
    module.reloadData = function(fieldAlias) {
        var contentTypeAlias = $('#content-type-alias').val();
        var contentID = $('#content-id').val();
        getRecordsFromServer(contentTypeAlias, fieldAlias, contentID, function(result) {
            if(!result.success) {
                return;
            }
            var data = result.data;
            var html = launchcms.Template.renderWithInPageTemplate('#tpl-'+ fieldAlias + '-body',
                {data: data}
            );
            $('#table-' + fieldAlias).find('tbody').html(html);

        })
    };
    return module;
}());

launchcms.FieldEditors.GeoEditor = (function () {
    var module = {};

    var getExistingLocationFromGeoInput = function(inputEl) {
        var hiddenFieldID = $(inputEl).attr('data-hidden-field');
        var existingValue = $('#' + hiddenFieldID ).val();
        var defLocation = null;
        if(existingValue != '') {
            var lngLat = existingValue.split(',');
            var lat = parseFloat(lngLat[0]);
            var lng = parseFloat(lngLat[1]);
            defLocation = new window.google.maps.LatLng(lat, lng);
        }
        return defLocation;
    };
    module.refreshMap = function() {
        $('.field-geo-input').each(function() {
            var defLocation = getExistingLocationFromGeoInput($(this));
            var map = $(this).geocomplete("map");
            google.maps.event.trigger(map, 'resize');
            if(defLocation != null) {
                map.setZoom(15);
                map.setCenter(defLocation);
            }
        });
    };
    module.initialize = function() {
        $('.field-geo-input').each(function() {
            var canvasID = $(this).attr('data-map-canvas-id');
            var labelFieldID = $(this).attr('data-label-field-id');
            var hiddenFieldID = $(this).attr('data-hidden-field');

            var defLocation = getExistingLocationFromGeoInput($(this));

            var mapField = $(this);
            var geoField = null;
            if(defLocation != null) {
                geoField = mapField.geocomplete({
                    map: "#" + canvasID,
                    location: defLocation,
                    markerOptions: {
                        draggable: true,
                        position: defLocation
                    },
                    mapOptions: {
                        center: defLocation
                    }
                });
                var map = mapField.geocomplete("map");
                google.maps.event.trigger(map, 'resize');
                map.setZoom(15);
                map.setCenter(defLocation);
            } else {
                geoField = mapField.geocomplete({
                    map: "#" + canvasID,
                    mapOptions: {
                        zoom: 10
                    },
                    markerOptions: {
                        draggable: true
                    }
                });
            }

            geoField .bind("geocode:result", function(event, result){
                    console.log(result);
                    var geoText = 'Longitude = ' + result.geometry.location.lng() + '. Latitude: '
                        + result.geometry.location.lat();
                    var geoValue = + result.geometry.location.lat() + ',' + result.geometry.location.lng();
                    $('#' + labelFieldID ).html(geoText);
                    $('#' + hiddenFieldID ).val(geoValue);
                    console.log("Result: " + result.formatted_address);
                    var map = mapField.geocomplete("map");
                    google.maps.event.trigger(map, 'resize');
                    map.setZoom(14);       //set appropriate zoom
                    map.setCenter(result.geometry.location);
                })
                .bind("geocode:error", function(event, status){

                })
                .bind("geocode:multiple", function(event, results){

                }).bind("geocode:dragged", function(event, latLng){
                    var geoText = 'Longitude = ' + latLng.lng() + '. Latitude: ' + latLng.lat();
                    $('#' + labelFieldID ).html(geoText);
                    var geoValue = latLng.lat() + ','  + latLng.lng();
                    $('#' + hiddenFieldID ).val(geoValue);

                });

        });
    };
    return module;
}());
