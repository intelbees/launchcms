# LaunchCMS


Launch CMS is the CMS for developer - which mainly focus on flexibility, scalability.

We're trying to provide a strong platform that help you to build any product which can be extended easily and quickly.

## Official Documentation
[Wiki] (https://gitlab.com/intelbees/launchcms/wikis/home)


## License

The Launch CMS is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
